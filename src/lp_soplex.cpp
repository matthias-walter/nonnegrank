#include "lp.hpp"

#include <soplex.h>

#include <iostream>

namespace nonnegrank
{
   struct LPSolver
   {
      soplex::SoPlex spx;
   };

   void LP::initSolver(const std::vector<double>& objective, bool lowerBound)
   {
      _lpSolver = new LPSolver;

      soplex::SoPlex& spx = _lpSolver->spx;

      spx.setIntParam(soplex::SoPlex::VERBOSITY, soplex::SoPlex::VERBOSITY_WARNING);
      spx.setIntParam(soplex::SoPlex::OBJSENSE, soplex::SoPlex::OBJSENSE_MAXIMIZE);
      soplex::LPColSetReal cols(objective.size());
      soplex::SVectorReal zeroVector;
      for (std::size_t v = 0; v < objective.size(); ++v)
         cols.add(objective[v], lowerBound ? 0.0 : -soplex::infinity, zeroVector, 1.0);

      if (_foolingPairs)
      {
         for (std::size_t p = 0; p < numEffectiveFoolingPairs(); ++p)
            cols.add(2.0, 0.0, zeroVector, 1.0);
      }
      spx.addColsReal(cols);
   }

   void LP::deinitSolver()
   {
      delete _lpSolver;
      _lpSolver = nullptr;
   }

   LP::Constraint LP::addRectangle(std::shared_ptr<Submatrix> rectangle)
   {
      soplex::SoPlex& spx = _lpSolver->spx;

      std::size_t count = 0;
      for (std::size_t i = 0; i < rectangle->nonzeros.size(); ++i)
      {
         if (rectangle->nonzeros[i])
            ++count;
      }
      assert(count == rectangle->rows.size() * rectangle->columns.size());

      soplex::DSVectorReal vector;
      for (auto row : rectangle->rows)
      {
         for (auto column : rectangle->columns)
         {
            std::size_t i = _data.denseIndices[row][column];
            assert(i < _data.numNonzeros());
            vector.add(i, 1.0);
         }
      }

      if (_foolingPairs)
      {
         for (std::size_t p = 0; p < numEffectiveFoolingPairs(); ++p)
         {
            const FoolingPair& fp = _data.foolingPairs[p];
            if (rectangle->nonzeros[fp.nonzero1] || rectangle->nonzeros[fp.nonzero2])
               vector.add(_data.numNonzeros() + p, 1.0);
         }
      }

      spx.addRowReal(soplex::LPRowReal(-soplex::infinity, vector, 1.0));

      _rectangles[spx.numRowsReal() - 1] = rectangle;

      return spx.numRowsReal() - 1;
   }

   LP::Constraint LP::mergeEntryVariables(std::size_t v1, std::size_t v2)
   {
      assert(v1 < _data.numNonzeros());
      assert(v2 < _data.numNonzeros());

      soplex::SoPlex& spx = _lpSolver->spx;

      soplex::DSVectorReal vector;
      vector.add(v1, 1.0);
      vector.add(v2, -1.0);
      spx.addRowReal(soplex::LPRowReal(0.0, vector, 0.0));

      return spx.numRowsReal() - 1;
   }

   LP::Constraint LP::mergeFoolingPairVariables(std::size_t p1, std::size_t p2)
   {
      assert(_foolingPairs);
      assert(p1 < _data.numFoolingPairs());
      assert(p2 < _data.numFoolingPairs());

      soplex::SoPlex& spx = _lpSolver->spx;

      soplex::DSVectorReal vector;
      vector.add(_data.numNonzeros() + p1, 1.0);
      vector.add(_data.numNonzeros() + p2, -1.0);
      spx.addRowReal(soplex::LPRowReal(0.0, vector, 0.0));

      return spx.numRowsReal() - 1;
   }

   void LP::splitVariables(const std::vector<Constraint>& constraints)
   {
      soplex::SoPlex& spx = _lpSolver->spx;
      for (std::size_t i = 0; i < constraints.size(); ++i)
         spx.changeRangeReal(constraints[i], -soplex::infinity, soplex::infinity);
      spx.clearBasis();
   }

   void LP::run(double abortIfAtLeast, double abortIfAtMost)
   {
      soplex::SoPlex& spx = _lpSolver->spx;
      soplex::DVectorReal solution(spx.numColsReal());

      bool resolve = true;
      while (resolve)
      {
         if (_data.verbosity > 1)
            std::cerr << "  Solving " << spx.numRowsReal() << "x" << spx.numColsReal() << " LP." << std::flush;

         soplex::SPxSolver::Status status = spx.solve();
         if (status == soplex::SPxSolver::RUNNING)
         {
            spx.clearBasis();
            std::cerr << "[SoPlex error - clear basis and retry]" << std::flush;
            status = spx.solve();
         }

         if (status != soplex::SPxSolver::OPTIMAL)
         {
            std::stringstream ss;
            ss << "Unexpected SoPlex status: ";
            ss << status;
            throw std::runtime_error(ss.str());
         }

         _objectiveValue = spx.objValueReal();

         if (_data.verbosity > 1)
            std::cerr << "obj.value: " << _objectiveValue << ". " << std::flush;

         spx.getPrimalReal(&solution[0], spx.numColsReal());

         for (std::size_t v = 0; v < _nonzeroWeights.size(); ++v)
            _nonzeroWeights[v] = solution[v];
         for (std::size_t p = 0; p < _foolingPairWeights.size(); ++p)
            _foolingPairWeights[p] = solution[_nonzeroWeights.size() + p];

         resolve = separate(abortIfAtMost, abortIfAtLeast);
      }
   }

   double LP::getSolution(std::vector<double>& weights, std::vector<double>& foolingPairWeights) const
   {
      soplex::SoPlex& spx = _lpSolver->spx;

      weights = _nonzeroWeights;
      foolingPairWeights = _foolingPairWeights;

      return spx.objValueReal();
   }

   void LP::getBasicRectangles(std::vector<std::shared_ptr<Submatrix>>& basicRectangles, std::vector<double>& duals)
      const
   {
      duals.clear();
      duals.resize(basicRectangles.size(), 0.0); // We keep old rectangles intact and set the dual value to 0.0.

      soplex::SoPlex& spx = _lpSolver->spx;
      std::vector<soplex::SPxSolver::VarStatus> rowBasis(spx.numRowsReal());
      std::vector<soplex::SPxSolver::VarStatus> colBasis(spx.numColsReal());
      soplex::DVectorReal dualSolution(spx.numRowsReal());
      spx.getBasis(&rowBasis[0], &colBasis[0]);
      spx.getDualReal(&dualSolution[0], spx.numRowsReal());
      for (std::size_t i = 0; i < spx.numRowsReal(); ++i)
      {
         if (rowBasis[i] != soplex::SPxSolver::VarStatus::ON_UPPER)
            continue;

         if (dualSolution[i] <= _dualEpsilon)
            dualSolution[i] = 0.0;

         auto got = _rectangles.find(i);
         if (got != _rectangles.end())
         {
            basicRectangles.push_back(got->second);
            duals.push_back(dualSolution[i]);
         }
      }
   }

} /* namespace nonnegrank */
