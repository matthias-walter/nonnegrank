#include "group.hpp"
#include "nauty_common.hpp"

namespace nonnegrank
{
   DYNALLSTAT(int,allp,allp_sz);
   DYNALLSTAT(int,id,id_sz);

   void writeautom(int* p, Param& param)
   {
      auto permutation = std::make_shared<Permutation>();
      permutation->rows.resize(param.numRows);
      permutation->columns.resize(param.numColumns);

      for (std::size_t r = 0; r < param.numRows; ++r)
         permutation->rows[r] = p[r];
      for (std::size_t c = 0; c < param.numColumns; ++c)
         permutation->columns[c] = p[param.numRows + c] - param.numRows; // TODO: subtraction is a guessed fix.

      param.automorphisms.push_back(permutation);
   }

   // Recursive routine used by allgroup.
   static void my_groupelts(levelrec *lr, int n, int level,int *before,int *after,int *id, Param& param)
   {
       int orbsize;
       int * p,* cr;
       cosetrec *coset;

       coset = lr[level].replist;
       orbsize = lr[level].orbitsize;

       for (std::size_t j = 0; j < orbsize; ++j)
       {
         cr = (coset[j].rep == NULL ? NULL : coset[j].rep->p);
         if (before == NULL)
            p = cr;
         else if (cr == NULL)
            p = before;
         else
         {
            p = after;
            for (std::size_t i = 0; i < n; ++i)
               p[i] = cr[before[i]];
         }

         if (level == 0)
            writeautom((p == NULL ? id : p), param);
         else
            my_groupelts(lr,n,level-1,p,after+n,id, param);
       }
   }

   // Call writeautom for every element of the group, including the identity.
   void my_allgroup(grouprec * grp, Param& param)
   {
      int depth, n;

      depth = grp->depth;
      n = grp->n;

      DYNALLOC1(int,id,id_sz,n, const_cast<char*>("malloc"));
      for (std::size_t i = 0; i < n; ++i)
         id[i] = i;

      if (depth == 0)
      {
         writeautom(id, param);
         return;
      }

      DYNALLOC1(int,allp,allp_sz,n*depth, const_cast<char*>("malloc"));
      my_groupelts(grp->levelinfo,n,depth-1,NULL,allp,id, param);
   }

   void computeAutomorphismGroup(Data& data, std::vector<std::shared_ptr<Permutation>>& automorphisms)
   {
      const std::size_t n = data.numRows + data.numColumns + data.numNonzeros();
      const std::size_t m = SETWORDSNEEDED(n);

      // Initializations.

      DYNALLSTAT(int, lab, lab_sz);
      DYNALLSTAT(int, ptn, ptn_sz);
      DYNALLSTAT(int, orbits, orbits_sz);
      DYNALLSTAT(graph,g,g_sz);

      static DEFAULTOPTIONS_GRAPH(options);
      statsblk stats;

      /* The following cause nauty to call two procedures which
        store the group information as nauty runs. */
      options.userautomproc = groupautomproc;
      options.userlevelproc = grouplevelproc;

      // Select option for custom partition
      options.defaultptn = FALSE;

      nauty_check(WORDSIZE,m,n,NAUTYVERSIONID);

      // Allocate memory for the graph
      DYNALLOC1(int,lab,lab_sz,n, const_cast<char*>("malloc"));
      DYNALLOC1(int,ptn,ptn_sz,n, const_cast<char*>("malloc"));
      DYNALLOC1(int,orbits,orbits_sz,n, const_cast<char*>("malloc"));
      DYNALLOC2(graph,g,g_sz,n,m, const_cast<char*>("malloc"));

      // Empty the graph
      EMPTYGRAPH(g,m,n);

      getMidNodeGraph(g,data, m, lab, ptn);

      // for (std::size_t j = 0; j != n; ++j ) std::cerr << lab[j] << ' ';
      // std::cerr << std::endl;
      // for (std::size_t j = 0; j != n; ++j ) std::cerr << ptn[j] << ' ';
      // std::cerr << std::endl;

      //determine the automorphism group of the graph with nauty
      densenauty(g, lab, ptn, orbits, &options, &stats, m, n, nullptr);

      /* Get a pointer to the structure in which the group information
        has been stored.  If you use TRUE as an argument, the
        structure will be "cut loose" so that it won’t be used
        again the next time nauty() is called.  Otherwise, as
        here, the same structure is used repeatedly. */
      grouprec* group = groupptr(FALSE);
      /* Expand the group structure to include a full set of coset
        representatives at every level.  This step is necessary
        if allgroup() is to be called. */
      makecosetreps(group);
      /* Call the procedure writeautom() for every element of the group.
       The first call is always for the identity. */

      automorphisms.clear();
      Param param = { data.numRows, data.numColumns, automorphisms };

      my_allgroup(group, param);//,n_rows,n_columns);

      // Clean up
      DYNFREE(allp,allp_sz);
      DYNFREE(id,id_sz);
      DYNFREE(lab,lab_sz);
      DYNFREE(ptn,ptn_sz);
      DYNFREE(orbits,orbits_sz);
      DYNFREE(g,g_sz);
   }

}
