#include "nauty_common.hpp"

#include <vector>
// #include <algorithm>
// #include <iostream>


namespace nonnegrank
{
   
   
   void getMidNodeGraph(graph* g, Data& data, const std::size_t m, int* lab, int* ptn)
   {
      const std::size_t numNodes = data.numRows + data.numColumns + data.numNonzeros();
      
      // Initially, everything is one color class.

      for (std::size_t v = 0; v < numNodes; ++v)
      {
         lab[v] = v;
         ptn[v] = 1;
      }

      // Color class for row nodes and for column nodes.

      ptn[data.numRows - 1] = 0;
      ptn[data.numRows + data.numColumns - 1] = 0;

      // Color classes for nonzero nodes.

      for (std::size_t i = 0; i < data.numNonzeros() - 1; ++i)
      {
         if (data.nonzeros[data.sortedNonzeros[i]].entry != data.nonzeros[data.sortedNonzeros[i + 1]].entry)
            ptn[data.numRows + data.numColumns + i] = 0;
      }
      ptn[data.numRows + data.numColumns + data.numNonzeros() - 1] = 0;

      // Add edges.

      for (std::size_t i = 0; i < data.numNonzeros(); ++i)
      {
         const Nonzero& nz = data.nonzeros[data.sortedNonzeros[i]];
         ADDONEEDGE(g, nz.row, data.numRows + data.numColumns + i, m);
         ADDONEEDGE(g, data.numRows + nz.column, data.numRows + data.numColumns + i, m);
      }
   }

} /* namespace nonnegrank */
