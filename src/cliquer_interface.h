#ifndef _NONNEGRANK_CLIQUER_INTERFACE_H
#define _NONNEGRANK_CLIQUER_INTERFACE_H

int computeMaximumClique(int numNodes, int numEdges, int* edgeNodes, int* cliqueNodes);

#endif /* _NONNEGRANK_CLIQUER_INTERFACE_H */