#include "data.hpp"

#include <iostream>
#include <sstream>
#include <scip/scip.h>
#include <scip/scipdefplugins.h>

namespace nonnegrank
{

   bool FoolingSetBound::doCompute(bool abortNoImprovement, double& subtractTime)
   {
      if (abortNoImprovement)
      {
         if (_data.bestLowerBound >= _data.bestUpperBound)
            return false;
      }

      if (_data.verbosity > 0)
         std::cerr << "Computing <fooling set> bound. " << std::flush;

      if (_data.numNonzeros() == 0)
      {
         if (_data.verbosity > 0)
            std::cerr << " done." << std::endl;
         return true;
      }

      // Create IP

      SCIP* scip;
      SCIP_CALL_ABORT(SCIPcreate(& scip));
      SCIP_CALL_ABORT(SCIPcreateProbBasic(scip, "problem"));
      SCIP_CALL_ABORT(SCIPincludeDefaultPlugins(scip));
      SCIP_CALL_ABORT(SCIPsetIntParam(scip, "display/verblevel", _data.verbosity > 1 ? 4 : 0));
      SCIP_CALL_ABORT(SCIPsetIntParam(scip, "separating/clique/priority", 10000));
      SCIP_CALL_ABORT(SCIPsetIntParam(scip, "separating/strongcg/freq", -1));
      SCIP_CALL_ABORT(SCIPsetBoolParam(scip, "misc/catchctrlc", false));
      SCIP_CALL_ABORT(SCIPsetObjsense(scip, SCIP_OBJSENSE_MAXIMIZE));

      // Construct a variable x_ij for every nonzero.

      std::vector<SCIP_VAR*> vars;
      vars.resize(_data.numNonzeros());
      for (std::size_t s = 0; s < _data.numNonzeros(); ++s)
      {
         std::stringstream name;
         name << "x#" << _data.nonzeros[s].row << "#" << _data.nonzeros[s].column;
         SCIP_CALL_ABORT(SCIPcreateVarBasic(scip, &vars[s], name.str().c_str(), 0.0, 1.0, 1.0, SCIP_VARTYPE_BINARY));
         SCIP_CALL_ABORT(SCIPaddVar(scip, vars[s]));
      }
      
      // For every nonzero pair (i,j) and (k,l), add a conflict if their induced submatrix is smaller than 2x2 or if
      // it contains no zero.

      for (std::size_t s1 = 0; s1 < _data.numNonzeros(); ++s1)
      {
         for (std::size_t s2 = s1 + 1; s2 < _data.numNonzeros(); ++s2)
         {
            bool conflict = false;
            std::size_t row1 = _data.nonzeros[s1].row;
            std::size_t column1 = _data.nonzeros[s1].column;
            std::size_t row2 = _data.nonzeros[s2].row;
            std::size_t column2 = _data.nonzeros[s2].column;
            if (row1 == row2 || column1 == column2
               || (_data.isNonzero(row1, column2) && _data.isNonzero(row2, column1)))
            {
               SCIP_CONS* constraint = nullptr;
               std::stringstream name;
               name << "pair#" << s1 << "#" << s2;
               SCIP_CALL_ABORT(SCIPcreateConsBasicLinear(scip, &constraint, name.str().c_str(), 0, nullptr, nullptr, 
                  0.0, 1.0));
               SCIP_CALL_ABORT(SCIPaddCoefLinear(scip, constraint, vars[s1], 1.0));
               SCIP_CALL_ABORT(SCIPaddCoefLinear(scip, constraint, vars[s2], 1.0));
               SCIP_CALL_ABORT(SCIPaddCons(scip, constraint));
               SCIP_CALL_ABORT(SCIPreleaseCons(scip, &constraint));
            }
         }
      }

      if (_data.verbosity > 1)
      {
         std::cerr << "\n  Initialized IP with " << SCIPgetNOrigVars(scip)  << " variables and "
            << SCIPgetNOrigConss(scip) << " constraints." << std::endl;
      }

      SCIP_CALL_ABORT(SCIPsolve(scip));

      if (_data.verbosity > 2)
         SCIP_CALL_ABORT(SCIPprintStatistics(scip, stdout));

      if (SCIPgetStatus(scip) != SCIP_STATUS_OPTIMAL)
      {
         std::cerr << "WARNING: <fooling set> bound computation via scip"
            << " terminated with nonoptimal status " << SCIPgetStatus(scip) << "." << std::endl;
      }
      
      _value = SCIPgetPrimalbound(scip);

      // Extract the fooling set as a certificate.

      int size = int(_value + 0.5);
      SCIP_SOL* bestSol = SCIPgetBestSol(scip);
      auto foolingSet = std::make_shared<Submatrix>();
      foolingSet->rows.reserve(size);
      foolingSet->columns.reserve(size);
      for (std::size_t s = 0; s < _data.numNonzeros(); ++s)
      {
         if (SCIPgetSolVal(scip, bestSol, vars[s]) > 0.5)
         {
            foolingSet->rows.push_back(_data.nonzeros[s].row);
            foolingSet->columns.push_back(_data.nonzeros[s].column);
         }
      }
      _foolingSet = foolingSet;

      for (std::size_t s = 0; s < _data.numNonzeros(); ++s)
      {
         SCIP_CALL_ABORT(SCIPreleaseVar(scip, &vars[s]));
      }
      SCIP_CALL_ABORT(SCIPfree(&scip));
      
      if (_data.verbosity > 0)
      {
         std::cerr << "done.\n" << std::flush;
      }

      return true;
   }

} /* namespace nonnegrank */
