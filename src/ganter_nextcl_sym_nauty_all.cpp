#include <boost/dynamic_bitset.hpp>

#include "data.hpp"
#include "group.hpp"
#include "ganter_common.hpp"
#include "ganter_nextcl_sym_nauty.hpp"

#include <iostream>

namespace nonnegrank
{
   boost::dynamic_bitset<> flatten(boost::dynamic_bitset<> & A, boost::dynamic_bitset<> & B)
   {
      auto n(B.size());
      boost::dynamic_bitset<> fl_R(A.size()*n);

      for (auto i = A.find_first(); i != boost::dynamic_bitset<>::npos; i = A.find_next(i))
      {
         for (auto j = B.find_first(); j != boost::dynamic_bitset<>::npos; j = B.find_next(j)) 
         {
            fl_R.flip( (n-1)*i + j );
         }
      }

   }

   // With symmetries
   void closure_sym_all(std::vector<std::shared_ptr<Permutation>>& automorphisms, boost::dynamic_bitset<> & A, boost::dynamic_bitset<> & B)
   {
      auto in_A(A);
      auto in_B(B);

      boost::dynamic_bitset<> phi_A(A.size());
      boost::dynamic_bitset<> phi_B(B.size());

      for (std::size_t i = 0; i < automorphisms.size(); ++i)
      {
         auto ithPermutation(automorphisms[i]);
         auto phiRows(ithPermutation->rows);
         auto phiCols(ithPermutation->columns);

         phi_A.reset();
         phi_B.reset();

         phi(in_A,in_B,phi_A,phi_B,phiRows,phiCols);

         //if (preccurly(flatten(A,B),flatten(phi_A,phi_B)))
         if (preccurly(A,phi_A) && preccurly(B,phi_B))
         {
            A = phi_A;
            B = phi_B;
         }
      }
      phi_A.clear();
      phi_B.clear();
   }

   void nextClosure_sym_all(std::vector<std::shared_ptr<Permutation>>& automorphisms,const std::vector<boost::dynamic_bitset<>> & M, const std::vector<boost::dynamic_bitset<>> & M_T, boost::dynamic_bitset<> & A, boost::dynamic_bitset<> & B)
   {
      boost::dynamic_bitset<> CI_A(A.size());
      boost::dynamic_bitset<> I_A(A.size());
      boost::dynamic_bitset<> CI_B(B.size());
      boost::dynamic_bitset<> I_B(B.size());
      boost::dynamic_bitset<> notA(~A);
      bool flag = true;

      auto i = notA.find_first();
      while (flag)
      {
         I_A = inc(A,i);
         i = notA.find_next(i);
         CI_A = I_A;
         while (flag)
         {
            boost::dynamic_bitset<> notB(~B);
            auto j = notB.find_first();
            I_B = inc(B,j);
            j = notB.find_next(j);    
            CI_B = I_B;
            closure_sym_all(automorphisms,CI_A,CI_B);
            flag = !(sqsubseteq(I_A,CI_A) && sqsubseteq(I_B,CI_B));
         }
      }
      A = CI_A;
      B = CI_B;
   }

   void constructRectanglesGanterSymmetryNauty(Data& data)
   {
      std::vector<boost::dynamic_bitset<>> M(data.numRows, boost::dynamic_bitset<>(data.numColumns));
      std::vector<boost::dynamic_bitset<>> M_T(data.numColumns, boost::dynamic_bitset<>(data.numRows));

      construct_matrices(data,M,M_T);

      boost::dynamic_bitset<> A(data.numRows);
      boost::dynamic_bitset<> B(data.numColumns);

      std::vector<std::shared_ptr<Permutation>> automorphisms;
      computeAutomorphismGroup(data, automorphisms);
    
      while(!(A.all() && B.all())) {
         nextClosure_sym_all(automorphisms, M, M_T, A, B);
         pushRectangle(data, data.maximalRectanglesSymmetry, A, B);
         data.setSubmatrixNonzeros(data.maximalRectanglesSymmetry.back());
         // std::cerr << "    " << A << std::endl;
         // std::cerr << "    " << B << std::endl << std::endl;
      }
   }
}
