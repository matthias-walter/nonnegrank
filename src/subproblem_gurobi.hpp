#ifndef _NONNEGRANK_SUBPROBLEM_GUROBI_HPP
#define _NONNEGRANK_SUBPROBLEM_GUROBI_HPP

#include "subproblem.hpp"

#include <gurobi_c.h>

namespace nonnegrank
{

   class SubproblemSolverGurobi: public SubproblemSolver
   {
   public:
      SubproblemSolverGurobi(Data& data, bool foolingPairs, bool branchings);

      virtual ~SubproblemSolverGurobi();

      virtual void findPositive(double constant, const std::vector<double>& nonzeroWeights,
         const std::vector<double>& foolingPairWeights,
         const std::vector<std::shared_ptr<BranchingData>>& branchingWeights,
         std::vector<std::shared_ptr<Submatrix>>& rectangles, double& bestWeight, double& upperBound);

   protected:
      GRBenv* _env;
      GRBmodel *_model;
      std::size_t _firstRowVariable;
      std::size_t _firstColumnVariable;
      std::size_t _firstNonzeroVariable;
      std::size_t _firstFoolingPairVariable;
      std::size_t _numVariables;
   };

}

#endif /* _NONNEGRANK_SUBPROBLEM_GUROBI_HPP */
