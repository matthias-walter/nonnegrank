#ifndef _NONNEGRANK_BRANCHANDPRICE_SCIP_BRANCH_HPP
#define _NONNEGRANK_BRANCHANDPRICE_SCIP_BRANCH_HPP

#include <objscip/objbranchrule.h>

#include "branchandprice_scip_probdata.hpp"
#include "subproblem.hpp"

namespace nonnegrank
{

   class RectangleCoveringBranchrule: public scip::ObjBranchrule
   {
   public:
      RectangleCoveringBranchrule(SCIP* scip);

      virtual ~RectangleCoveringBranchrule();

      virtual SCIP_RETCODE scip_execlp(SCIP* scip, SCIP_BRANCHRULE* branchrule, unsigned int allowaddcons,
         SCIP_RESULT* result);

   protected:
      bool findBranchingPair(SCIP* scip, RectangleCoveringProbdata* probdata, std::vector<std::size_t>& nonzeros);

      bool findBranchingExtension(SCIP* scip, RectangleCoveringProbdata* probdata, std::vector<std::size_t>& nonzeros);

      bool findBranchingMaximal(SCIP* scip, RectangleCoveringProbdata* probdata, std::vector<std::size_t>& nonzeros);
   };

}

#endif /* _NONNEGRANK_BRANCHANDPRICE_SCIP_BRANCH_HPP */