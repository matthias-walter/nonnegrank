#include "data.hpp"

#include <limits>
#include <iostream>

#include <Eigen/Dense>

namespace nonnegrank
{

   bool RankBound::doCompute(bool abortNoImprovement, double& subtractTime)
   {
      if (abortNoImprovement)
      {
         if (_data.bestLowerBound >= _data.bestUpperBound)
            return false;
      }

      if (_data.verbosity > 0)
         std::cerr << "Computing <rank> bound." << std::flush;

      if (_data.numNonzeros() == 0)
      {
         if (_data.verbosity > 0)
            std::cerr << " done." << std::endl;
         return true;
      }

      Eigen::MatrixXd matrix(_data.numRows, _data.numColumns);
      for (std::size_t r = 0; r < _data.numRows; ++r)
      {
         for (std::size_t c = 0; c < _data.numColumns; ++c)
         {
            std::size_t nz = _data.denseIndices[r][c];
            if (nz == std::numeric_limits<std::size_t>::max())
               matrix(r,c) = 0.0;
            else
               matrix(r,c) = _data.nonzeros[nz].entry;
         }
      }
      Eigen::FullPivHouseholderQR<Eigen::MatrixXd> decomposition(matrix);
      std::size_t rank = decomposition.rank();
      auto colPerm = decomposition.colsPermutation().indices();
      auto basisMatrix = std::make_shared<Submatrix>();
      basisMatrix->rows.resize(_data.numRows);
      basisMatrix->columns.resize(rank);
      for (std::size_t i = 0; i < rank; ++i)
         basisMatrix->columns[i] = colPerm(i);
      auto rowTransp = decomposition.rowsTranspositions();
      for (std::size_t i = 0; i < _data.numRows; ++i)
         basisMatrix->rows[i] = i;
      for (std::size_t i = 0; i < rowTransp.size(); ++i)
      {
         std::size_t t = rowTransp(0, i);
         std::swap(basisMatrix->rows[i], basisMatrix->rows[t]);
      }

      _basisMatrix = basisMatrix;
      basisMatrix->rows.resize(rank);
      _value = basisMatrix->rows.size();
      if (_data.verbosity > 0)
         std::cerr << " done." << std::endl;

      _data.updateLowerBound(_value);
      return true;
   }

} /* namespace nonnegrank */
