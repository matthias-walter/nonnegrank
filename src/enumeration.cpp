#include "enumeration.hpp"

#include <chrono>
#include <cassert>
#include <iostream>

#include "data.hpp"

#if defined(WITH_ENUMERATION)
#include <boost/dynamic_bitset.hpp>
#include "ganter_nextcl.hpp"


#if defined(WITH_SYMMETRY_nauty)
#include "ganter_nextcl_sym_nauty.hpp"
#endif

#endif // WITH_ENUMERATION

namespace nonnegrank
{

   double enumerateMaximalRectangles(Data& data)
   {
      double time = 0.0;

#if !defined(WITH_ENUMERATION)
      throw std::runtime_error("Enumeration of maximal rectangles is not supported!");
#else

      if (!data.maximalRectangles.empty())
         return 0.0;

      std::chrono::time_point<std::chrono::system_clock> start, end;
      start = std::chrono::system_clock::now();
      if (data.verbosity > 0)
         std::cerr << "\n  Enumerating maximal rectangles." << std::flush;

      constructMaximalRectanglesGanter(data);

      end = std::chrono::system_clock::now();
      time = std::chrono::duration_cast<std::chrono::milliseconds> (end - start).count() / 1000.0;
      data.enumerateTime += time;

      if (data.verbosity > 0)
      {
         std::cerr << " Computed " << data.maximalRectangles.size() << " rectangles (time: " << time << "s)"
            << std::endl;
      }
#endif

      return time;
   }

   double enumerateMaximalRectanglesSymmetry(Data& data)
   {
      double time = 0.0;

#if !defined(WITH_ENUMERATION) || !defined(WITH_SYMMETRY_nauty)
      throw std::runtime_error("Enumeration of maximal rectangles up to symmetry is not supported!");
#else
      if (!data.maximalRectanglesSymmetry.empty())
         return 0.0;

      std::chrono::time_point<std::chrono::system_clock> start, end;
      start = std::chrono::system_clock::now();
      if (data.verbosity > 0)
         std::cerr << "\n  Enumerating maximal rectangles up to symmetry." << std::flush;

      constructMaximalRectanglesGanterSymmetryNauty(data);

      end = std::chrono::system_clock::now();
      time = std::chrono::duration_cast<std::chrono::milliseconds> (end - start).count() / 1000.0;
      data.enumerateSymmetryTime += time;

      if (data.verbosity > 0)
      {
         std::cerr << " Computed " << data.maximalRectanglesSymmetry.size()
            << " rectangles (time: " << time << "s)" << std::endl;
      }
#endif

      return time;
   }

} /* nonnegrank */
