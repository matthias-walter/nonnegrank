#include "data.hpp"

#include "enumeration.hpp"
#include "subproblem.hpp"
#include "subproblem_debug.hpp"
#include "subproblem_enumerate_small.hpp"

#ifdef WITH_IP_scip
#include "subproblem_scip.hpp"
#endif

#ifdef WITH_IP_gurobi
#include "subproblem_gurobi.hpp"
#endif

#include <scip/scip.h>
#include <scip/nodesel_bfs.h>
#include <scip/cons_integral.h>
#include <scip/cons_linear.h>
#include <scip/disp_default.h>
#include <scip/heur_crossover.h>
#include <scip/heur_dins.h>
#include <scip/heur_fixandinfer.h>
#include <scip/heur_intshifting.h>
#include <scip/heur_localbranching.h>
#include <scip/heur_mutation.h>
#include <scip/heur_octane.h>
#include <scip/heur_oneopt.h>
#include <scip/heur_rens.h>
#include <scip/heur_rins.h>
#include <scip/heur_rounding.h>
#include <scip/heur_shifting.h>
#include <scip/heur_simplerounding.h>
#include <scip/heur_trivial.h>
#include <scip/heur_trysol.h>
#include <scip/heur_twoopt.h>
#include <scip/heur_undercover.h>
#include <scip/heur_veclendiving.h>
#include <scip/heur_zirounding.h>
#include <scip/heur_actconsdiving.h>
#include <scip/heur_coefdiving.h>
#include <scip/heur_fracdiving.h>
#include <scip/heur_guideddiving.h>
#include <scip/heur_intdiving.h>
#include <scip/heur_linesearchdiving.h>
#include <scip/heur_objpscostdiving.h>
#include <scip/heur_pscostdiving.h>
#include <scip/heur_rootsoldiving.h>

#include <scip/table_default.h>

#include "branchandprice_scip_probdata.hpp"
#include "branchandprice_scip_pricer.hpp"
#include "branchandprice_scip_branch.hpp"
#include "branchandprice_scip_heur_mip.hpp"

#include <sstream>
#include <iostream>

namespace nonnegrank
{

   bool RectangleCoveringBound::doComputeBranchAndPrice(bool abortNoImprovement, double& subtractTime)
   {
      // Check if we shall do anything at all.

      if (abortNoImprovement)
      {
         if (_data.bestLowerBound >= _data.bestUpperBound)
            return false;
      }

      // Check for zero matrix.

      if (_data.maximumEntry() == 0)
      {
         _value = 0.0;
         return true;
      }

      if (_data.verbosity > 0)
      {
         std::cerr << "Computing <" << (_refined ? "refined " : "")
            << "rectangle covering> bound via branch-and-price. " << std::flush;
      }

      // Initialize rectangles.

      std::vector<std::shared_ptr<Submatrix>>* initialRectangles = nullptr;
      if (enumerateRectangles)
      {
         enumerateMaximalRectangles(_data);
         initialRectangles = &_data.maximalRectangles;
      }
      else
      {
         initialRectangles = &_data.usefulRectangles;
      }

      // Compute abortion objective values.

      double abortIfAtLeast = std::numeric_limits<double>::infinity();
      double abortIfAtMost = 0.0;
      if (abortNoImprovement)
      {
         abortIfAtLeast = _data.bestUpperBound - 1.0 + 0.01; // If we reach it, rounding up matches the upper bound.
         abortIfAtMost = _data.bestLowerBound - 0.01; // If we get to this value or below, rounding up cannot improve.
      }

      // Initialize SCIP instance.

      SCIP* scip = NULL;
      SCIP_CALL_ABORT(SCIPcreate(&scip));
      SCIP_CALL_ABORT(SCIPincludeNodeselBfs(scip));
      SCIP_CALL_ABORT(SCIPincludeConshdlrIntegral(scip));
      SCIP_CALL_ABORT(SCIPincludeConshdlrLinear(scip));
      SCIP_CALL_ABORT(SCIPincludeDispDefault(scip));
      SCIP_CALL_ABORT(SCIPincludeTableDefault(scip));
      
      SCIP_CALL_ABORT( SCIPincludeHeurCrossover(scip) );
      SCIP_CALL_ABORT( SCIPincludeHeurDins(scip) );
      SCIP_CALL_ABORT( SCIPincludeHeurFixandinfer(scip) );
      SCIP_CALL_ABORT( SCIPincludeHeurIntshifting(scip) );
      SCIP_CALL_ABORT( SCIPincludeHeurLocalbranching(scip) );
      SCIP_CALL_ABORT( SCIPincludeHeurMutation(scip) );
      SCIP_CALL_ABORT( SCIPincludeHeurOctane(scip) );
      SCIP_CALL_ABORT( SCIPincludeHeurOneopt(scip) );
      SCIP_CALL_ABORT( SCIPincludeHeurRens(scip) );
      SCIP_CALL_ABORT( SCIPincludeHeurRins(scip) );
      SCIP_CALL_ABORT( SCIPincludeHeurRounding(scip) );
      SCIP_CALL_ABORT( SCIPincludeHeurShifting(scip) );
      SCIP_CALL_ABORT( SCIPincludeHeurSimplerounding(scip) );
      SCIP_CALL_ABORT( SCIPincludeHeurTrivial(scip) );
      SCIP_CALL_ABORT( SCIPincludeHeurTrySol(scip) );
      SCIP_CALL_ABORT( SCIPincludeHeurTwoopt(scip) );
      SCIP_CALL_ABORT( SCIPincludeHeurUndercover(scip) );
      SCIP_CALL_ABORT( SCIPincludeHeurVeclendiving(scip) );
      SCIP_CALL_ABORT( SCIPincludeHeurZirounding(scip) );
      SCIP_CALL_ABORT( SCIPincludeHeurActconsdiving(scip) );
      SCIP_CALL_ABORT( SCIPincludeHeurCoefdiving(scip) );
      SCIP_CALL_ABORT( SCIPincludeHeurFracdiving(scip) );
      SCIP_CALL_ABORT( SCIPincludeHeurGuideddiving(scip) );
      SCIP_CALL_ABORT( SCIPincludeHeurIntdiving(scip) );
      SCIP_CALL_ABORT( SCIPincludeHeurLinesearchdiving(scip) );
      SCIP_CALL_ABORT( SCIPincludeHeurObjpscostdiving(scip) );
      SCIP_CALL_ABORT( SCIPincludeHeurPscostdiving(scip) );
      SCIP_CALL_ABORT( SCIPincludeHeurRootsoldiving(scip) );

      auto heur = new RectangleCoveringHeuristicMIP(scip, _data, _refined);
      SCIP_CALL_ABORT( SCIPincludeObjHeur(scip, heur, true) );

      auto probdata = new RectangleCoveringProbdata(scip, _data);
      SCIP_CALL_ABORT(SCIPcreateObjProb(scip, "rectangle-covering", probdata, true));
      SCIP_CALL_ABORT(SCIPsetObjIntegral(scip));
      SCIP_CALL_ABORT(SCIPsetObjsense(scip, SCIP_OBJSENSE_MINIMIZE));
      SCIP_CALL_ABORT(SCIPsetIntParam(scip, "display/verblevel", _data.verbosity > 1 ? 4 : 0));
      SCIP_CALL_ABORT(SCIPsetIntParam(scip, "display/freq", 1));
      SCIP_CALL_ABORT(SCIPsetBoolParam(scip, "misc/catchctrlc", false));

      // Create the constraints.

      SCIP_CALL_ABORT(probdata->createConstraints(_refined));

      // Add initial rectangles.

      for (auto rectangle : *initialRectangles)
         probdata->addRectangle(rectangle);

      // Output IP properties.

      if (_data.verbosity > 1)
      {
         std::cerr << "\n  Initialized IP with " << initialRectangles->size() << " variables and "
            << SCIPgetNOrigConss(scip) << " constraints." << std::endl;
      }

      // Create subproblem solvers.

      std::vector<SubproblemSolver*> subproblemSolvers;
      subproblemSolvers.push_back(new SubproblemSolverEnumerateSmall(_data, _refined, true));
#ifdef WITH_IP_scip
      subproblemSolvers.push_back(new SubproblemSolverSCIP(_data, _refined, true));
#endif
#ifdef WITH_IP_gurobi
      subproblemSolvers.push_back(new SubproblemSolverGurobi(_data, _refined, true));
#endif

      auto pricer = new RectangleCoveringPricer(scip, _data, subproblemSolvers);
      SCIP_CALL_ABORT(SCIPincludeObjPricer(scip, pricer, true));
      SCIP_CALL_ABORT(SCIPactivatePricer(scip, SCIPfindPricer(scip, "rectanglecovering")));

      auto branchrule = new RectangleCoveringBranchrule(scip);
      SCIP_CALL_ABORT(SCIPincludeObjBranchrule(scip, branchrule, true));

      SCIP_CALL_ABORT(SCIPsolve(scip));
//       SCIP_CALL_ABORT(SCIPprintStatistics(scip, stdout));

      _value = SCIPgetPrimalbound(scip);
      if (SCIPgetStatus(scip) != SCIP_STATUS_OPTIMAL)
      {
         std::stringstream ss;
         ss << SCIPgetStatus(scip);
         std::cerr << "WARNING: <" << (_refined ? "refined " : "") << "rectangle covering> bound computation via scip"
            << " terminated with nonoptimal status " << ss.str() << "." << std::endl;
      }

      if (_data.verbosity > 0)
      {
         std::cerr << "done.\n" << std::flush;
      }
      
      SCIP_CALL_ABORT(SCIPfreeProb(scip));
      SCIP_CALL_ABORT(SCIPfree(&scip));

      for (auto solver: subproblemSolvers)
         delete solver;

      return true;
   }

}
