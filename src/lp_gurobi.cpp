#include "lp.hpp"

#include <gurobi_c.h>

#include <cmath>
#include <sstream>
#include <iostream>
#include <cassert>

#include <fcntl.h>
#include <unistd.h>

namespace nonnegrank
{
   struct LPSolver
   {
      GRBenv* env;
      GRBmodel *model;
      std::size_t numRows;
   };

   void LP::initSolver(const std::vector<double>& objective, bool lowerBound)
   {
      _lpSolver = new LPSolver;
      _lpSolver->numRows = 0;

      // Save stdout.

      if (_data.verbosity > 1)
         std::cerr << std::flush;
      int savedStdout = dup(1);
      close(1);
      int newStdout = open("/dev/null", O_WRONLY);
      assert(newStdout == 1);

      // Create the Gurobi model.

      GRBloadenv(&_lpSolver->env, "");
      GRBnewmodel(_lpSolver->env, &_lpSolver->model, nullptr, 0, nullptr, nullptr, nullptr, nullptr, nullptr);

      // Restore stdout.

      close(newStdout);
      newStdout = dup(savedStdout);
      assert(newStdout == 1);
      close(savedStdout);

      GRBsetintparam(GRBgetenv(_lpSolver->model), "outputflag", 0);
      GRBsetintparam(GRBgetenv(_lpSolver->model), "threads", 1);
      GRBsetintattr(_lpSolver->model, "modelsense", GRB_MAXIMIZE);

      auto lb = new double[objective.size()];
      auto ub = new double[objective.size()];
      for (std::size_t i = 0; i < objective.size(); ++i)
      {
         lb[i] = lowerBound ? 0.0 : -GRB_INFINITY;
         ub[i] = 1.0;
      }

      GRBaddvars(_lpSolver->model, objective.size(), 0, nullptr, nullptr, nullptr, const_cast<double*>(&objective[0]),
         lb, ub, nullptr, nullptr);

      delete[] lb;
      delete[] ub;

      if (_foolingPairs)
      {
         auto ub = new double[_data.numFoolingPairs()];
         auto obj = new double[_data.numFoolingPairs()];
         for (std::size_t p = 0; p < _data.numFoolingPairs(); ++p)
         {
            ub[p] = 1.0;
            obj[p] = 2.0;
         }

         GRBaddvars(_lpSolver->model, _data.numFoolingPairs(), 0, nullptr, nullptr, nullptr, obj, nullptr, ub, nullptr,
            nullptr);

         delete[] obj;
         delete[] ub;
      }

      // Actually add the variables.

      GRBupdatemodel(_lpSolver->model);
   }

   void LP::deinitSolver()
   {
      GRBfreemodel(_lpSolver->model);
      GRBfreeenv(_lpSolver->env);
      delete _lpSolver;
      _lpSolver = nullptr;
   }

   LP::Constraint LP::addRectangle(std::shared_ptr<Submatrix> rectangle)
   {
      std::vector<int> indices;
      indices.reserve(rectangle->rows.size() * rectangle->columns.size() + _data.foolingPairs.size());

      for (auto row : rectangle->rows)
      {
         for (auto column : rectangle->columns)
         {
            std::size_t i = _data.denseIndices[row][column];
            indices.push_back(i);
         }
      }

      if (_foolingPairs)
      {
         for (std::size_t p = 0; p < _data.foolingPairs.size(); ++p)
         {
            const FoolingPair& fp = _data.foolingPairs[p];
            assert(rectangle->nonzeros.size() == _data.numNonzeros());
            assert(fp.nonzero1 < rectangle->nonzeros.size());
            assert(fp.nonzero2 < rectangle->nonzeros.size());
            if (rectangle->nonzeros[fp.nonzero1] || rectangle->nonzeros[fp.nonzero2])
               indices.push_back(_data.numNonzeros() + p);
         }
      }

      std::vector<double> coefficients;
      coefficients.resize(indices.size(), 1.0);

      GRBaddconstr(_lpSolver->model, indices.size(), &indices[0], &coefficients[0], '<', 1.0, nullptr);

      _rectangles[_lpSolver->numRows] = rectangle;
      _lpSolver->numRows++;

      return _lpSolver->numRows - 1;
   }

   LP::Constraint LP::mergeEntryVariables(std::size_t v1, std::size_t v2)
   {
      assert(v1 < _data.numNonzeros());
      assert(v2 < _data.numNonzeros());

      int indices[2] = { static_cast<int>(v1), static_cast<int>(v2) };
      double coefficients[2] = { 1.0, -1.0 };

      GRBaddconstr(_lpSolver->model, 2, indices, coefficients, '=', 0.0, nullptr);

      _lpSolver->numRows++;

      return _lpSolver->numRows - 1;
   }

   LP::Constraint LP::mergeFoolingPairVariables(std::size_t p1, std::size_t p2)
   {
      assert(_foolingPairs);
      assert(p1 < _data.numFoolingPairs());
      assert(p2 < _data.numFoolingPairs());

      int indices[2] = { static_cast<int>(_data.numNonzeros() + p1), static_cast<int>(_data.numNonzeros() + p2) };
      double coefficients[2] = { 1.0, -1.0 };

      GRBaddconstr(_lpSolver->model, 2, indices, coefficients, '=', 0.0, nullptr);

      _lpSolver->numRows++;

      return _lpSolver->numRows - 1;
   }

   void LP::splitVariables(const std::vector<Constraint>& constraints)
   {
      auto indices = new int[constraints.size()];
      for (std::size_t i = 0; i < constraints.size(); ++i)
         indices[i] = constraints[i];
      GRBdelconstrs(_lpSolver->model, constraints.size(), indices );
      delete[] indices;
   }

   void LP::run(double abortIfAtLeast, double abortIfAtMost)
   {
      bool resolve = true;
      while (resolve)
      {
         if (_data.verbosity > 1)
         {
            int numColumns;
            GRBgetintattr(_lpSolver->model, "numvars", &numColumns);
            std::cerr << "  Solving " << _lpSolver->numRows << "x" << numColumns << " LP." << std::flush;
         }

         GRBupdatemodel(_lpSolver->model);

         GRBoptimize(_lpSolver->model);

         int status;
         GRBgetintattr(_lpSolver->model, "status", &status);
         if (status != GRB_OPTIMAL)
         {
            std::stringstream ss;
            ss << "Unexpected Gurobi status: ";
            ss << status;
            throw std::runtime_error(ss.str());
         }

         GRBgetdblattr(_lpSolver->model, "objval", &_objectiveValue);

         if (_data.verbosity > 1)
            std::cerr << "obj.value: " << _objectiveValue << ". " << std::flush;

         GRBgetdblattrarray(_lpSolver->model, "x", 0, _data.numNonzeros(), &_nonzeroWeights[0]);

         for (std::size_t i = 0; i < _data.numNonzeros(); ++i)
         {
            if (fabs(_nonzeroWeights[i]) <= _primalEpsilon)
               _nonzeroWeights[i] = 0.0;
         }

         GRBgetdblattrarray(_lpSolver->model, "x", _data.numNonzeros(), _foolingPairWeights.size(),
            &_foolingPairWeights[0]);

         resolve = separate(abortIfAtMost, abortIfAtLeast);
      }
   }

   double LP::getSolution(std::vector<double>& weights, std::vector<double>& foolingPairWeights) const
   {
      weights = _nonzeroWeights;
      foolingPairWeights = _foolingPairWeights;

      double objectiveValue;
      GRBgetdblattr(_lpSolver->model, "objval", &objectiveValue);

      return objectiveValue;
   }

   void LP::getBasicRectangles(std::vector<std::shared_ptr<Submatrix>>& basicRectangles, std::vector<double>& duals)
      const
   {
      duals.clear();
      duals.resize(basicRectangles.size(), 0.0); // We keep old rectangles intact and set the dual value to 0.0.

      int* basis = new int[_lpSolver->numRows];
      double* dual = new double[_lpSolver->numRows];
      GRBgetintattrarray(_lpSolver->model, "cbasis", 0, _lpSolver->numRows, basis);
      GRBgetdblattrarray(_lpSolver->model, "pi", 0, _lpSolver->numRows, dual);

      for (int i = 0; i < _lpSolver->numRows; ++i)
      {
//          if (basis[i] != 0)
//             continue;

         if (dual[i] <= _dualEpsilon)
            dual[i] = 0.0;

         auto got = _rectangles.find(i);
         if (got != _rectangles.end())
         {
            basicRectangles.push_back(got->second);
            duals.push_back(dual[i]);
         }
      }

      delete[] dual;
      delete[] basis;
   }

} /* namespace nonnegrank */
