#include "branchandprice_scip_probdata.hpp"

#include <sstream>

#include <scip/cons_linear.h>

// TODO: remove
#include <iostream>

namespace nonnegrank
{

   RectangleCoveringProbdata::RectangleCoveringProbdata(SCIP* scip, Data& data)
      : enforceHeuristic(false), _scip(scip), _data(data)
   {

   }

   RectangleCoveringProbdata::~RectangleCoveringProbdata()
   {
//       // Release constraints
// 
//       for (std::size_t i = 0; i < _coveringConstraints.size(); ++i)
//       {
//          SCIP_CALL_ABORT(SCIPreleaseCons(_scip, &_coveringConstraints[i]));
//       }
// 
//       for (std::size_t i = 0; i < _foolingPairConstraints.size(); ++i)
//       {
//          SCIP_CALL_ABORT(SCIPreleaseCons(_scip, &_foolingPairConstraints[i]));
//       }
// 
//       for (auto iter : _branchingConstraints)
//       {
//          SCIP_CONS* cons = iter.first;
//          SCIP_CALL_ABORT(SCIPreleaseCons(_scip, &cons));
//       }
// 
//       // Release variables
// 
//       for (auto column : _columns)
//       {
//          SCIP_CALL_ABORT( SCIPreleaseVar(_scip, &column.variable) );
//       }
   }

   SCIP_RETCODE RectangleCoveringProbdata::createConstraints(bool refined)
   {
      // Add initial covering constraints.

      _coveringConstraints.resize(_data.numNonzeros());
      for (std::size_t i = 0; i < _data.numNonzeros(); ++i)
      {
         std::stringstream name;
         name << "cover#" << i;
         SCIP_CALL_ABORT(SCIPcreateConsBasicLinear(_scip, &_coveringConstraints[i], name.str().c_str(), 0,
            nullptr, nullptr, 1.0, SCIPinfinity(_scip)));
         SCIP_CALL_ABORT(SCIPsetConsModifiable(_scip, _coveringConstraints[i], true));
         SCIP_CALL_ABORT(SCIPaddCons(_scip, _coveringConstraints[i]));
      }

      // Add constraints for fooling pairs.

      if (refined)
      {
         _foolingPairConstraints.resize(_data.foolingPairs.size());
         for (std::size_t i = 0; i < _foolingPairConstraints.size(); ++i)
         {
            std::stringstream name;
            name << "fp#" << _data.foolingPairs[i].nonzero1 << "#"
               << _data.foolingPairs[i].nonzero2;
            SCIP_CALL_ABORT(SCIPcreateConsBasicLinear(_scip, &_foolingPairConstraints[i], name.str().c_str(), 0,
               nullptr, nullptr, 2.0, SCIPinfinity(_scip)));
            SCIP_CALL_ABORT(SCIPsetConsModifiable(_scip, _foolingPairConstraints[i], true));
            SCIP_CALL_ABORT(SCIPaddCons(_scip, _foolingPairConstraints[i]));
         }
      }

      return SCIP_OKAY;
   }

   SCIP_RETCODE RectangleCoveringProbdata::scip_copy(SCIP* scip, SCIP* sourcescip, SCIP_HASHMAP* varmap,
      SCIP_HASHMAP* consmap, scip::ObjProbData** objprobdata, unsigned int global, SCIP_RESULT* result)
   {
//       std::cerr << "probdata::scip_copy" << std::endl;

      return scip::ObjProbData::scip_copy(scip, sourcescip, varmap, consmap, objprobdata, global, result);
   }

   SCIP_RETCODE RectangleCoveringProbdata::scip_delorig(SCIP* scip)
   {
//       std::cerr << "probdata::del_orig" << std::endl;

      // Release constraints

      for (std::size_t i = 0; i < _coveringConstraints.size(); ++i)
      {
         SCIP_CALL_ABORT(SCIPreleaseCons(_scip, &_coveringConstraints[i]));
      }

      for (std::size_t i = 0; i < _foolingPairConstraints.size(); ++i)
      {
         SCIP_CALL_ABORT(SCIPreleaseCons(_scip, &_foolingPairConstraints[i]));
      }

      for (auto iter : _branchingConstraints)
      {
         SCIP_CONS* cons = iter.first;
         SCIP_CALL_ABORT(SCIPreleaseCons(_scip, &cons));
      }

      // Release variables

      for (auto column : _columns)
      {
         SCIP_CALL_ABORT( SCIPreleaseVar(_scip, &column.variable) );
      }

      return SCIP_OKAY;
   }

   SCIP_RETCODE RectangleCoveringProbdata::scip_trans(SCIP* scip, scip::ObjProbData** objprobdata,
      unsigned int* deleteobject)
   {
//       std::cerr << "probdata::scip_trans" << std::endl;

      auto transprobdata = new RectangleCoveringProbdata(scip, _data);

      // Transform the variables.

      for (auto column : _columns)
      {
         Column transcolumn;
         transcolumn.rectangle = column.rectangle;
         SCIP_CALL_ABORT(SCIPtransformVar(scip, column.variable, &transcolumn.variable));
//          std::cerr << "Transforming column variable " << SCIPvarGetName(column.variable) << " to "
//             << SCIPvarGetName(transcolumn.variable) << std::endl;
         transprobdata->_columns.push_back(transcolumn);
      }

      // Transform the constraints.

      transprobdata->_coveringConstraints.resize(_coveringConstraints.size());
      for (std::size_t i = 0; i < _coveringConstraints.size(); ++i)
      {
         SCIP_CALL_ABORT(SCIPtransformCons(scip, _coveringConstraints[i], &transprobdata->_coveringConstraints[i]));
      }
      transprobdata->_foolingPairConstraints.resize(_foolingPairConstraints.size());
      for (std::size_t i = 0; i < _foolingPairConstraints.size(); ++i)
      {
         SCIP_CALL_ABORT(SCIPtransformCons(scip, _foolingPairConstraints[i],
            &transprobdata->_foolingPairConstraints[i]));
      }
      for (auto iter : _branchingConstraints)
      {
         SCIP_CONS* transcons;
         SCIP_CALL_ABORT(SCIPtransformCons(scip, iter.first, &transcons));
         transprobdata->_branchingConstraints[transcons] = iter.second;
      }

      *objprobdata = transprobdata;
      *deleteobject = true;

      return SCIP_OKAY;
   }

   SCIP_RETCODE RectangleCoveringProbdata::scip_deltrans(SCIP* scip)
   {
//       std::cerr << "probdata::scip_deltrans" << std::endl;

      // Release constraints

      for (std::size_t i = 0; i < _coveringConstraints.size(); ++i)
      {
         SCIP_CALL_ABORT(SCIPreleaseCons(_scip, &_coveringConstraints[i]));
      }

      for (std::size_t i = 0; i < _foolingPairConstraints.size(); ++i)
      {
         SCIP_CALL_ABORT(SCIPreleaseCons(_scip, &_foolingPairConstraints[i]));
      }

      for (auto iter : _branchingConstraints)
      {
         SCIP_CONS* cons = iter.first;
         SCIP_CALL_ABORT(SCIPreleaseCons(_scip, &cons));
      }

      // Release variables

      for (auto column : _columns)
         SCIP_CALL_ABORT( SCIPreleaseVar(_scip, &column.variable) );

      return SCIP_OKAY;
   }

   void RectangleCoveringProbdata::addRectangle(std::shared_ptr<Submatrix>& rectangle)
   {
      Column col;
      col.rectangle = rectangle;

      // Add variable.

      std::stringstream name;
      name << "rect#rows";
      for (auto row : rectangle->rows)
         name << "#" << row;
      name << "#columns";
      for (auto column : rectangle->columns)
         name << "#" << column;
      SCIP_CALL_ABORT(SCIPcreateVarBasic(_scip, &(col.variable), name.str().c_str(), 0.0, SCIPinfinity(_scip), 1.0,
         SCIP_VARTYPE_INTEGER));
      SCIPvarSetData(col.variable, (SCIP_VARDATA*)(_columns.size()));
      if (SCIPgetStage(_scip) == SCIP_STAGE_SOLVING)
         SCIP_CALL_ABORT(SCIPaddPricedVar(_scip, col.variable, 0.0));
      else
         SCIP_CALL_ABORT(SCIPaddVar(_scip, col.variable));

      // Link it to constraints.

      for (std::size_t i = 0; i < _data.numNonzeros(); ++i)
      {
         if (rectangle->nonzeros[i])
            SCIP_CALL_ABORT(SCIPaddCoefLinear(_scip, _coveringConstraints[i], col.variable, 1.0));
      }

      for (std::size_t i = 0; i < _foolingPairConstraints.size(); ++i)
      {
         const FoolingPair& fp = _data.foolingPairs[i];
         if (rectangle->nonzeros[fp.nonzero1] || rectangle->nonzeros[fp.nonzero2])
            SCIP_CALL_ABORT(SCIPaddCoefLinear(_scip, _foolingPairConstraints[i], col.variable, 1.0));
      }

      for (auto iter : _branchingConstraints)
      {
         SCIP_CONS* cons = iter.first;
         std::shared_ptr<BranchingData>& bd = iter.second;

         bool isSubset = true;
         for (std::size_t j = 0; j < bd->nonzeros.size(); ++j)
         {
            if (!rectangle->nonzeros[bd->nonzeros[j]])
            {
               isSubset = false;
               break;
            }
         }
         if (isSubset)
            SCIP_CALL_ABORT(SCIPaddCoefLinear(_scip, cons, col.variable, 1.0));
      }

      _columns.push_back(col);
   }

   void RectangleCoveringProbdata::getConstraints(std::shared_ptr<Submatrix> rectangle,
      std::vector<std::size_t>& covering, std::vector<std::size_t>& foolingPairs,
      std::vector<SCIP_CONS*>& branching)
   {
      covering.clear();
      for (auto row : rectangle->rows)
      {
         for (auto column : rectangle->columns)
            covering.push_back(_data.denseIndices[row][column]);
      }

      foolingPairs.clear();
      for (std::size_t p = 0; p < numFoolingPairConstraints(); ++p)
      {
         const FoolingPair& fp = _data.foolingPairs[p];
         if (rectangle->nonzeros[fp.nonzero1] || rectangle->nonzeros[fp.nonzero2])
            foolingPairs.push_back(p);
      }

      branching.clear();
      for (auto iter : _branchingConstraints)
      {
         if (!SCIPconsIsActive(iter.first))
            continue;

         bool isSubset = true;
         for (auto b : iter.second->nonzeros)
         {
            if (!rectangle->nonzeros[b])
            {
               isSubset = false;
               break;
            }
         }
         if (isSubset)
            branching.push_back(iter.first);
      }
   }

   void RectangleCoveringProbdata::getConstraints(SCIP_VAR* var, std::vector<std::size_t>& covering,
      std::vector<std::size_t>& foolingPairs, std::vector<SCIP_CONS*>& branching)
   {
      Column& col = _columns[(std::size_t)(SCIPvarGetData(var))];
      assert(col.variable == var);
      getConstraints(col.rectangle, covering, foolingPairs, branching);
   }

} /* namespace nonnegrank */
