#include "data.hpp"

namespace nonnegrank
{

   FoolingSetBound::FoolingSetBound(Data& data)
      : Bound(data, "<fooling set>", true)
   {
#ifdef WITH_FOOLINGSET
      _isSupported = true;
#endif
   }

   FoolingSetBound::~FoolingSetBound()
   {

   }

   void FoolingSetBound::printCertificate(std::ostream& stream)
   {
      if (_wasComputed)
      {
         stream << "# Fooling set as row/column indices:\n";
         for (std::size_t i = 0; i < _foolingSet->rows.size(); ++i)
            stream << _foolingSet->rows[i] << " " << _foolingSet->columns[i] << "\n";
      }
      else
      {
         stream << "# <fooling set> bound was not computed.\n";
      }
   }

#ifndef WITH_FOOLINGSET

   bool FoolingSetBound::doCompute(bool abortNoImprovement, double& subtractTime)
   {
      throw std::runtime_error("<foolingset> bound is not supported. Please configure with the Cliquer library.");
   }

#endif

} /* namespace nonnegrank */
