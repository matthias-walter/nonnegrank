#include "subproblem.hpp"

#include <limits>
#include <cassert>

namespace nonnegrank
{

   SubproblemSolver::SubproblemSolver(const std::string& name, Data& data, bool foolingPairs, bool branchings)
      : _name(name), _data(data), _foolingPairs(foolingPairs), _branchings(branchings), _epsilon(1.0e-3)
   {

   }

   SubproblemSolver::~SubproblemSolver()
   {

   }

   double SubproblemSolver::computeWeight(double constant, const std::vector<double>& nonzeroWeights,
      const std::vector<double>& foolingPairWeights,
      const std::vector<std::shared_ptr<BranchingData>>& branchingWeights, std::shared_ptr<Submatrix>& rectangle)
   {
      _data.setSubmatrixNonzeros(rectangle);

      double weight = constant;
      for (std::size_t r = 0; r < rectangle->rows.size(); ++r)
      {
         for (std::size_t c = 0; c < rectangle->columns.size(); ++c)
         {
            std::size_t v = _data.denseIndices[rectangle->rows[r]][rectangle->columns[c]];
            assert(v != std::numeric_limits<std::size_t>::max());
            weight += nonzeroWeights[v];
         }
      }

      // Fooling pairs

      for (std::size_t i = 0; i < foolingPairWeights.size(); ++i)
      {
         std::size_t v1 = _data.foolingPairs[i].nonzero1;
         std::size_t v2 = _data.foolingPairs[i].nonzero2;
         if (rectangle->nonzeros[v1] || rectangle->nonzeros[v2])
            weight += foolingPairWeights[i];
      }

      // Branchings

      for (std::shared_ptr<BranchingData> bd : branchingWeights)
      {
         bool isSuperset = true;
         for (auto v : bd->nonzeros)
         {
            if (!rectangle->nonzeros[v])
            {
               isSuperset = false;
               break;
            }
         }
         if (isSuperset)
            weight += bd->weight;
      }

      return weight;
   }

} /* namespace nonnegrank */
