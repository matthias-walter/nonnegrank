#include "branchandprice_scip_heur_mip.hpp"

#include <iostream>
#include <sstream>

#include "branchandprice_scip_probdata.hpp"

#include <scip/scipdefplugins.h>
#include <scip/cons_linear.h>

namespace nonnegrank
{

   RectangleCoveringHeuristicMIP::RectangleCoveringHeuristicMIP(SCIP* scip, Data& data, bool refined)
      : ObjHeur(scip, "rc-mip", "Solves MIP with currently priced vars", '#', 1, 1, 0, -1, 
      SCIP_HEURTIMING_AFTERNODE, true), _data(data), _numKnownColumns(0)
   {
      SCIP_CALL_ABORT(SCIPcreate(&_scip));
      SCIP_CALL_ABORT(SCIPcreateProbBasic(_scip, "MIP Heuristic model"));
      SCIP_CALL_ABORT(SCIPsetIntParam(_scip, "display/verblevel", _data.verbosity > 2 ? 4 : 0));
      SCIP_CALL_ABORT(SCIPincludeDefaultPlugins(_scip));
      SCIP_CALL_ABORT(SCIPsetBoolParam(_scip, "misc/catchctrlc", false));
      SCIP_CALL_ABORT(SCIPsetObjsense(_scip, SCIP_OBJSENSE_MINIMIZE));
      SCIP_CALL_ABORT(SCIPsetIntParam(_scip, "separating/aggregation/maxrounds", 0));
      SCIP_CALL_ABORT(SCIPsetIntParam(_scip, "separating/aggregation/maxroundsroot", 0));
//      SCIP_CALL_ABORT(SCIPsetIntParam(_scip, "separating/strongcg/maxrounds", 0));
//      SCIP_CALL_ABORT(SCIPsetIntParam(_scip, "separating/strongcg/maxroundsroot", 0));

      // Create a constraint sum_{R: nz in R} lambda_R >= 1 for every nonzero entry nz of the matrix.

      _nonzeroConstraints.resize(_data.numNonzeros());
      for (std::size_t s = 0; s < _data.numNonzeros(); ++s)
      {
         std::stringstream name;
         name << "nonzero#" << s;
         SCIP_CALL_ABORT(SCIPcreateConsBasicLinear(_scip, &_nonzeroConstraints[s], name.str().c_str(), 0, nullptr, 
            nullptr, 1.0, SCIPinfinity(_scip)));

         SCIP_CALL_ABORT(SCIPaddCons(_scip, _nonzeroConstraints[s]));
      }

      if (refined)
      {
         // Create a constraing sum_{R: s1 in R or s2 in R} lambda_R >= 2 for every fooling pair {s1,s2}.

         _foolingPairConstraints.resize(_data.numFoolingPairs());
         for (std::size_t i = 0; i < _data.numFoolingPairs(); ++i)
         {
            std::stringstream name;
            name << "foolingPair#" << i;
            const FoolingPair& fp = _data.foolingPairs[i];
            SCIP_CALL_ABORT(SCIPcreateConsBasicLinear(_scip, &_foolingPairConstraints[i], name.str().c_str(), 0, 
               nullptr, nullptr, 2.0, SCIPinfinity(_scip)));

            SCIP_CALL_ABORT(SCIPaddCons(_scip, _foolingPairConstraints[i]));
         }
      }
   }

   RectangleCoveringHeuristicMIP::~RectangleCoveringHeuristicMIP()
   {
      for (std::size_t i = 0; i < _foolingPairConstraints.size(); ++i)
      {
         SCIP_CALL_ABORT(SCIPreleaseCons(_scip, &_foolingPairConstraints[i]));
      }
      
      for (std::size_t s = 0; s < _data.numNonzeros(); ++s)
      {
         SCIP_CALL_ABORT(SCIPreleaseCons(_scip, &_nonzeroConstraints[s]));
      }

      for (std::size_t i = 0; i < _columns.size(); ++i)
      {
         SCIP_CALL_ABORT(SCIPreleaseVar(_scip, &_columns[i].variable));
      }

      SCIP_CALL_ABORT(SCIPfree(&_scip));
   }

   SCIP_RETCODE RectangleCoveringHeuristicMIP::scip_exec(SCIP* scip, SCIP_HEUR* heur, SCIP_HEURTIMING heurtiming, 
      unsigned int nodeinfeasible, SCIP_RESULT* result)
   {
      auto probdata = dynamic_cast<RectangleCoveringProbdata*>(SCIPgetObjProbData(scip));
      

      if (probdata->numColumns() < _numKnownColumns * 1.2 && !probdata-> enforceHeuristic)
      {
         *result = SCIP_DIDNOTRUN;
         return SCIP_OKAY;
      }

//       std::cerr << "MIP Heuristic called. Currently, " << probdata->numColumns() << " rectangles are known."
//          << std::endl;

      if (probdata->enforceHeuristic)
      {
//          std::cout << "Enforcing heuristic." << std::endl;
         SCIP_CALL(SCIPsetIntParam(_scip, "branching/pscost/priority", 2000));         
      }
      else
      {
         SCIP_CALL(SCIPsetIntParam(_scip, "branching/pscost/priority", 100000));
      }
      probdata->enforceHeuristic = false;

      for (;_numKnownColumns < probdata->numColumns(); ++_numKnownColumns)
      {
         auto rectangle = probdata->getRectangle(_numKnownColumns);

         // Is this rectangle contained in another or vice versa?

         bool newIsContained = false;
         for (std::size_t j = 0; j < _columns.size(); ++j)
         {
            if (SCIPisZero(_scip, SCIPvarGetUbGlobal(_columns[j].variable)))
               continue;

            bool newIsContained = true;
            bool currentIsContained = true;
            for (std::size_t s = 0; s < _data.numNonzeros(); ++s)
            {
               if (rectangle->nonzeros[s] && !_columns[j].rectangle->nonzeros[s])
                  newIsContained = false;
               else if (!rectangle->nonzeros[s] && _columns[j].rectangle->nonzeros[s])
                  currentIsContained = false;
            }
            if (newIsContained)
               break;

            if (currentIsContained)
            {
               // Disable variable by fixing it to 0.

               SCIP_CALL(SCIPchgVarUbGlobal(_scip, _columns[j].variable, 0.0));
            }
         }

         if (newIsContained)
            continue;

         // We have a non-dominated rectangle, so let's add it.

         Column newColumn;
         newColumn.rectangle = rectangle;
         std::stringstream ss;
         ss << "lambda#" << _numKnownColumns;
         SCIP_CALL(SCIPcreateVarBasic(_scip, &newColumn.variable, ss.str().c_str(), 0.0, 1.0, 1.0, 
            SCIP_VARTYPE_BINARY));
         SCIP_CALL(SCIPaddVar(_scip, newColumn.variable));
         _columns.push_back(newColumn);

         // Set coefficients in constraints.

         for (std::size_t s = 0; s < _data.numNonzeros(); ++s)
         {
            if (rectangle->nonzeros[s])
               SCIP_CALL(SCIPaddCoefLinear(_scip, _nonzeroConstraints[s], newColumn.variable, 1.0));
         }

         for (std::size_t i = 0; i < _foolingPairConstraints.size(); ++i)
         {
            const FoolingPair& fp = _data.foolingPairs[i];
            if (rectangle->nonzeros[fp.nonzero1] || rectangle->nonzeros[fp.nonzero2])
               SCIP_CALL(SCIPaddCoefLinear(_scip, _foolingPairConstraints[i], newColumn.variable, 1.0));
         }
      }

      if (_data.verbosity > 2)
      {
         std::cerr << "\n  Initialized IP with " << _columns.size() << " variables and "
            << SCIPgetNOrigConss(_scip) << " constraints." << std::endl;
      }

      SCIP_CALL(SCIPsolve(_scip));
      
//       SCIP_CALL(SCIPprintStatistics(_scip, stdout));

      *result = SCIP_DIDNOTFIND;

      if (SCIPisLT(scip, SCIPgetPrimalbound(_scip), SCIPgetPrimalbound(scip)))
      {
         SCIP_SOL* heurSolution = SCIPgetBestSol(_scip);
         SCIP_SOL* solution;
         SCIP_CALL(SCIPcreateSol(scip, &solution, heur));
         for (std::size_t i = 0; i < _columns.size(); ++i)
         {
            if (SCIPgetSolVal(_scip, heurSolution, _columns[i].variable) > 0.5)
               SCIP_CALL(SCIPsetSolVal(scip, solution, probdata->getVariable(i), 1.0));
         }
         SCIP_Bool stored;
         SCIP_CALL(SCIPaddSol(scip, solution, &stored));
         if (stored)
            *result = SCIP_FOUNDSOL;
      }

      SCIP_CALL(SCIPfreeSolve(_scip, true));
      SCIP_CALL(SCIPfreeTransform(_scip));
      
      return SCIP_OKAY;
   }

} /* namespace nonnegrank */
