#include <boost/dynamic_bitset.hpp>

#include "data.hpp"
#include "group.hpp"
#include "ganter_common.hpp"
#include "ganter_nextcl_sym_nauty.hpp"

#include <iostream>

namespace nonnegrank
{
   // With symmetries
   void closure_sym(std::vector<std::shared_ptr<Permutation>>& automorphisms, boost::dynamic_bitset<> & A, boost::dynamic_bitset<> & B)
   {
      auto in_A(A);
      auto in_B(B);

      boost::dynamic_bitset<> phi_A(A.size());
      boost::dynamic_bitset<> phi_B(B.size());

      for (std::size_t i = 0; i < automorphisms.size(); ++i)
      {
         auto ithPermutation(automorphisms[i]);
         auto phiRows(ithPermutation->rows);
         auto phiCols(ithPermutation->columns);

         phi_A.reset();
         phi_B.reset();

         phi(in_A,in_B,phi_A,phi_B,phiRows,phiCols);

         if (preccurly(A,phi_A))
         {
            A = phi_A;
            B = phi_B;
         }
      }
      phi_A.clear();
      phi_B.clear();
      // return A;
   }

   void nextClosure_sym(std::vector<std::shared_ptr<Permutation>>& automorphisms,const std::vector<boost::dynamic_bitset<>> & M, const std::vector<boost::dynamic_bitset<>> & M_T, boost::dynamic_bitset<> & A, boost::dynamic_bitset<> & B)
   {
      boost::dynamic_bitset<> CI(A.size());
      boost::dynamic_bitset<> I(A.size());
      boost::dynamic_bitset<> notA(~A);

      auto i = notA.find_first();
      do {
         I = inc(A,i);
         i = notA.find_next(i);
         CI = I;
         closure(M,M_T,CI,B);
         closure_sym(automorphisms,CI,B);
      } while (!sqsubseteq(I,CI));
      A = CI;
   }

   void constructMaximalRectanglesGanterSymmetryNauty(Data& data)
   {
      std::vector<boost::dynamic_bitset<>> M(data.numRows, boost::dynamic_bitset<>(data.numColumns));
      std::vector<boost::dynamic_bitset<>> M_T(data.numColumns, boost::dynamic_bitset<>(data.numRows));

      construct_matrices(data,M,M_T);

      boost::dynamic_bitset<> A(data.numRows);
      boost::dynamic_bitset<> B(data.numColumns);

      std::vector<std::shared_ptr<Permutation>> automorphisms;
      computeAutomorphismGroup(data, automorphisms);
      
      // std::cerr << "done)" << std::endl;

      // std::cerr << std::endl;
      // for (std::size_t i = 0; i < automorphisms.size(); ++i)
      // {
      //    auto ithPermutation(automorphisms[i]);
      //    auto phiRows(ithPermutation->rows);
      //    auto phiCols(ithPermutation->columns);
      //    for (auto j = phiRows.begin(); j != phiRows.end(); ++j ) std::cerr << *j << ' ';
      //    std::cerr << std::endl;
      //    for (auto j = phiCols.begin(); j != phiCols.end(); ++j ) std::cerr << *j << ' ';
      //    std::cerr << std::endl;
      //    std::cerr << std::endl;
      // }

      while(!A.all()) {
         nextClosure_sym(automorphisms, M, M_T, A, B);
         if (B.any())
         {
            pushRectangle(data, data.maximalRectanglesSymmetry, A, B);
            data.setSubmatrixNonzeros(data.maximalRectanglesSymmetry.back());
            // std::cerr << "    " << A << std::endl;
            // std::cerr << "    " << B << std::endl << std::endl;
         }
      }
   }
}
