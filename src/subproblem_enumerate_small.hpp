#ifndef _NONNEGRANK_SUBPROBLEM_ENUMERATE_SMALL_HPP
#define _NONNEGRANK_SUBPROBLEM_ENUMERATE_SMALL_HPP

#include "subproblem.hpp"

namespace nonnegrank
{

   class SubproblemSolverEnumerateSmall: public SubproblemSolver
   {
   public:
      SubproblemSolverEnumerateSmall(Data& data, bool foolingPairs, bool branchings);

      virtual ~SubproblemSolverEnumerateSmall();

      virtual void findPositive(double constant, const std::vector<double>& nonzeroWeights,
         const std::vector<double>& foolingPairWeights,
         const std::vector<std::shared_ptr<BranchingData>>& branchingWeights,
         std::vector<std::shared_ptr<Submatrix>>& rectangles, double& bestWeight, double& upperBound);
   };

}

#endif /* _NONNEGRANK_SUBPROBLEM_ENUMERATE_SMALL_HPP */
