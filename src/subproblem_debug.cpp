#include "subproblem_debug.hpp"

#include <sstream>

namespace nonnegrank
{

   SubproblemSolverDebug::SubproblemSolverDebug(SubproblemSolver* otherSolver, bool successAllowed)
      : SubproblemSolver("debug(" + otherSolver->name() + ")", otherSolver->_data, otherSolver->_foolingPairs,
      otherSolver->_branchings), allowSuccess(successAllowed), _otherSolver(otherSolver)
   {

   }

   SubproblemSolverDebug::~SubproblemSolverDebug()
   {
      delete _otherSolver;
   }

   void SubproblemSolverDebug::findPositive(double constant, const std::vector<double>& nonzeroWeights,
      const std::vector<double>& foolingPairWeights,
      const std::vector<std::shared_ptr<BranchingData>>& branchingWeights,
      std::vector<std::shared_ptr<Submatrix>>& rectangles, double& bestWeight, double& upperBound)
   {
      _otherSolver->findPositive(constant, nonzeroWeights, foolingPairWeights, branchingWeights, rectangles,
         bestWeight, upperBound);
      if (!allowSuccess && !rectangles.empty())
      {
         std::stringstream ss;
         ss << "Subproblem solver <" << _otherSolver->name() << "> unexpectedly found improving rectangle:\nRows:";
         for (auto row : rectangles[0]->rows)
            ss << " " << row;
         ss << "\nColumns:";
         for (auto column : rectangles[0]->columns)
            ss << " " << column;
         ss << "\n\nThe constant was " << constant << " and the weight vector was:\n ";
         for (double entry : nonzeroWeights)
            ss << " " << entry;
         ss << "\nBest weight was " << bestWeight << ".\n";
         throw std::runtime_error(ss.str());
      }
   }


} /* namespace nonnegrank */
