#include "cliquer_interface.h"

#include <assert.h>

#include <cliquer/cliquerconf.h>
#include <cliquer/graph.h>
#include <cliquer/cliquer.h>

int computeMaximumClique(int numNodes, int numEdges, int* edgeNodes, int* cliqueNodes)
{
   assert(numEdges == 0 || edgeNodes != NULL);
   assert(cliqueNodes != NULL);

   graph_t* graph = graph_new(numNodes);
   for (int i = 0; i < numEdges; ++i)
   {
      GRAPH_ADD_EDGE(graph, edgeNodes[2*i], edgeNodes[2*i+1] );
   }
   ASSERT(graph_test(graph, NULL));

   clique_options opts;
   opts.reorder_function = NULL;
   opts.reorder_map = NULL;
   opts.time_function = NULL;
   opts.user_data = NULL;
   opts.clique_list = NULL;

   set_t result = clique_find_single(graph, 0, 0, FALSE, &opts);

   int node = 0;
   for (int i = 0; i < numNodes; ++i)
   {
      if (SET_CONTAINS_FAST(result, i))
      {
         cliqueNodes[node] = i;
         ++node;
      }
   }
   set_free(result);
   graph_free(graph);

   return node;
}
