#ifndef _NONNEGRANK_BRANCHANDPRICE_SCIP_PROBDATA_HPP
#define _NONNEGRANK_BRANCHANDPRICE_SCIP_PROBDATA_HPP

#undef WITH_SCIP
#define WITH_SCIP // TODO: remove

#include <objscip/objprobdata.h>

#include "data.hpp"
#include "subproblem.hpp"

namespace nonnegrank
{
   struct Column
   {
      std::shared_ptr<Submatrix> rectangle;
      SCIP_VAR* variable;
   };

   class RectangleCoveringProbdata: public scip::ObjProbData
   {
   public:
      RectangleCoveringProbdata(SCIP* scip, Data& data);

      virtual ~RectangleCoveringProbdata();

      SCIP_RETCODE createConstraints(bool refined);

   protected:
      virtual SCIP_RETCODE scip_copy(SCIP* scip, SCIP* sourcescip, SCIP_HASHMAP* varmap, SCIP_HASHMAP* consmap,
         ObjProbData** objprobdata, unsigned int global, SCIP_RESULT* result);

      virtual SCIP_RETCODE scip_delorig(SCIP* scip);

      virtual SCIP_RETCODE scip_trans(SCIP* scip, ObjProbData** objprobdata, unsigned int* deleteobject);

      virtual SCIP_RETCODE scip_deltrans(SCIP* scip);

      friend class RectangleCoveringPricer;

      friend class RectangleCoveringBranchrule;

   public:
      void addRectangle(std::shared_ptr<Submatrix>& rectangle);

      void getConstraints(std::shared_ptr<Submatrix> rectangle, std::vector<std::size_t>& covering, 
         std::vector<std::size_t>& foolingPairs, std::vector<SCIP_CONS*>& branching);

      void getConstraints(SCIP_VAR* var, std::vector<std::size_t>& covering, 
         std::vector<std::size_t>& foolingPairs, std::vector<SCIP_CONS*>& branching);

      inline std::size_t numColumns() const
      {
         return _columns.size();
      }

      inline std::size_t numFoolingPairConstraints() const
      {
         return _foolingPairConstraints.size();
      }

      inline std::shared_ptr<Submatrix> getRectangle(std::size_t column)
      {
         return _columns[column].rectangle;
      }

      inline SCIP_VAR* getVariable(std::size_t column)
      {
         return _columns[column].variable;
      }

   public:
      bool enforceHeuristic;

   protected:
      SCIP* _scip;
      Data& _data;
      std::vector<SCIP_CONS*> _coveringConstraints;
      std::vector<SCIP_CONS*> _foolingPairConstraints;
      std::unordered_map<SCIP_CONS*, std::shared_ptr<BranchingData>> _branchingConstraints;
      std::vector<Column> _columns;
   };

} /* namespace nonnegrank */

#endif /* _NONNEGRANK_BRANCHANDPRICE_SCIP_PROBDATA_HPP */
