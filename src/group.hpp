#ifndef _NONNEGRANK_GROUP_HPP
#define _NONNEGRANK_GROUP_HPP

#include <vector>
#include <memory>

#include "data.hpp"

namespace nonnegrank
{

   struct Permutation
   {
      std::vector<std::size_t> rows;
      std::vector<std::size_t> columns;
   };

	void computeAutomorphismGroup(Data& data, std::vector<std::shared_ptr<Permutation>>& automorphisms);
}

#endif /* _NONNEGRANK_GROUP_HPP */