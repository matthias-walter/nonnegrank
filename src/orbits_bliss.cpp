#include <cassert>
#include <bliss/defs.hh>
#include <bliss/graph.hh>
#include <map>

#include "data.hpp"
#include "orbits.hpp"

#include <iostream>

namespace nonnegrank
{
   std::size_t findRepresentative(std::vector<std::size_t>& representatives, std::size_t index)
   {
      std::size_t repr = representatives[index];
      if (repr == index)
         return repr;
      repr = findRepresentative(representatives, repr);
      representatives[index] = repr;
      return repr;
   }

   static void generatorFound(void* param, unsigned int n, const unsigned int* automorphism)
   {
      std::vector<std::size_t>& representatives = *static_cast<std::vector<std::size_t>*>(param);

      for (std::size_t i = 0; i < representatives.size(); ++i)
      {
         std::size_t j = automorphism[i];
         if (i == j)
            continue;
         std::size_t iRepr = findRepresentative(representatives, i);
         std::size_t jRepr = findRepresentative(representatives, j);
         representatives[iRepr] = jRepr;
      }
   }

   void Orbits::compute(Data& data)
   {
      std::map<double, unsigned int> entryToColor;
      unsigned int nextColor = 2;
      bliss::Graph graph(data.numRows + data.numColumns + data.numNonzeros());

      const std::size_t offsetNonzeros = 0;
      const std::size_t offsetRows = data.numNonzeros();
      const std::size_t offsetColumns = data.numNonzeros() + data.numRows;

      for (std::size_t r = 0; r < data.numRows; ++r)
         graph.change_color(offsetRows + r, 0);
      for (std::size_t c = 0; c < data.numColumns; ++c)
         graph.change_color(offsetColumns + c, 1);
      for (std::size_t i = 0; i < data.numNonzeros(); ++i)
      {
         std::size_t r = data.nonzeros[i].row;
         std::size_t c = data.nonzeros[i].column;
         double entry = data.nonzeros[i].entry;
         unsigned int color;
         auto iter = entryToColor.find(entry);
         if (iter != entryToColor.end())
            color = iter->second;
         else
         {
            color = nextColor;
            entryToColor.insert(std::make_pair(entry, color));
            ++nextColor;
         }
         assert(color >= 2);
         graph.change_color(offsetNonzeros + i, color);
         graph.add_edge(offsetNonzeros + i, offsetRows + r);
         graph.add_edge(offsetNonzeros + i, offsetColumns + c);
      }

      bliss::Stats stats;
      graph.find_automorphisms(stats, &generatorFound, &_representatives);

      for (std::size_t i = 0; i < _representatives.size(); ++i)
         findRepresentative(_representatives, i);
   }

} /* namespace nonnegrank */
