#ifndef _NONNEGRANK_SUBPROBLEM_HPP
#define _NONNEGRANK_SUBPROBLEM_HPP

#include "data.hpp"

namespace nonnegrank
{
   struct BranchingData
   {
      bool covering;
      std::vector<std::size_t> nonzeros;
      double weight;
   };

   class SubproblemSolver
   {
   public:
      SubproblemSolver(const std::string& name, Data& data, bool foolingPairs, bool branchings);

      virtual ~SubproblemSolver();

      virtual void findPositive(double constant, const std::vector<double>& nonzeroWeights,
         const std::vector<double>& foolingPairWeights,
         const std::vector<std::shared_ptr<BranchingData>>& branchingWeights,
         std::vector<std::shared_ptr<Submatrix>>& rectangles, double& bestWeight, double& upperBound) = 0;

      inline const std::string& name() const
      {
         return _name;
      }

   protected:
      double computeWeight(double constant, const std::vector<double>& nonzeroWeights,
         const std::vector<double>& foolingPairWeights,
         const std::vector<std::shared_ptr<BranchingData>>& branchingWeights, std::shared_ptr<Submatrix>& rectangle);

   protected:
      std::string _name;
      Data& _data;
      bool _foolingPairs;
      bool _branchings;
      double _epsilon;

      friend class SubproblemSolverDebug;
   };

}

#endif /* _NONNEGRANK_SUBPROBLEM_HPP */
