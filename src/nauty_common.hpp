#ifndef _NONNEGRANK_NAUTY_COMMON_HPP
#define _NONNEGRANK_NAUTY_COMMON_HPP

#include <vector>
#include <map>

#include "nauty/nauty.h"
#include "nauty/naugroup.h"

#include "data.hpp"
#include "group.hpp"

namespace nonnegrank
{
   struct Param
   {
      std::size_t numRows;
      std::size_t numColumns;
      std::vector<std::shared_ptr<Permutation>>& automorphisms;
   };

   void getMidNodeGraph(graph* g,Data & data,const std::size_t m, int* lab, int* ptn);

} /* namespace nonnegrank */

#endif /* _NONNEGRANK_NAUTY_COMMON_HPP */
