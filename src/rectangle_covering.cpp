#include <cassert>
#include <iostream>
#include <limits>

#include "data.hpp"

namespace nonnegrank
{

   RectangleCoveringBound::RectangleCoveringBound(Data& data, bool refined)
      : Bound(data, refined ? "<refined rectangle covering>" : "<rectangle covering>", true),
      enumerateRectangles(false), _refined(refined)
   {
      _isSupported = false;
#ifdef WITH_IP_scip
      _isSupported = true;
#endif
#ifdef WITH_IP_gurobi
      _isSupported = true;
#endif
      _wasComputed = data.numNonzeros() == 0;
   }

   RectangleCoveringBound::~RectangleCoveringBound()
   {

   }

   void RectangleCoveringBound::printCertificate(std::ostream& stream)
   {
      if (wasComputed())
      {
         stream << "# Rectangle covering bound due to the smallest rectangle covering\n";
         stream << "# (#rows, #columns, rows and columns for each rectangle).\n";
         for (std::size_t i = 0; i < _rectangles.size(); ++i)
         {
            const Submatrix& rectangle = *_rectangles[i];
            stream << rectangle.rows.size() << " " << rectangle.columns.size() << "\n";
            for (auto row : rectangle.rows)
               stream << " " << row;
            stream << "\n";
            for (auto column : rectangle.columns)
               stream << " " << column;
            stream << "\n";
         }
      }
      else
      {
         stream << "# Rectangle covering bound was not computed.\n";
      }
   }

   bool RectangleCoveringBound::doCompute(bool abortNoImprovement, double& subtractTime)
   {
#if !defined(WITH_IP) || (!defined(WITH_ENUMERATION) && !defined(WITH_BRANCHANDPRICE))
      throw std::runtime_error(_name + " bound is not supported. Please configure with an IP solver and rectangle \
enumeration or branchandprice.");
#endif
      
      if (enumerateRectangles)
         return doComputeEnumeration(abortNoImprovement, subtractTime);
      else
         return doComputeBranchAndPrice(abortNoImprovement, subtractTime);
   }

#if !defined(WITH_ENUMERATION)
   bool RectangleCoveringBound::doComputeEnumeration(bool abortNoImprovement, double& subtractTime)
   {
      throw std::runtime_error(_name +
         " bound via enumeration is not supported. Please configure with an IP solver and rectangle enumeration.");
   }
#endif

#if !defined(WITH_BRANCHANDPRICE)
   bool RectangleCoveringBound::doComputeBranchAndPrice(bool abortNoImprovement, double& subtractTime)
   {
      throw std::runtime_error(_name +
         " bound via branch-and-price is not supported. Please configure with scip and an IP solver.");
   }
#endif

} /* namespace nonnegrank */
