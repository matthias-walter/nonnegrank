#include "subproblem_gurobi.hpp"

#include <limits>
#include <iostream>
#include <iterator>
#include <iomanip>
#include <sstream>
#include <cassert>
#include <fcntl.h>
#include <unistd.h>

namespace nonnegrank
{
   SubproblemSolverGurobi::SubproblemSolverGurobi(Data& data, bool foolingPairs, bool branchings)
      : SubproblemSolver("gurobi", data, foolingPairs, branchings)
   {
      int savedStdout = dup(1);
      close(1);
      int newStdout = open("/dev/null", O_WRONLY);
      assert(newStdout == 1);

      // Create the Gurobi model.

      GRBloadenv(&_env, "");
      GRBnewmodel(_env, &_model, nullptr, 0, nullptr, nullptr, nullptr, nullptr, nullptr);

      // Restore stdout.

      close(newStdout);
      newStdout = dup(savedStdout);
      assert(newStdout == 1);
      close(savedStdout);

      GRBsetintparam(GRBgetenv(_model), "outputflag", 0);
      GRBsetintparam(GRBgetenv(_model), "threads", 1);
      GRBsetintattr(_model, "modelsense", GRB_MAXIMIZE);

      // Add the variables.

      _firstRowVariable = 0;
      _firstColumnVariable = _data.numRows;
      _firstNonzeroVariable = _firstColumnVariable + _data.numColumns;
      _firstFoolingPairVariable = _firstNonzeroVariable + _data.numNonzeros();
      _numVariables = _firstFoolingPairVariable + (_foolingPairs ? _data.numFoolingPairs() : 0);

      auto obj = new double[_numVariables];
      auto lb = new double[_numVariables];
      auto ub = new double[_numVariables];
      auto vtype = new char[_numVariables];

      // x_r = 1 iff row r belongs to rectangle.
      // y_c = 1 iff column c belongs to rectangle.
      // z_i = 1 iff nonzero i belongs to rectangle.
      // f_{i,j} = 1 iff i or j belongs to the rectangle.

      for (std::size_t i = 0; i < _numVariables; ++i)
      {
         obj[i] = 0.0;
         lb[i] = 0.0;
         ub[i] = 1.0;
         vtype[i] = GRB_BINARY;
      }

      GRBaddvars(_model, _numVariables, 0, nullptr, nullptr, nullptr, obj, lb, ub, vtype, nullptr);

      delete[] vtype;
      delete[] ub;
      delete[] lb;
      delete[] obj;
      
      // Actually add the variables.

      GRBupdatemodel(_model);

      // x_r + y_c <= 1 for zero entry (r,c).

      for (std::size_t  row = 0; row < _data.numRows; ++row)
      {
         for (std::size_t  column = 0; column < _data.numColumns; ++column)
         {
            if (_data.isNonzero(row, column))
               continue;

            int indices[2] = { static_cast<int>(_firstRowVariable + row), static_cast<int>(_firstColumnVariable + 
               column) };
            double coefficients[2] = { 1.0, 1.0 };
            GRBaddconstr(_model, 2, &indices[0], &coefficients[0], '<', 1.0, nullptr);
         }
      }


      // x_r - z_i >= 0 for i'th nonzero (r,c).

      for (std::size_t  i = 0; i < _data.nonzeros.size(); ++i)
      {
         int indices[2] = { static_cast<int>(_firstRowVariable + _data.nonzeros[i].row), 
            static_cast<int>(_firstNonzeroVariable + i) };
         double coefficients[2] = { 1.0, -1.0 };
         GRBaddconstr(_model, 2, &indices[0], &coefficients[0], '>', 0.0, nullptr);
      }


      // y_c - z_i >= 0 for i'th nonzero (r,c).

      for (std::size_t  i = 0; i < _data.nonzeros.size(); ++i)
      {
         int indices[2] = { static_cast<int>(_firstColumnVariable + _data.nonzeros[i].column), 
            static_cast<int>(_firstNonzeroVariable + i) };
         double coefficients[2] = { 1.0, -1.0 };
         GRBaddconstr(_model, 2, &indices[0], &coefficients[0], '>', 0.0, nullptr);
      }

      // x_r + y_c - z_i <= 1 for i'th nonzero (row,column).

      for (std::size_t  i = 0; i < _data.nonzeros.size(); ++i)
      {
         int indices[3] = { static_cast<int>(_firstRowVariable + _data.nonzeros[i].row), 
            static_cast<int>(_firstColumnVariable + _data.nonzeros[i].column), static_cast<int>(_firstNonzeroVariable + 
            i) };
         double coefficients[3] = { 1.0, 1.0, -1.0 };
         GRBaddconstr(_model, 3, &indices[0], &coefficients[0], '<', 1.0, nullptr);
      }

      if (_foolingPairs)
      {
         // p_{i,j} - z_i - z_j <= 0 for nonzeros i and j forming a fooling pair.

         for (std::size_t p = 0; p < _data.foolingPairs.size(); ++p)
         {
            const FoolingPair& fp = _data.foolingPairs[p];
            assert(fp.nonzero1 < std::numeric_limits<std::size_t>::max());
            assert(fp.nonzero2 < std::numeric_limits<std::size_t>::max());

            int indices[3] = { static_cast<int>(_firstNonzeroVariable + fp.nonzero1), 
               static_cast<int>(_firstNonzeroVariable + fp.nonzero2), static_cast<int>(_firstFoolingPairVariable + p) };
            double coefficients[3] = { -1.0, -1.0, 1.0 };
            GRBaddconstr(_model, 3, &indices[0], &coefficients[0], '<', 0.0, nullptr);
         }
      }

      GRBupdatemodel(_model);
   }

   SubproblemSolverGurobi::~SubproblemSolverGurobi()
   {
      GRBfreemodel(_model);
      GRBfreeenv(_env);
   }

   void SubproblemSolverGurobi::findPositive(double constant, const std::vector<double>& nonzeroWeights,
      const std::vector<double>& foolingPairWeights,
      const std::vector<std::shared_ptr<BranchingData>>& branchingWeights,
      std::vector<std::shared_ptr<Submatrix>>& rectangles, double& bestWeight, double& upperBound)
   {
      bestWeight = 0.0;

      // b_B = 1 iff nonzeros in set B are in rectangle.

      std::size_t _firstBranchingVariable = _numVariables;
      _numVariables += branchingWeights.size();
      
      auto obj = new double[branchingWeights.size()];
      auto lb = new double[branchingWeights.size()];
      auto ub = new double[branchingWeights.size()];
      auto vtype = new char[branchingWeights.size()];

      for (std::size_t b = 0; b < branchingWeights.size(); ++b)
      {
         const BranchingData& bd = *branchingWeights[b];
         
         obj[b] = bd.weight;
         lb[b] = 0.0;
         ub[b] = 1.0;
         vtype[b] = GRB_BINARY;
      }

      GRBaddvars(_model, branchingWeights.size(), 0, nullptr, nullptr, nullptr, obj, lb, ub, vtype, nullptr);

      delete[] obj;
      delete[] lb;
      delete[] ub;
      delete[] vtype;

      GRBupdatemodel(_model);

      // Save current number of constraints.
      int _firstBranchingConstraint;
      GRBgetintattr(_model, GRB_INT_ATTR_NUMCONSTRS, &_firstBranchingConstraint);

      // b_B - z_b <= 0 for all b in B.

      for (std::size_t b = 0; b < branchingWeights.size(); ++b)
      {
         const BranchingData& bd = *branchingWeights[b];

         for (std::size_t i = 0; i < bd.nonzeros.size(); ++i)
         {
            int indices[2] = { static_cast<int>(_firstBranchingVariable + b), static_cast<int>(_firstNonzeroVariable + 
               bd.nonzeros[i]) };
            double coefficients[2] = { 1.0, -1.0 };
            GRBaddconstr(_model, 2, &indices[0], &coefficients[0], '<', 0.0, nullptr);
         }
      }

      // z_i + z_j + ... + z_k - b_B <= |B|-1 for B = {i,j,...,k}

      std::vector<int> indices;
      std::vector<double> coefficients;
      for (std::size_t b = 0; b < branchingWeights.size(); ++b)
      {
         const BranchingData& bd = *branchingWeights[b];

         indices.clear();
         for (std::size_t i = 0; i < bd.nonzeros.size(); ++i)
            indices.push_back(_firstNonzeroVariable + bd.nonzeros[i]);
         indices.push_back(_firstBranchingVariable + b);
         coefficients.clear();
         coefficients.resize(indices.size(), 1.0);
         coefficients.back() = -1.0;
         GRBaddconstr(_model, indices.size(), &indices[0], &coefficients[0], '<', bd.nonzeros.size() - 1.0, nullptr);
      }
      
      // Adapt objective vector.
 
      for (std::size_t i = 0; i < nonzeroWeights.size(); ++i)
      {
         GRBsetdblattrelement(_model, GRB_DBL_ATTR_OBJ, _firstNonzeroVariable + i, nonzeroWeights[i]);
      }

      for (std::size_t p = 0; p < foolingPairWeights.size(); ++p)
      {
         GRBsetdblattrelement(_model, GRB_DBL_ATTR_OBJ, _firstFoolingPairVariable + p, foolingPairWeights[p]);
      }

      // We only care about solutions with positive weight.

//       SCIP_CALL_ABORT(SCIPsetObjlimit(_scip, -constant));

      GRBupdatemodel(_model);

      GRBoptimize(_model);

      // Upper bound

      double dualBound;
      GRBgetdblattr(_model, GRB_DBL_ATTR_OBJBOUND, &dualBound);
      if (dualBound < GRB_INFINITY)
         upperBound = dualBound + constant;

      // Solutions

      int numSols;
      GRBgetintattr(_model, GRB_INT_ATTR_SOLCOUNT, &numSols);

      for (int sol = 0; sol < numSols; ++sol)
      {
         GRBsetintparam(_env, GRB_INT_PAR_SOLUTIONNUMBER, sol);
         double objectiveValue;
         GRBgetdblattr(_model, GRB_DBL_ATTR_POOLOBJVAL, &objectiveValue);
         objectiveValue += constant;
         if (objectiveValue < _epsilon)
            continue;

         auto rectangle = std::make_shared<Submatrix>();
         for (std::size_t r = 0; r < _data.numRows; ++r)
         {
            double x;
            GRBgetdblattrelement(_model, GRB_DBL_ATTR_XN, static_cast<int>(_firstRowVariable + r), &x);
            if (x > 0.5)
               rectangle->rows.push_back(r);
         }
         for (std::size_t c = 0; c < _data.numColumns; ++c)
         {
            double x;
            GRBgetdblattrelement(_model, GRB_DBL_ATTR_XN, static_cast<int>(_firstColumnVariable + c), &x);
            if (x > 0.5)
               rectangle->columns.push_back(c);
         }

         // TODO: Check if weights are correct in branch-and-price too. If yes, we could skip this and extract
         // everything from the solution.
         double weight = computeWeight(constant, nonzeroWeights, foolingPairWeights, branchingWeights, rectangle);
//          std::cerr << "Gurobi solution with objective value " << objectiveValue << " yields weight " << weight <<
//             std::endl;
         bestWeight = std::max(bestWeight, weight);
         if (weight > _epsilon)
         {
            rectangles.push_back(rectangle);
         }
      }

//       std::cerr << "Gurobi -> bestWeight = " << bestWeight << ", upperBound = " << upperBound << std::endl;

      // Get current number of constraints.
      int numConstraints;
      GRBgetintattr(_model, GRB_INT_ATTR_NUMCONSTRS, &numConstraints);

      // Remove branching constraints again.
      for (int c = _firstBranchingConstraint; c < numConstraints; ++c)
         GRBdelconstrs(_model, 1, &c);

      for (int v = 0; v < branchingWeights.size(); ++v)
      {
         int var = _firstBranchingVariable + v;
         GRBdelvars(_model, 1, &var);
      }

      GRBupdatemodel(_model);
   }

} /* namespace nonnegrank */
