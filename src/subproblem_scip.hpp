#ifndef _NONNEGRANK_SUBPROBLEM_SCIP_HPP
#define _NONNEGRANK_SUBPROBLEM_SCIP_HPP

#include "subproblem.hpp"

#include <scip/scip.h>

namespace nonnegrank
{

   class SubproblemSolverSCIP: public SubproblemSolver
   {
   public:
      SubproblemSolverSCIP(Data& data, bool foolingPairs, bool branchings);

      virtual ~SubproblemSolverSCIP();

      virtual void findPositive(double constant, const std::vector<double>& nonzeroWeights,
         const std::vector<double>& foolingPairWeights,
         const std::vector<std::shared_ptr<BranchingData>>& branchingWeights,
         std::vector<std::shared_ptr<Submatrix>>& rectangles, double& bestWeight, double& upperBound);

   protected:
      SCIP* _scip;
      std::vector<SCIP_VAR*> _rowVariables;
      std::vector<SCIP_VAR*> _columnVariables;
      std::vector<SCIP_VAR*> _nonzeroVariables;
      std::vector<SCIP_VAR*> _foolingPairVariables;
   };

}

#endif /* _NONNEGRANK_SUBPROBLEM_SCIP_HPP */
