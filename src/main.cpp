#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <algorithm>
#include <map>

#include "nonnegrank.hpp"

void printUsage(const std::string& program)
{
   std::cout << "Usage: " << program << " MATRIX [OPTIONS]\n";
   std::cout << "Options:\n";
   std::cout << "  +sym   Turn on symmetry (default if supported).\n";
   std::cout << "  -sym   Turn off symmetry.\n";
   std::cout << "  +enum  Turn on rectangle enumeration (default if supported).\n";
   std::cout << "  -enum  Turn off rectangle enumeration.\n";
   std::cout << "  +BOUND Explicitly enable BOUND. Disables all that are not explicitly enabled.\n";
   std::cout << "  -BOUND Explicitly disable BOUND. By default, all are enabled.\n";
   std::cout << "Bounds:\n";
   std::cout << "  rk     Rank bound\n";
   std::cout << "  fs     Fooling set bound\n";
   std::cout << "  hsb    Hyperplane separation bound\n";
   std::cout << "  nhsb   Nonnegative hyperplane separation bound\n";
   std::cout << "  frc    Fractional rectangle covering bound\n";
   std::cout << "  frrc   Fractional refined rectangle covering bound\n";
   std::cout << "  rc     Rectangle covering bound\n";
   std::cout << "  rrc    Refined rectangle covering bound\n";
}

int main(int argc, char** argv)
{
   bool optionSymmetry = true;
   bool optionEnumerate = true;
   std::map<std::string, char> optionBounds;
   optionBounds["hsb"] = ' ';
   optionBounds["nhsb"] = ' ';
   optionBounds["frc"] = ' ';
   optionBounds["frrc"] = ' ';
   optionBounds["rc"] = ' ';
   optionBounds["rrc"] = ' ';
   optionBounds["rk"] = ' ';
   optionBounds["fs"] = ' ';
   
   if (argc < 2)
   {
      printUsage(argv[0]);
      return EXIT_FAILURE;
   }
   else if (std::string(argv[1]) == "-h" || std::string(argv[1]) == "--help")
   {
      printUsage(argv[0]);
      return EXIT_SUCCESS;
   }

   for (int a = 2; a < argc; ++a)
   {
      std::string arg = argv[a];
      std::transform(arg.begin(), arg.end(), arg.begin(), ::tolower);
      
      if (arg == "-h" || arg == "--help")
      {
         printUsage(argv[0]);
         return EXIT_SUCCESS;
      }

      if (arg == "+sym" || arg == "+symmetry")
         optionSymmetry = true;
      else if (arg == "-sym" || arg == "-symmetry")
         optionSymmetry = false;
      else if (arg == "+enum" || arg == "+enumerate")
         optionEnumerate = true;
      else if (arg == "-enum" || arg == "-enumerate")
         optionEnumerate = false;
      else
      {
         std::string bound = arg;
         char sign = '+';
         if (arg[0] == '+' || arg[0] == '-')
         {
            bound = arg.substr(1);
            sign = arg[0];
         }
         else
            bound = arg;
         if (optionBounds.find(bound) != optionBounds.end())
            optionBounds[bound] = sign;
         else
         {
            std::cout << "Unknown parameter: " << arg << "\n\n";
            printUsage(argv[0]);
            return EXIT_FAILURE;
         }
      }
   }

   std::vector<std::vector<double>> matrix;

   int numRows, numColumns;
   std::ifstream f(argv[1]);
   f >> numRows >> numColumns;
   if (!f.good())
   {
      std::cout << "Invalid matrix file: " << argv[1] << "\n\n";
      printUsage(argv[0]);
      return EXIT_FAILURE;
   }
   matrix.resize(numRows);
   for (std::size_t r = 0; r < numRows; ++r)
   {
      matrix[r].resize(numColumns);
      for (std::size_t c = 0; c < numColumns; ++c)
         f >> matrix[r][c];
   }
   f.close();

   nonnegrank::Solver solver(matrix);
   solver.verbosity = 2;
   
   // Did we explicitly ask for a bound?
   char defaultOption = '+';
   for (auto& iter: optionBounds)
   {
      if (iter.second == '+')
      {
         defaultOption = '-';
         break;
      }
   }
   for (auto& iter: optionBounds)
   {
      if (iter.second == ' ')
         iter.second = defaultOption;
   }
   
   std::cerr << "Read " << solver.numRows() << "x" << solver.numColumns() << " matrix with " << solver.numNonzeros()
      << (solver.numNonzeros() == 1 ? " nonzero and " : " nonzeros and ") << solver.numFoolingPairs()
      << (solver.numFoolingPairs() == 1 ? " fooling pair." : " fooling pairs.") << std::endl;
      
   solver.hyperplaneSeparationBound.symmetry = optionSymmetry;
   solver.nonnegativeHyperplaneSeparationBound.symmetry = optionSymmetry;
   solver.fractionalRectangleCoveringBound.symmetry = optionSymmetry;
   solver.fractionalRefinedRectangleCoveringBound.symmetry = optionSymmetry;

   solver.hyperplaneSeparationBound.enumerateRectangles = optionEnumerate;
   solver.nonnegativeHyperplaneSeparationBound.enumerateRectangles = optionEnumerate;
   solver.fractionalRectangleCoveringBound.enumerateRectangles = optionEnumerate;
   solver.fractionalRefinedRectangleCoveringBound.enumerateRectangles = optionEnumerate;

   solver.rectangleCoveringBound.enumerateRectangles = optionEnumerate;
   solver.refinedRectangleCoveringBound.enumerateRectangles = optionEnumerate;
   
   if (solver.rankBound.isSupported() && optionBounds["rk"] == '+')
      solver.rankBound.compute();
   if (solver.foolingSetBound.isSupported() &&optionBounds["fs"] == '+')
      solver.foolingSetBound.compute();
   if (solver.nonnegativeHyperplaneSeparationBound.isSupported() && optionBounds["nhsb"] == '+')
      solver.nonnegativeHyperplaneSeparationBound.compute();
   if (solver.hyperplaneSeparationBound.isSupported() && optionBounds["hsb"] == '+')
      solver.hyperplaneSeparationBound.compute();
   if (solver.fractionalRectangleCoveringBound.isSupported() && optionBounds["frc"] == '+')
      solver.fractionalRectangleCoveringBound.compute();
   if (solver.fractionalRefinedRectangleCoveringBound.isSupported() && optionBounds["frrc"] == '+')
      solver.fractionalRefinedRectangleCoveringBound.compute();
   if (solver.rectangleCoveringBound.isSupported() && optionBounds["rc"] == '+')
      solver.rectangleCoveringBound.compute();
   if (solver.refinedRectangleCoveringBound.isSupported() && optionBounds["rrc"] == '+')
      solver.refinedRectangleCoveringBound.compute();

   solver.printAuxiliaryStatistics(std::cerr);
   if (solver.rankBound.wasComputed())
      solver.rankBound.printStatistics(std::cerr);
   if (solver.foolingSetBound.wasComputed())
      solver.foolingSetBound.printStatistics(std::cerr);
   if (solver.nonnegativeHyperplaneSeparationBound.wasComputed())
      solver.nonnegativeHyperplaneSeparationBound.printStatistics(std::cerr);
   if (solver.hyperplaneSeparationBound.wasComputed())
      solver.hyperplaneSeparationBound.printStatistics(std::cerr);
   if (solver.fractionalRectangleCoveringBound.wasComputed())
      solver.fractionalRectangleCoveringBound.printStatistics(std::cerr);
   if (solver.fractionalRefinedRectangleCoveringBound.wasComputed())
      solver.fractionalRefinedRectangleCoveringBound.printStatistics(std::cerr);
   if (solver.rectangleCoveringBound.wasComputed())
      solver.rectangleCoveringBound.printStatistics(std::cerr);
   if (solver.refinedRectangleCoveringBound.wasComputed())
      solver.refinedRectangleCoveringBound.printStatistics(std::cerr);
   
   return EXIT_SUCCESS;
}
