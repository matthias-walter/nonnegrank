#ifndef _NONNEGRANK_GANTER_NEXTCL_SYM_NAUTY_HPP
#define _NONNEGRANK_GANTER_NEXTCL_SYM_NAUTY_HPP

#include <boost/dynamic_bitset.hpp>

#include "data.hpp"
#include "group.hpp"

namespace nonnegrank
{
	// With symmetries
	void closure_sym(std::vector<std::shared_ptr<Permutation>>& automorphisms, boost::dynamic_bitset<> & A, boost::dynamic_bitset<> & B);

   void nextClosure_sym(std::vector<std::shared_ptr<Permutation>>& automorphisms,const std::vector<boost::dynamic_bitset<>> & M, const std::vector<boost::dynamic_bitset<>> & M_T, boost::dynamic_bitset<> & A, boost::dynamic_bitset<> & B);

	void constructMaximalRectanglesGanterSymmetryNauty(Data & data);

} /* namespace nonnegrank */

#endif /* _NONNEGRANK_GANTER_NEXTCL_SYM_NAUTY_HPP */
