#include "nonnegrank.hpp"
#include "data.hpp"

#include <chrono>
#include <limits>
#include <iostream>

namespace nonnegrank
{
   std::ostream& operator<<(std::ostream& stream, const Submatrix& submatrix)
   {
      stream << "[rows:";
      for (auto row: submatrix.rows)
         stream << ' ' << row;
      stream << ", columns:";
      for (auto column: submatrix.columns)
         stream << ' ' << column;
      stream << "]";
      return stream;
   }

   Bound::Bound(Data& data, const std::string& name, bool isLowerBound)
      : _data(data), _name(name), _isLowerBound(isLowerBound), _isSupported(false), _wasComputed(false),
         _value(isLowerBound ? std::numeric_limits<double>::min() : std::numeric_limits<double>::max()), _time(0.0)
   {

   }

   Bound::~Bound()
   {

   }

   bool Bound::wasComputed() const
   {
      return _wasComputed;
   }

   void Bound::printStatistics(std::ostream& stream)
   {
      stream << (_isLowerBound ? "Lower bound " : "Upper bound ") << _name << ": ";
      if (!isSupported())
         stream << "not supported.\n";
      else if (!wasComputed())
         stream << "not computed.\n";
      else
         stream << _value << " (time: " << _time << "s)\n";
   }

   bool Bound::compute(bool abortNoImprovement)
   {
      if (!isSupported())
         throw std::runtime_error(_name + std::string(" bound is not supported!"));
      else if (!wasComputed())
      {
         std::chrono::time_point<std::chrono::system_clock> start, end;
         start = std::chrono::system_clock::now();
         double subtractTime = 0.0;
         _wasComputed = doCompute(abortNoImprovement, subtractTime);
         end = std::chrono::system_clock::now();
         if (wasComputed())
         {
            _time += std::chrono::duration_cast<std::chrono::milliseconds> (end - start).count() / 1000.0
               - subtractTime;
         }
      }
      return wasComputed();
   }

   Solver::Solver(const std::vector<std::vector<double>>& matrix)
      : _data(new Data(matrix, verbosity)),
      rankBound(*_data), foolingSetBound(*_data),
      hyperplaneSeparationBound(*_data, LPBound::HYPERPLANE_SEPARATION),
      nonnegativeHyperplaneSeparationBound(*_data, LPBound::NONNEGATIVE_HYPERPLANE_SEPARATION),
      fractionalRectangleCoveringBound(*_data, LPBound::FRACTIONAL_RECTANGLE_COVERING),
      fractionalRefinedRectangleCoveringBound(*_data, LPBound::FRACTIONAL_REFINED_RECTANGLE_COVERING),
      rectangleCoveringBound(*_data, false), refinedRectangleCoveringBound(*_data, true)
   {

   }

   Solver::Solver(const std::vector<std::size_t>& rows, const std::vector<std::size_t>& columns,
      const std::vector<double>& entries)
      : _data(new Data(rows, columns, entries, verbosity)),
      rankBound(*_data), foolingSetBound(*_data),
      hyperplaneSeparationBound(*_data, LPBound::HYPERPLANE_SEPARATION),
      nonnegativeHyperplaneSeparationBound(*_data, LPBound::NONNEGATIVE_HYPERPLANE_SEPARATION),
      fractionalRectangleCoveringBound(*_data, LPBound::FRACTIONAL_RECTANGLE_COVERING),
      fractionalRefinedRectangleCoveringBound(*_data, LPBound::FRACTIONAL_REFINED_RECTANGLE_COVERING),
      rectangleCoveringBound(*_data, false), refinedRectangleCoveringBound(*_data, true)
   {

   }

   Solver::~Solver()
   {
      delete _data;
   }

   std::size_t Solver::numRows() const
   {
      return _data->numRows;
   }

   std::size_t Solver::numColumns() const
   {
      return _data->numColumns;
   }

   std::size_t Solver::numNonzeros() const
   {
      return _data->numNonzeros();
   }

   std::size_t Solver::numFoolingPairs() const
   {
      return _data->foolingPairs.size();
   }

   void Solver::printAuxiliaryStatistics(std::ostream& stream)
   {
      stream << "Statistics:\n" << std::flush;

      stream << "  Fooling pairs: " << _data->numFoolingPairs() << "\n";// (time: " << _data->enumerateTime<< "s)\n";

      if (_data->orbits->isInitialized())
         stream << "  Orbits: " << _data->orbits->numOrbits()  << " (time: " << _data->orbitsTime << "s)\n";
      if (!_data->maximalRectangles.empty())
      {
         stream << "  Maximal rectangles: " << _data->maximalRectangles.size() << " (time: " << _data->enumerateTime
            << "s)\n";
      }
      if (!_data->maximalRectanglesSymmetry.empty())
      {
         stream << "  Maximal rectangles up to symmetry: " << _data->maximalRectanglesSymmetry.size() << " (time: "
            << _data->enumerateSymmetryTime << "s)\n";
      }
   }

   int Solver::computeAllLowerBounds()
   {
      if (rankBound.isSupported())
         rankBound.compute();
      if (foolingSetBound.isSupported())
         foolingSetBound.compute();

      if (nonnegativeHyperplaneSeparationBound.isSupported())
         nonnegativeHyperplaneSeparationBound.compute();
      if (hyperplaneSeparationBound.isSupported())
         hyperplaneSeparationBound.compute();

      if (fractionalRectangleCoveringBound.isSupported())
         fractionalRectangleCoveringBound.compute();
      if (fractionalRefinedRectangleCoveringBound.isSupported())
         fractionalRefinedRectangleCoveringBound.compute();

      if (rectangleCoveringBound.isSupported())
         rectangleCoveringBound.compute();
      if (refinedRectangleCoveringBound.isSupported())
         refinedRectangleCoveringBound.compute();

      return _data->bestLowerBound;
   }

   int Solver::computeBestLowerBound()
   {
      if (rankBound.isSupported())
         rankBound.compute(true);

      if (hyperplaneSeparationBound.isSupported())
         hyperplaneSeparationBound.compute(true);

      if (refinedRectangleCoveringBound.isSupported())
         refinedRectangleCoveringBound.compute(true);
      else if (rectangleCoveringBound.isSupported())
         rectangleCoveringBound.compute(true);
      else if (fractionalRefinedRectangleCoveringBound.isSupported())
         fractionalRefinedRectangleCoveringBound.compute(true);
      else if (fractionalRectangleCoveringBound.isSupported())
         fractionalRectangleCoveringBound.compute(true);
      else if (!hyperplaneSeparationBound.wasComputed())
         foolingSetBound.compute(true);

      return _data->bestLowerBound;
   }

   double Solver::getCurrentLowerBound() const
   {
      return _data->bestLowerBound;
   }

   int Solver::getCurrentUpperBound() const
   {
      return _data->bestUpperBound;
   }

} /* namespace nonnegrank */

