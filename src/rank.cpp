#include <limits>
#include <iostream>

#ifdef WITH_RANK
#include <Eigen/Dense>
#endif

#include "data.hpp"

namespace nonnegrank
{
   RankBound::RankBound(Data& data)
      : Bound(data, "<rank>", true)
   {
#ifdef WITH_RANK
      _isSupported = true;
#endif
   }

   RankBound::~RankBound()
   {

   }

   void RankBound::printCertificate(std::ostream& stream)
   {
      if (_wasComputed)
      {
         stream << "# Maximum-rank matrix row indices and column indices:\n";
         for (std::size_t i = 0; i < _basisMatrix->rows.size(); ++i)
            stream << (i == 0 ? "" : " ") << _basisMatrix->rows[i];
         stream << "\n";
         for (std::size_t i = 0; i < _basisMatrix->columns.size(); ++i)
            stream << (i == 0 ? "" : " ") << _basisMatrix->columns[i];
         stream << "\n";
      }
      else
      {
         stream << "# <rank> bound was not computed.\n";
      }
   }

#ifndef WITH_RANK

   bool RankBound::doCompute(bool abortNoImprovement, double& subtractTime)
   {
      throw std::runtime_error("<rank> bound is not supported. Please configure with the Eigen3 library.");
   }

#endif

} /* namespace nonnegrank */
