#include "data.hpp"

#include <gurobi_c.h>

#include <sstream>
#include <iostream>

#include <cassert>
#include <fcntl.h>
#include <unistd.h>

namespace nonnegrank
{

   bool FoolingSetBound::doCompute(bool abortNoImprovement, double& subtractTime)
   {
      if (abortNoImprovement)
      {
         if (_data.bestLowerBound >= _data.bestUpperBound)
            return false;
      }

      if (_data.verbosity > 0)
         std::cerr << "Computing <fooling set> bound. " << std::flush;

      if (_data.numNonzeros() == 0)
      {
         if (_data.verbosity > 0)
            std::cerr << " done." << std::endl;
         return true;
      }

      // Init gurobi.

      GRBenv* env;
      GRBmodel *model;

      int savedStdout = dup(1);
      close(1);
      int newStdout = open("/dev/null", O_WRONLY);
      assert(newStdout == 1);

      // Create the Gurobi model.

      GRBloadenv(&env, "");
      GRBnewmodel(env, &model, nullptr, 0, nullptr, nullptr, nullptr, nullptr, nullptr);

      // Restore stdout.

      close(newStdout);
      newStdout = dup(savedStdout);
      assert(newStdout == 1);
      close(savedStdout);

      if (_data.verbosity < 2)
         GRBsetintparam(GRBgetenv(model), "outputflag", 0);
      GRBsetintparam(GRBgetenv(model), "threads", 1);
      GRBsetintattr(model, "modelsense", GRB_MAXIMIZE);

      // Add the variables.

      auto obj = new double[_data.numNonzeros()];
      auto lb = new double[_data.numNonzeros()];
      auto ub = new double[_data.numNonzeros()];
      auto vtype = new char[_data.numNonzeros()];
      for (std::size_t s = 0; s < _data.numNonzeros(); ++s)
      {
         obj[s] = 1.0;
         lb[s] = 0.0;
         ub[s] = 1.0;
         vtype[s] = GRB_BINARY;
      }

      GRBaddvars(model, _data.numNonzeros(), 0, nullptr, nullptr, nullptr, obj, lb, ub, vtype, nullptr);

      delete[] obj;
      delete[] lb;
      delete[] ub;
      delete[] vtype;

      // Actually add the variables.

      GRBupdatemodel(model);

      int indices[2];
      double coefficients[2] = { 1.0, 1.0 };
      std::size_t countConstraints = 0;
      for (std::size_t s1 = 0; s1 < _data.numNonzeros(); ++s1)
      {
         for (std::size_t s2 = s1 + 1; s2 < _data.numNonzeros(); ++s2)
         {
            bool conflict = false;
            std::size_t row1 = _data.nonzeros[s1].row;
            std::size_t column1 = _data.nonzeros[s1].column;
            std::size_t row2 = _data.nonzeros[s2].row;
            std::size_t column2 = _data.nonzeros[s2].column;
            if (row1 == row2 || column1 == column2
               || (_data.isNonzero(row1, column2) && _data.isNonzero(row2, column1)))
            {
               indices[0] = s1;
               indices[1] = s2;
               GRBaddconstr(model, 2, &indices[0], &coefficients[0], '<', 1.0, nullptr);
               ++countConstraints;
            }
         }
      }

      if (_data.verbosity > 1)
      {
         std::cerr << "\n  Initialized IP with " << _data.numNonzeros() << " variables and " << countConstraints
            << " constraints." << std::endl;
      }

      GRBupdatemodel(model);

      GRBoptimize(model);

      int status;
      GRBgetintattr(model, "status", &status);
      if (status != GRB_OPTIMAL)
      {
         std::cerr << "WARNING: <fooling set> bound computation via gurobi terminated with nonoptimal status "
            << status << "." << std::endl;
      }

      GRBgetdblattr(model, "objval", &_value);

      // Extract certificate.

      int size = int(_value + 0.5);
      auto solution = new double[_data.numNonzeros()];
      GRBgetdblattrarray(model, "x", 0, _data.numNonzeros(), solution);
      auto foolingSet = std::make_shared<Submatrix>();
      foolingSet->rows.reserve(size);
      foolingSet->columns.reserve(size);
      for (std::size_t s = 0; s < _data.numNonzeros(); ++s)
      {
         if (solution[s] > 0.5)
         {
            foolingSet->rows.push_back(_data.nonzeros[s].row);
            foolingSet->columns.push_back(_data.nonzeros[s].column);
         }
      }
      _foolingSet = foolingSet;
      delete[] solution;

      GRBfreemodel(model);
      GRBfreeenv(env);

      if (_data.verbosity > 0)
      {
         std::cerr << "done.\n" << std::flush;
      }

      return true;
   }

} /* namespace nonnegrank */
