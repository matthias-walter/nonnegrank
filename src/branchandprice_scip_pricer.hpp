#ifndef _NONNEGRANK_BRANCHANDPRICE_SCIP_PRICER_HPP
#define _NONNEGRANK_BRANCHANDPRICE_SCIP_PRICER_HPP

#include <objscip/objpricer.h>

#include "data.hpp"
#include "subproblem.hpp"

namespace nonnegrank
{
   class RectangleCoveringPricer: public scip::ObjPricer
   {
   public:
      RectangleCoveringPricer(SCIP* scip, Data& data,
         std::vector<SubproblemSolver*>& SubproblemSolvers);

      virtual ~RectangleCoveringPricer();

   protected:
      virtual SCIP_RETCODE scip_price(SCIP* scip, SCIP_PRICER* pricer, double* lowerbound, unsigned int* stopearly,
         bool farkas, SCIP_RESULT* result);

      virtual SCIP_RETCODE scip_redcost(SCIP* scip, SCIP_PRICER* pricer, double* lowerbound, unsigned int* stopearly,
         SCIP_RESULT* result);

      virtual SCIP_RETCODE scip_farkas(SCIP* scip, SCIP_PRICER* pricer, SCIP_RESULT* result);

   protected:
      SCIP* _scip;
      Data& _data;
      std::vector<SubproblemSolver*>& _subproblemSolvers;
   };

} /* namespace nonnegrank */

#endif /* _NONNEGRANK_BRANCHANDPRICE_SCIP_PRICER_HPP */
