#ifndef _NONNEGRANK_LP_HPP
#define _NONNEGRANK_LP_HPP

#include <unordered_map>

#include "data.hpp"
#include "subproblem.hpp"

namespace nonnegrank
{
   struct LPSolver;

   class LP
   {
   public:
      typedef std::size_t Constraint;

      LP(Data& data, const std::vector<double>& objective, bool lowerBound, bool foolingPairs,
         std::vector<SubproblemSolver*>& subproblemSolvers);

      ~LP();

      LP::Constraint addRectangle(std::shared_ptr<Submatrix> rectangle);

      Constraint mergeEntryVariables(std::size_t v1, std::size_t v2);

      Constraint mergeFoolingPairVariables(std::size_t p1, std::size_t p2);

      void splitVariables(const std::vector<Constraint>& mergeConstraint);

      void run(double abortIfAtLeast, double abortIfAtMost);

      double getSolution(std::vector<double>& weights, std::vector<double>& foolingPairWeights) const;

      void getBasicRectangles(std::vector<std::shared_ptr<Submatrix>>& basicRectangles, std::vector<double>& duals)
         const;

   private:
      void initSolver(const std::vector<double>& objective, bool lowerBound);

      void deinitSolver();

      bool  separate(double abortIfAtMost, double abortIfAtLeast);

      std::size_t numEffectiveFoolingPairs() const;

   private:
      Data& _data;
      bool _foolingPairs;
      double _primalEpsilon;
      double _dualEpsilon;
      std::vector<SubproblemSolver*>& _subproblemSolvers;
      double _objectiveValue;
      std::vector<double> _nonzeroWeights;
      std::vector<double> _foolingPairWeights;
      const std::vector<std::shared_ptr<BranchingData>> _branchingWeights;
      std::unordered_map<std::size_t, std::shared_ptr<Submatrix>> _rectangles;
      LPSolver* _lpSolver;
   };

} /* namespace nonnegrank */

#endif /* _NONNEGRANK_LP_HPP */
