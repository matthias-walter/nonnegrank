#include "branchandprice_scip_pricer.hpp"

#include <iostream>
#include <sstream>

#include "branchandprice_scip_probdata.hpp"

#include <scip/cons_linear.h>

namespace nonnegrank
{

   RectangleCoveringPricer::RectangleCoveringPricer(SCIP* scip, Data& data,
      std::vector<SubproblemSolver*>& SubproblemSolvers)
      : scip::ObjPricer(scip, "rectanglecovering", "Pricer for maximal rectangles", 1000000, TRUE), _scip(scip),
      _data(data), _subproblemSolvers(SubproblemSolvers)
   {

   }

   RectangleCoveringPricer::~RectangleCoveringPricer()
   {

   }

   SCIP_RETCODE RectangleCoveringPricer::scip_price(SCIP* scip, SCIP_PRICER* pricer, double* lowerbound,
      unsigned int* stopearly, bool farkas, SCIP_RESULT* result)
   {
      auto probdata = dynamic_cast<RectangleCoveringProbdata*>(SCIPgetObjProbData(scip));

//       std::cerr << "Current LP solution has value " << SCIPgetSolOrigObj(scip, nullptr) << ". " << std::flush;
//       for (std::size_t c = 0; c < probdata->_columns.size(); ++c)
//       {
//          double x;
//          double redcost;
//          x = SCIPgetSolVal(scip, nullptr, probdata->_columns[c].variable);
//          redcost = SCIPgetVarRedcost(scip, probdata->_columns[c].variable);
//          std::cerr << "Rectangle " << * probdata->_columns[c].rectangle << " has lambda " << x << " and redcost = "
//             << redcost << std::endl;
//       }

      // Constant coefficient of column.

      double constCoefficient = farkas ? 0.0 : 1.0;

      // Duals for covering duals.

      std::vector<double> coveringDuals;
      coveringDuals.resize(_data.numNonzeros());
      for (std::size_t i = 0; i < _data.numNonzeros(); ++i)
      {
         coveringDuals[i] = farkas ? SCIPgetDualfarkasLinear(scip, probdata->_coveringConstraints[i]) :
            SCIPgetDualsolLinear(scip, probdata->_coveringConstraints[i]);
      }

      // Duals for fooling pair constraints.

      std::vector<double> foolingPairDuals;
      foolingPairDuals.resize(probdata->_foolingPairConstraints.size());
      for (std::size_t i = 0; i < probdata->_foolingPairConstraints.size(); ++i)
      {
         foolingPairDuals[i] = farkas ? SCIPgetDualfarkasLinear(scip, probdata->_foolingPairConstraints[i]) :
            SCIPgetDualsolLinear(scip, probdata->_foolingPairConstraints[i]);
      }

      // Duals for branching constraints.

      std::vector<std::shared_ptr<BranchingData>> branchingDuals;
      for (auto iter : probdata->_branchingConstraints)
      {
         SCIP_CONS* cons = iter.first;
         if (SCIPconsIsActive(cons))
         {
            iter.second->weight = farkas ? SCIPgetDualfarkasLinear(scip, iter.first) : SCIPgetDualsolLinear(scip,
               iter.first);
            branchingDuals.push_back(iter.second);
         }
         else
            iter.second->weight = 0.0;
      }

      // Query subproblem solvers.

      std::vector<std::shared_ptr<Submatrix>> rectangles;
      for (SubproblemSolver* solver : _subproblemSolvers)
      {
         double bestWeight;
         double upperBound;

//          std::cerr << "Solver " << solver->name() << " queried with " << coveringDuals.size() << " covering duals, "
//             << foolingPairDuals.size() << " fooling pair duals, " << branchingDuals.size() << " branching duals.\n";

         solver->findPositive(farkas ? 0.0 : -1.0, coveringDuals, foolingPairDuals, branchingDuals, rectangles,
            bestWeight, upperBound);

//          std::cerr << " -> Returned " << rectangles.size() << " rectangles." << std::endl;

         if (!rectangles.empty())
            break;
      }

      for (auto rectangle : rectangles)
         probdata->addRectangle(rectangle);
//       {
//          double redcost = computeWeight(_data, farkas ? 0.0 : -1.0, coveringDuals, foolingPairDuals, branchingDuals, 
//             rectangle);
//          std::cerr << "Pricing rectangle " << *rectangle << " with red-cost " << redcost << std::endl;
//          std::cerr << "Nonzeros are";
//          for (std::size_t i = 0; i <_data.numNonzeros(); ++i)
//          {
// //             if (rectangle->nonzeros[i])
//                std::cerr << " " << i;
//          }
         
//       }

      *result = SCIP_SUCCESS;

      return SCIP_OKAY;
   }

   SCIP_RETCODE RectangleCoveringPricer::scip_redcost(SCIP* scip, SCIP_PRICER* pricer, double* lowerbound,
      unsigned int* stopearly, SCIP_RESULT* result)
   {
//       std::cerr << "scip_redcost" << std::endl;

      SCIP_CALL(scip_price(scip, pricer, lowerbound, stopearly, false, result));

      return SCIP_OKAY;
   }

   SCIP_RETCODE RectangleCoveringPricer::scip_farkas(SCIP* scip, SCIP_PRICER* pricer, SCIP_RESULT* result)
   {
//       std::cerr << "scip_farkas" << std::endl;

      SCIP_CALL(scip_price(scip, pricer, nullptr, nullptr, true, result));

      return SCIP_OKAY;
   }

} /* namespace nonnegrank */
