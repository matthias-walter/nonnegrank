#include <chrono>
#include <iostream>
#include <limits>
#include <cassert>

#include "orbits.hpp"

namespace nonnegrank
{

   Orbits::Orbits(Data& data)
      : _data(data), _numOrbits(std::numeric_limits<std::size_t>::max()), _time(0.0)
   {

   }

   Orbits::~Orbits()
   {

   }

   bool Orbits::isInitialized() const
   {
      return _numOrbits < std::numeric_limits<std::size_t>::max();
   }

   bool Orbits::hasSymmetry() const
   {
      assert(_numOrbits < std::numeric_limits<std::size_t>::max());
      return _numOrbits < _data.numNonzeros();
   }

   std::size_t Orbits::findFoolingPairRepresentative(std::size_t index)
   {
      std::size_t repr = _foolingPairRepresentatives[index];
      if (repr == index)
         return repr;
      repr = findFoolingPairRepresentative(repr);
      _foolingPairRepresentatives[index] = repr;
      return repr;
   }

   void Orbits::initialize()
   {
      if (_numOrbits < std::numeric_limits<std::size_t>::max())
         return;

      _representatives.resize(_data.numNonzeros());
      _foolingPairRepresentatives.resize(_data.numFoolingPairs());

#if !defined(WITH_SYMMETRY)

      if (_data.verbosity > 0)
         std::cerr << "Warning: symmetry detection requested but not supported!" << std::endl;

#endif

      if (_data.verbosity > 0)
         std::cerr << "Detecting orbits of matrix symmetry." << std::flush;

      for (std::size_t i = 0; i < _representatives.size(); ++i)
         _representatives[i] = i;

      std::chrono::time_point<std::chrono::system_clock> start = std::chrono::system_clock::now();

#if defined(WITH_SYMMETRY)
      this->compute(_data);

      // Compute representatives for fooling pairs.

      _foolingPairRepresentatives.resize(_data.numFoolingPairs());
      for (std::size_t p = 0; p < _foolingPairRepresentatives.size(); ++p)
         _foolingPairRepresentatives[p] = p;
      for (std::size_t p = 0; p < _foolingPairRepresentatives.size(); ++p)
      {
         for (std::size_t q = p + 1; q < _foolingPairRepresentatives.size(); ++q)
         {
            std::size_t pr = findFoolingPairRepresentative(p);
            std::size_t qr = findFoolingPairRepresentative(q);
            if (pr == qr)
               continue;
            std::size_t pr1 = getRepresentative(_data.foolingPairs[pr].nonzero1);
            std::size_t pr2 = getRepresentative(_data.foolingPairs[pr].nonzero2);
            std::size_t qr1 = getRepresentative(_data.foolingPairs[qr].nonzero1);
            std::size_t qr2 = getRepresentative(_data.foolingPairs[qr].nonzero2);
            if ((pr1 == qr1 && pr2 == qr2) || (pr1 == qr2 && pr2 == qr1))
               _foolingPairRepresentatives[pr] = qr;
         }
      }
      for (std::size_t p = 0; p < _foolingPairRepresentatives.size(); ++p)
         findFoolingPairRepresentative(p);
#endif

      std::chrono::time_point<std::chrono::system_clock> end = std::chrono::system_clock::now();
      _time =  std::chrono::duration_cast<std::chrono::seconds> (end - start).count();

      _numOrbits = 0;
      for (std::size_t i = 0; i < _data.numNonzeros(); ++i)
      {
         if (_representatives[i] == i)
            ++_numOrbits;
         else
         {
            // Entries in the same orbit should at least have the same entry.
            assert(_data.nonzeros[i].entry == _data.nonzeros[_representatives[i]].entry);
         }
      }

      if (_data.verbosity > 0)
      {
         std::cerr << " done: " << _data.numNonzeros() << " nonzeros are in " << numOrbits() 
            << (numOrbits() > 2 ? " orbits." : " orbit.") << std::endl;
      }
   }

}
