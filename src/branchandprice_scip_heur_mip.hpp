#ifndef _NONNEGRANK_BRANCHANDPRICE_SCIP_HEUR_MIP_HPP
#define _NONNEGRANK_BRANCHANDPRICE_SCIP_HEUR_MIP_HPP

#include <objscip/objheur.h>

#include "branchandprice_scip_probdata.hpp"
#include "data.hpp"

namespace nonnegrank
{
   class RectangleCoveringHeuristicMIP: public scip::ObjHeur
   {
   public:
      RectangleCoveringHeuristicMIP(SCIP* scip, Data& data, bool refined);

      virtual ~RectangleCoveringHeuristicMIP();

   protected:
      virtual SCIP_RETCODE scip_exec(SCIP* scip, SCIP_HEUR* heur, SCIP_HEURTIMING heurtiming,
         unsigned int nodeinfeasible, SCIP_RESULT* result);

   protected:
      SCIP* _scip;
      Data& _data;
      std::size_t _numKnownColumns;
      std::vector<Column> _columns;
      std::vector<SCIP_CONS*> _nonzeroConstraints;
      std::vector<SCIP_CONS*> _foolingPairConstraints;
   };

} /* namespace nonnegrank */

#endif /* _NONNEGRANK_BRANCHANDPRICE_SCIP_HEUR_MIP_HPP */
