#include "data.hpp"
#include "enumeration.hpp"

#include <gurobi_c.h>

#include <sstream>
#include <iostream>
#include <cassert>
#include <fcntl.h>
#include <unistd.h>

namespace nonnegrank
{

   bool RectangleCoveringBound::doComputeEnumeration(bool abortNoImprovement, double& subtractTime)
   {
      // Check if we shall do anything at all.

      if (abortNoImprovement)
      {
         if (_data.bestLowerBound >= _data.bestUpperBound)
            return false;
      }

      // Check for zero matrix.

      if (_data.maximumEntry() == 0)
      {
         _value = 0.0;
         return true;
      }

      if (_data.verbosity > 0)
      {
         std::cerr << "Computing <" << (_refined ? "refined " : "")
            << "rectangle covering> bound using maximal rectangles. " << std::flush;
      }

      // Initialize rectangles.

      if (enumerateRectangles)
      {
         enumerateMaximalRectangles(_data);
      }
      else
      {
         throw std::runtime_error(_name +
            " bound via enumeration is not supported. Please configure with an IP solver and rectangle enumeration.");
      }

      GRBenv* env;
      GRBmodel *model;

      int savedStdout = dup(1);
      close(1);
      int newStdout = open("/dev/null", O_WRONLY);
      assert(newStdout == 1);

      // Create the Gurobi model.

      GRBloadenv(&env, "");
      GRBnewmodel(env, &model, nullptr, 0, nullptr, nullptr, nullptr, nullptr, nullptr);

      // Restore stdout.

      close(newStdout);
      newStdout = dup(savedStdout);
      assert(newStdout == 1);
      close(savedStdout);

      if (_data.verbosity < 2)
         GRBsetintparam(GRBgetenv(model), "outputflag", 0);
      GRBsetintparam(GRBgetenv(model), "threads", 1);
      GRBsetintattr(model, "modelsense", GRB_MINIMIZE);

      // Add the variables.

      auto obj = new double[_data.maximalRectangles.size()];
      auto lb = new double[_data.maximalRectangles.size()];
      auto ub = new double[_data.maximalRectangles.size()];
      auto vtype = new char[_data.maximalRectangles.size()];
      for (std::size_t i = 0; i < _data.maximalRectangles.size(); ++i)
      {
         obj[i] = 1.0;
         lb[i] = 0.0;
         ub[i] = _refined ? 2.0 : 1.0;
         vtype[i] = GRB_INTEGER;
      }

      GRBaddvars(model, _data.maximalRectangles.size(), 0, nullptr, nullptr, nullptr, obj, lb, ub, vtype, nullptr);

      delete[] obj;
      delete[] lb;
      delete[] ub;
      delete[] vtype;

      // Actually add the variables.

      GRBupdatemodel(model);

      std::vector<int> indices;
      std::vector<double> coefficients;
      indices.reserve(_data.maximalRectangles.size());
      coefficients.reserve(_data.maximalRectangles.size());
      for (std::size_t s = 0; s < _data.numNonzeros(); ++s)
      {
         indices.clear();
         for (std::size_t r = 0; r != _data.maximalRectangles.size(); ++r)
         {
            auto rectangle = _data.maximalRectangles[r];
            if (rectangle->nonzeros[s])
               indices.push_back(r);
         }
         coefficients.clear();
         coefficients.resize(indices.size(), 1.0);
         GRBaddconstr(model, indices.size(), &indices[0], &coefficients[0], '>', 1.0, nullptr);
      }
   
      if (_refined)
      {
         for (std::size_t i = 0; i < _data.numFoolingPairs(); ++i)
         {
            const FoolingPair& fp = _data.foolingPairs[i];
            indices.clear();
            for (std::size_t r = 0; r != _data.maximalRectangles.size(); ++r)
            {
               auto rectangle = _data.maximalRectangles[r];
               if (rectangle->nonzeros[fp.nonzero1] || rectangle->nonzeros[fp.nonzero2])
                  indices.push_back(r);
            }
            coefficients.clear();
            coefficients.resize(indices.size(), 1.0);
            GRBaddconstr(model, indices.size(), &indices[0], &coefficients[0], '>', 2.0, nullptr);
         }
      }

      if (_data.verbosity > 1)
      {
         std::cerr << "\n  Initialized IP with " << _data.maximalRectangles.size() << " variables and "
            << (_data.numNonzeros() + (_refined ? _data.numFoolingPairs() : 0)) << " constraints." << std::endl;
      }

      GRBupdatemodel(model);

      GRBoptimize(model);

      int status;
      GRBgetintattr(model, "status", &status);
      if (status != GRB_OPTIMAL)
      {
         std::cerr << "WARNING: <" << (_refined ? "refined " : "") << "rectangle covering> bound computation via gurobi"
            << " terminated with nonoptimal status " << status << "." << std::endl;
      }

      GRBgetdblattr(model, "objval", &_value);

      // Extract certificate.

      auto solution = new double[_data.maximalRectangles.size()];
      GRBgetdblattrarray(model, "x", 0, _data.maximalRectangles.size(), solution);
      _rectangles.clear();
      for (std::size_t r = 0; r < _data.maximalRectangles.size(); ++r)
      {
         if (solution[r] > 0.5)
            _rectangles.push_back(_data.maximalRectangles[r]);
         if (solution[r] > 1.5)
            _rectangles.push_back(_data.maximalRectangles[r]);
      }
      delete[] solution;

      GRBfreemodel(model);
      GRBfreeenv(env);

      if (_data.verbosity > 0)
      {
         std::cerr << "done.\n" << std::flush;
      }

      return true;
   }

}