#include "branchandprice_scip_branch.hpp"

#include "branchandprice_scip_probdata.hpp"

#include <set>
#include <cmath>
#include <iostream>
#include <stdexcept>
#include <sstream>

#include <scip/cons_linear.h>

namespace nonnegrank
{

   RectangleCoveringBranchrule::RectangleCoveringBranchrule(SCIP* scip)
      : ObjBranchrule(scip, "rectanglecoveringbranch", "Branching rule for (refined) rectangle covering", 50000, -1,
      1.0)
   {

   }

   RectangleCoveringBranchrule::~RectangleCoveringBranchrule()
   {

   }

   bool RectangleCoveringBranchrule::findBranchingPair(SCIP* scip, RectangleCoveringProbdata* probdata, 
      std::vector<std::size_t>& nonzeros)
   {
      const Data& data = probdata->_data;

      // Setup a matrix that will contain the activity of all pair constraints.
      
      std::vector<std::vector<double> > pairActivity;
      pairActivity.resize(data.numNonzeros());
      for (std::size_t s1 = 0; s1 < data.numNonzeros(); ++s1)
         pairActivity[s1].resize(s1, 0.0);

      // Go through the rectangles and add to its covered pairs.

      for (std::size_t c = 0; c < probdata->numColumns(); ++c)
      {
         Column& col = probdata->_columns[c];
         double lambda = SCIPgetSolVal(scip, nullptr, col.variable);
         if (SCIPisZero(scip, lambda))
            continue;

         for (std::size_t s1 = 0; s1 < data.numNonzeros(); ++s1)
         {
            if (!col.rectangle->nonzeros[s1])
               continue;

            for (std::size_t s2 = 0; s2 < s1; ++s2)
            {
               if (!col.rectangle->nonzeros[s2])
                  continue;

               // R covers s1 and s2.

               pairActivity[s1][s2] += lambda;
            }
         }
      }

      // Find best one, minimizing distance to 0.5.

      std::vector<std::size_t> bestNonzeros;
      double bestDistance = 2.0;
      for (std::size_t s1 = 0; s1 < data.numNonzeros(); ++s1)
      {
         for (std::size_t s2 = 0; s2 < s1; ++s2)
         {
            double distance = fabs(pairActivity[s1][s2] - 0.5);
//             std::cerr << "{" << s2 << "," << s1 << "} have activity " << pairActivity[s1][s2] << std::endl;
            if (distance < bestDistance)
            {
               bestNonzeros.clear();
               bestNonzeros.push_back(s2);
               bestNonzeros.push_back(s1);
               bestDistance = distance;
            }
         }
      }

//       std::cerr << "Minimum distance Branching pair has distance " << bestDistance << std::endl;

      if (SCIPisLT(scip, bestDistance, 0.5))
      {
         nonzeros = bestNonzeros;
         return true;
      }
      else
      {
         nonzeros.clear();
         return false;
      }
   }

   bool RectangleCoveringBranchrule::findBranchingExtension(SCIP* scip, RectangleCoveringProbdata* probdata,    
      std::vector<std::size_t>& nonzeros)
   {
      // Initialize mapping (constraint, nonzero) -> activity.

      std::unordered_map<SCIP_CONS*, std::vector<double>> extensionActivity;
      for (auto iter : probdata->_branchingConstraints)
      {
         auto constraint = iter.first;
         if (!SCIPconsIsActive(constraint) || !iter.second->covering)
            continue;

         extensionActivity[constraint] = std::vector<double>();
         extensionActivity[constraint].resize(probdata->_data.numNonzeros(), 0.0);
      }

      // Go through the rectangles and add to the mapping.

      for (std::size_t c = 0; c < probdata->numColumns(); ++c)
      {
         Column& col = probdata->_columns[c];
         double lambda = SCIPgetSolVal(scip, nullptr, col.variable);
         if (SCIPisZero(scip, lambda))
            continue;

//          std::cerr << "Rectangle " << *col.rectangle << std::endl;

         for (auto & iter : extensionActivity)
         {
            auto constraint = iter.first;
//             std::cerr << "  Considering branching " << iter.first << std::endl;
            std::vector<double>& activities = iter.second;
            const std::shared_ptr<BranchingData>& branchingData = probdata->_branchingConstraints[constraint];
            bool isCovered = true;
            for (std::size_t s : branchingData->nonzeros)
            {
//                std::cerr << "    and branching set nonzero " << s << " at "
//                   << probdata->_data.nonzeros[s].row << "," << probdata->_data.nonzeros[s].column << std::endl;
               if (!col.rectangle->nonzeros[s])
               {
                  isCovered = false;
                  break;
               }
            }
            if (!isCovered)
               continue;

//             std::cerr << "!!!!    Rectangle covers set !!!!" << std::endl;

            for (std::size_t s = 0; s < probdata->_data.numNonzeros(); ++s)
            {
               if (col.rectangle->nonzeros[s])
               {
//                   std::cerr << "activities for " << iter.first << "," << s << " increased by " << lambda << std::endl; 
                  activities[s] += lambda;
               }
            }
         }
      }

      std::vector<std::size_t> bestNonzeros;
      double bestDistance = 2.0;
      for (auto iter : extensionActivity)
      {
         for (std::size_t s = 0; s < probdata->_data.numNonzeros(); ++s)
         {
            double distance = fabs(iter.second[s] - 0.5);
            if (distance < bestDistance && SCIPisLT(scip, distance, 0.5))
            {
               bestNonzeros = probdata->_branchingConstraints[iter.first]->nonzeros;
               bestNonzeros.push_back(s);
               bestDistance = distance;
            }
         }
      }
      
//       std::cerr << "Best branching extension has distance " << bestDistance << std::endl;

      if (SCIPisLT(scip, bestDistance, 0.5))
      {
         nonzeros = bestNonzeros;
         return true;
      }
      else
      {
         nonzeros.clear();
         return false;
      }
   }

   bool RectangleCoveringBranchrule::findBranchingMaximal(SCIP* scip, RectangleCoveringProbdata* probdata,
      std::vector<std::size_t>& nonzeros)
   {
      nonzeros.clear();
      double bestLambda = 0.0;

      for (std::size_t c1 = 0; c1 < probdata->numColumns(); ++c1)
      {
         Column& col1 = probdata->_columns[c1];
         double lambda1 = SCIPgetSolVal(scip, nullptr, col1.variable);

         if (!SCIPisPositive(scip, lambda1) || !SCIPisLT(scip, lambda1, 1.0))
            continue;

//          std::cerr << "lambda#" << c1 << " = " << lambda1 << std::endl;

         bool isMaximal = true;
         for (std::size_t c2 = 0; c2 < probdata->numColumns(); ++c2)
         {
            if (c2 == c1)
               continue;

            Column& col2 = probdata->_columns[c2];
            double lambda2 = SCIPgetSolVal(scip, nullptr, col2.variable);

            if (!SCIPisPositive(scip, lambda2) || !SCIPisLT(scip, lambda2, 1.0))
               continue;

//             std::cerr << "  lambda#" << c2 << " = " << lambda2 << std::endl;
            
            // Check if one rectangle contains the other one.

            bool firstSubsetOfSecond = true;
            for (std::size_t s = 0; s < probdata->_data.numNonzeros(); ++s)
            {
               if (col1.rectangle->nonzeros[s] && !col2.rectangle->nonzeros[s])
               {
                  firstSubsetOfSecond = false;
                  break;
               }
            }
            if (firstSubsetOfSecond)
            {
               isMaximal = false;
               break;
            }
         }

         if (isMaximal && lambda1 > bestLambda)
         {
//             std::cerr << "Rectangle " << *col1.rectangle << " is maximal with lambda = " << lambda1 << std::endl;
            nonzeros.clear();
            for (std::size_t s = 0; s < probdata->_data.numNonzeros(); ++s)
            {
               if (col1.rectangle->nonzeros[s])
                  nonzeros.push_back(s);
            }
            bestLambda = lambda1;
         }
      }

      return !nonzeros.empty();
   }

   SCIP_RETCODE RectangleCoveringBranchrule::scip_execlp(SCIP* scip, SCIP_BRANCHRULE* branchrule,
      unsigned int allowaddcons, SCIP_RESULT* result)
   {
      *result = SCIP_DIDNOTRUN;

      auto probdata = dynamic_cast<RectangleCoveringProbdata*>(SCIPgetObjProbData(scip));
      std::vector<std::size_t> branchingSet;

      if (branchingSet.empty())
         findBranchingPair(scip, probdata, branchingSet);

      if (branchingSet.empty())
         findBranchingExtension(scip, probdata, branchingSet);

      if (branchingSet.empty())
         findBranchingMaximal(scip, probdata, branchingSet);

      // If none of the above works we know that the optimal fractional solution can be slightly changed to yield an 
      // integral one. Hence, we enforce the heuristic that solves an IP on the current variables and postpone branching
      // by creating an identical child node.

      if (branchingSet.empty())
      {
         probdata->enforceHeuristic = true;
         SCIP_NODE* child;
         SCIP_CALL(SCIPcreateChild(scip, &child, 0.0, SCIPgetLocalTransEstimate(scip)));
         return SCIP_OKAY;
      }

      // Carry out the branching.

      auto branchZero = std::make_shared<BranchingData>();
      branchZero->covering = false;
      branchZero->weight = std::numeric_limits<double>::signaling_NaN();
      branchZero->nonzeros = branchingSet;
      auto branchCover = std::make_shared<BranchingData>();
      branchCover->covering = true;
      branchCover->weight = std::numeric_limits<double>::signaling_NaN();
      branchCover->nonzeros = branchingSet;

//       std::cerr << "Found a branching vector:";
      std::stringstream name;
      for (auto b : branchingSet)
      {
         name << '#' << b;
//          std::cerr << ' ' << b << "(" << data.nonzeros[b].row << "," << 
//             data.nonzeros[b].column << ")";
      }
//       std::cerr << std::endl;

      SCIP_NODE* childCover;
      SCIP_NODE* childZero;
      SCIP_CALL(SCIPcreateChild(scip, &childCover, 0.0, SCIPgetLocalTransEstimate(scip)));
      SCIP_CALL(SCIPcreateChild(scip, &childZero, 0.0, SCIPgetLocalTransEstimate(scip)));

      SCIP_CONS* consCover;
      SCIP_CONS* consZero;
      SCIP_CALL(SCIPcreateConsBasicLinear(scip, &consCover, ("cover" + name.str()).c_str(), 0, nullptr, nullptr, 1.0, 
         SCIPinfinity(scip)));
      SCIP_CALL(SCIPcreateConsBasicLinear(scip, &consZero, ("zero" + name.str()).c_str(), 0, nullptr, nullptr, 
         -SCIPinfinity(scip), 0.0));
      SCIPsetConsModifiable(scip, consCover, true);
      SCIPsetConsModifiable(scip, consZero, true);
      SCIPsetConsLocal(scip, consCover, true);
      SCIPsetConsLocal(scip, consZero, true);

      // Add coefficients for existing rectangle variables.

      for (std::size_t c = 0; c < probdata->_columns.size(); ++c)
      {
         const Column& column = probdata->_columns[c];
         bool subset = true;
         for (auto i : branchingSet)
         {
            if (!column.rectangle->nonzeros[i])
            {
               subset = false;
               break;
            }
         }
         if (subset)
         {
//             std::cerr << "Rectangle " << *column.rectangle << " receives a nonzero coefficient." << std::endl;
            SCIP_CALL(SCIPaddCoefLinear(scip, consCover, column.variable, 1.0));
            SCIP_CALL(SCIPaddCoefLinear(scip, consZero, column.variable, 1.0));
         }
      }

      // We do not release them since we store them in our data structures.

      probdata->_branchingConstraints.insert(std::make_pair(consCover, branchCover));
      probdata->_branchingConstraints.insert(std::make_pair(consZero, branchZero));

      SCIP_CALL(SCIPaddConsNode(scip, childCover, consCover, nullptr));
      SCIP_CALL(SCIPaddConsNode(scip, childZero, consZero, nullptr));

      *result = SCIP_BRANCHED;
  
      return SCIP_OKAY;
   }

} /* namespace nonnegrank */