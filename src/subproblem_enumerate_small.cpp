#include "subproblem_enumerate_small.hpp"

#include <limits>
#include <cassert>
#include <iostream>

namespace nonnegrank
{

   SubproblemSolverEnumerateSmall::SubproblemSolverEnumerateSmall(Data& data, bool foolingPairs, bool branchings)
      : SubproblemSolver("enum-small", data, foolingPairs, branchings)
   {

   }

   SubproblemSolverEnumerateSmall::~SubproblemSolverEnumerateSmall()
   {

   }

   void SubproblemSolverEnumerateSmall::findPositive(double constant, const std::vector<double>& nonzeroWeights,
      const std::vector<double>& foolingPairWeights,
      const std::vector<std::shared_ptr<BranchingData>>& branchingWeights,
      std::vector<std::shared_ptr<Submatrix>>& rectangles, double& bestWeight, double& upperBound)
   {
      bestWeight = 0.0; // Empty rectangle

      // We add fooling-pair weights to the respective nonzeros. If both are are included, the total weight was
      // overestimated, but we filter false positives.

      // Sum up positive entries row- and column-wise.

      std::vector<double> sumPositiveByRow(_data.numRows, 0.0);
      std::vector<double> sumPositiveByColumn(_data.numColumns, 0.0);
      for (std::size_t i = 0; i < _data.numNonzeros(); ++i)
      {
         double value = nonzeroWeights[i];
         if (value > 0.0)
         {
            sumPositiveByRow[_data.nonzeros[i].row] += value;
            sumPositiveByColumn[_data.nonzeros[i].column] += value;
         }
      }

      // Add fooling-pair weights on top.

      if (_foolingPairs)
      {
         for (std::size_t i = 0; i < foolingPairWeights.size(); ++i)
         {
            sumPositiveByRow[_data.nonzeros[_data.foolingPairs[i].nonzero1].row] += foolingPairWeights[i];
            sumPositiveByRow[_data.nonzeros[_data.foolingPairs[i].nonzero2].row] += foolingPairWeights[i];
            sumPositiveByColumn[_data.nonzeros[_data.foolingPairs[i].nonzero1].column] += foolingPairWeights[i];
            sumPositiveByColumn[_data.nonzeros[_data.foolingPairs[i].nonzero2].column] += foolingPairWeights[i];
         }
      }

      // Consider rectangles consisting of 1 row.

      for (std::size_t r = 0; r < _data.numRows; ++r)
      {
         double estimate = sumPositiveByRow[r] + constant;
         if (estimate > _epsilon)
         {
            auto rectangle = std::make_shared<Submatrix>();
            rectangle->rows.push_back(r);
            for (std::size_t c = 0; c < _data.numColumns; ++c)
            {
               std::size_t v = _data.denseIndices[r][c];
               if (v < std::numeric_limits<std::size_t>::max() && nonzeroWeights[v] >= 0.0)
                  rectangle->columns.push_back(c);
            }
            assert(_data.isRectangle(rectangle));
            double weight = computeWeight(constant, nonzeroWeights, foolingPairWeights, branchingWeights, rectangle);
            if (weight > _epsilon)
            {
               if (weight > bestWeight)
                  bestWeight = weight;
               rectangles.push_back(rectangle);
            }
         }
      }

      // Consider rectangles consisting of 1 column.

      for (std::size_t c = 0; c < _data.numColumns; ++c)
      {
         double estimate = sumPositiveByColumn[c] + constant;
         if (estimate > _epsilon)
         {
            auto rectangle = std::make_shared<Submatrix>();
            rectangle->columns.push_back(c);
            for (std::size_t r = 0; r < _data.numRows; ++r)
            {
               std::size_t v = _data.denseIndices[r][c];
               if (v < std::numeric_limits<std::size_t>::max() && nonzeroWeights[v] >= 0.0)
                  rectangle->rows.push_back(r);
            }
            assert(_data.isRectangle(rectangle));
            double weight = computeWeight(constant, nonzeroWeights, foolingPairWeights, branchingWeights, rectangle);
            if (weight > _epsilon)
            {
               if (weight > bestWeight)
                  bestWeight = weight;
               rectangles.push_back(rectangle);
            }
         }
      }

      if (!rectangles.empty())
         return;

      // Consider rectangles consisting of 2 rows.

      std::vector<std::size_t> columns;
      for (std::size_t r1 = 0; r1 < _data.numRows; ++r1)
      {
         for (std::size_t r2 = r1 + 1; r2 < _data.numRows; ++r2)
         {
            if (sumPositiveByRow[r1] + sumPositiveByRow[r2] + constant <= _epsilon)
               continue;

            columns.clear();
            double estimate = constant;
            for (std::size_t c = 0; c < _data.numColumns; ++c)
            {
               std::size_t v1 = _data.denseIndices[r1][c];
               std::size_t v2 = _data.denseIndices[r2][c];

               if (v1 == std::numeric_limits<std::size_t>::max())
                  continue;
               if (v2 == std::numeric_limits<std::size_t>::max())
                  continue;
               double contribution = nonzeroWeights[v1] + nonzeroWeights[v2];
               if (contribution >= 0.0)
               {
                  estimate += contribution;
                  columns.push_back(c);
               }
            }

            if (estimate > _epsilon)
            {
               auto rectangle = std::make_shared<Submatrix>();
               rectangle->rows.push_back(r1);
               rectangle->rows.push_back(r2);
               rectangle->columns.swap(columns);
               assert(_data.isRectangle(rectangle));
               double weight = computeWeight(constant, nonzeroWeights, foolingPairWeights, branchingWeights, rectangle);
               if (weight > _epsilon)
               {
                  if (weight > bestWeight)
                     bestWeight = weight;
                  rectangles.push_back(rectangle);
               }
            }
         }
      }

      if (!rectangles.empty())
         return;

      // Consider rectangles consisting of 2 columns.

      std::vector<std::size_t> rows;
      for (std::size_t c1 = 0; c1 < _data.numColumns; ++c1)
      {
         for (std::size_t c2 = c1 + 1; c2 < _data.numColumns; ++c2)
         {
            if (sumPositiveByColumn[c1] + sumPositiveByColumn[c2] + constant <= _epsilon)
               continue;

            rows.clear();
            double estimate = constant;
            for (std::size_t r = 0; r < _data.numRows; ++r)
            {
               std::size_t v1 = _data.denseIndices[r][c1];
               std::size_t v2 = _data.denseIndices[r][c2];

               if (v1 == std::numeric_limits<std::size_t>::max())
                  continue;
               if (v2 == std::numeric_limits<std::size_t>::max())
                  continue;
               double contribution = nonzeroWeights[v1] + nonzeroWeights[v2];
               if (contribution >= 0.0)
               {
                  estimate += contribution;
                  rows.push_back(r);
               }
            }

            if (estimate > _epsilon)
            {
               auto rectangle = std::make_shared<Submatrix>();
               rectangle->columns.push_back(c1);
               rectangle->columns.push_back(c2);
               rectangle->rows.swap(rows);
               assert(_data.isRectangle(rectangle));
               double weight = computeWeight(constant, nonzeroWeights, foolingPairWeights, branchingWeights, rectangle);
               if (weight > _epsilon)
               {
                  if (weight > bestWeight)
                     bestWeight = weight;
                  rectangles.push_back(rectangle);
               }
            }
         }
      }

      if (!rectangles.empty())
         return;

      // Consider rectangles consisting of 3 rows.

      for (std::size_t r1 = 0; r1 < _data.numRows; ++r1)
      {
         for (std::size_t r2 = r1 + 1; r2 < _data.numRows; ++r2)
         {
            for (std::size_t r3 = r2 + 1; r3 < _data.numRows; ++r3)
            {
               if (sumPositiveByRow[r1] + sumPositiveByRow[r2] + sumPositiveByRow[r3] + constant <= _epsilon)
                  continue;

               columns.clear();
               double estimate = constant;
               for (std::size_t c = 0; c < _data.numColumns; ++c)
               {
                  std::size_t v1 = _data.denseIndices[r1][c];
                  std::size_t v2 = _data.denseIndices[r2][c];
                  std::size_t v3 = _data.denseIndices[r3][c];

                  if (v1 == std::numeric_limits<std::size_t>::max())
                     continue;
                  if (v2 == std::numeric_limits<std::size_t>::max())
                     continue;
                  if (v3 == std::numeric_limits<std::size_t>::max())
                     continue;
                  double contribution = nonzeroWeights[v1] + nonzeroWeights[v2] + nonzeroWeights[v3];
                  if (contribution >= 0.0)
                  {
                     estimate += contribution;
                     columns.push_back(c);
                  }
               }

               if (estimate > _epsilon)
               {
                  auto rectangle = std::make_shared<Submatrix>();
                  rectangle->rows.push_back(r1);
                  rectangle->rows.push_back(r2);
                  rectangle->rows.push_back(r3);
                  rectangle->columns.swap(columns);
                  assert(_data.isRectangle(rectangle));
                  double weight = computeWeight(constant, nonzeroWeights, foolingPairWeights, branchingWeights,
                     rectangle);

                  if (weight > _epsilon)
                  {
                     if (weight > bestWeight)
                        bestWeight = weight;
                     rectangles.push_back(rectangle);
                  }
               }
            }
         }
      }
      if (!rectangles.empty())
         return;

      // Consider rectangles consisting of 3 columns.

      for (std::size_t c1 = 0; c1 < _data.numColumns; ++c1)
      {
         for (std::size_t c2 = c1 + 1; c2 < _data.numColumns; ++c2)
         {
            for (std::size_t c3 = c2 + 1; c3 < _data.numColumns; ++c3)
            {
               if (sumPositiveByColumn[c1] + sumPositiveByColumn[c2] + sumPositiveByColumn[c3] + constant <= _epsilon)
                  continue;

               rows.clear();
               double estimate = constant;
               for (std::size_t r = 0; r < _data.numRows; ++r)
               {
                  std::size_t v1 = _data.denseIndices[r][c1];
                  std::size_t v2 = _data.denseIndices[r][c2];
                  std::size_t v3 = _data.denseIndices[r][c3];

                  if (v1 == std::numeric_limits<std::size_t>::max())
                     continue;
                  if (v2 == std::numeric_limits<std::size_t>::max())
                     continue;
                  if (v3 == std::numeric_limits<std::size_t>::max())
                     continue;
                  double contribution = nonzeroWeights[v1] + nonzeroWeights[v2] + nonzeroWeights[v3];
                  if (contribution >= 0.0)
                  {
                     estimate += contribution;
                     rows.push_back(r);
                  }
               }

               if (estimate > _epsilon)
               {
                  auto rectangle = std::make_shared<Submatrix>();
                  rectangle->columns.push_back(c1);
                  rectangle->columns.push_back(c2);
                  rectangle->columns.push_back(c3);
                  rectangle->rows.swap(rows);
                  assert(_data.isRectangle(rectangle));
                  double weight = computeWeight(constant, nonzeroWeights, foolingPairWeights, branchingWeights,
                     rectangle);

                  if (weight > _epsilon)
                  {
                     if (weight > bestWeight)
                        bestWeight = weight;
                     rectangles.push_back(rectangle);
                  }
               }
            }
         }
      }
   }

} /* namespace nonnegrank */
