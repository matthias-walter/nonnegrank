#include <cassert>
#include <iostream>
#include <limits>
#include <iomanip>
#include <cmath>
#include <chrono>

#include "data.hpp"
#include "lp.hpp"
#include "enumeration.hpp"
#include "subproblem.hpp"
#include "subproblem_debug.hpp"
#include "subproblem_enumerate_small.hpp"

#ifdef WITH_IP_scip
#include "subproblem_scip.hpp"
#endif

#ifdef WITH_IP_gurobi
#include "subproblem_gurobi.hpp"
#endif

namespace nonnegrank
{
   LP::LP(Data& data, const std::vector<double>& objective, bool lowerBound, bool foolingPairs,
      std::vector<SubproblemSolver*>& subproblemSolvers)
      : _data(data), _foolingPairs(foolingPairs), _primalEpsilon(1.0e-8), _dualEpsilon(1.0e-8),
      _subproblemSolvers(subproblemSolvers), _lpSolver(nullptr)
   {
      _nonzeroWeights.resize(objective.size());
      if (foolingPairs)
         _foolingPairWeights.resize(_data.numFoolingPairs());

#ifdef WITH_LP
      initSolver(objective, lowerBound);
#endif
   }

   LP::~LP()
   {
#ifdef WITH_LP
      deinitSolver();
#endif
      assert(_lpSolver == nullptr);
   }

   std::size_t LP::numEffectiveFoolingPairs() const
   {
      return _foolingPairWeights.size();
   }

   bool LP::separate(double abortIfAtMost, double abortIfAtLeast)
   {
      std::vector<std::shared_ptr<Submatrix>> rectangles;

      for (std::size_t p = 0; p < _foolingPairWeights.size(); ++p)
      {
         if (fabs(_foolingPairWeights[p]) <= _primalEpsilon)
            _foolingPairWeights[p] = 0.0;
      }

      if (_subproblemSolvers.empty())
      {
         if (_data.verbosity > 1)
            std::cerr << "-> optimal" << std::endl;

         return false;
      }

      // Early termination because we got below abortIfAtMost?

      if (_objectiveValue <= abortIfAtMost)
      {
         if (_data.verbosity > 1)
            std::cerr << "-> no lower bound improvement possible" << std::endl;

         return false;
      }

      // Solve subproblem.

      double globalBestWeight = -1.0;
      double upperBound = std::numeric_limits<double>::infinity();
      for (auto solver : _subproblemSolvers)
      {
         double bestWeight = -1.0;
         solver->findPositive(-1.0, _nonzeroWeights, _foolingPairWeights, _branchingWeights, rectangles, bestWeight,
            upperBound);
         globalBestWeight = std::max(globalBestWeight, bestWeight);

         if (_data.verbosity > 1)
            std::cerr << solver->name() << ": " << rectangles.size() << " " << std::flush;

         if (rectangles.empty())
            continue;

         if (_objectiveValue / (bestWeight + 1.0) >= abortIfAtLeast)
         {
            double factor = 1.0 / (bestWeight + 1.0);
             // The current matrix scaled by (1.0 / boundMaximumWeight) will induce a maximum weight <= 1.0,
            // i.e., it is feasible.

            for (std::size_t v = 0; v < _nonzeroWeights.size(); ++v)
               _nonzeroWeights[v] *= factor;
            for (std::size_t p = 0; p < _foolingPairWeights.size(); ++p)
               _foolingPairWeights[p] *= factor;

            if (_data.verbosity > 1)
               std::cerr << "-> upper bound tight" << std::endl;

            return false;
         }

         break;
      }

      if (rectangles.empty())
      {
         if (fabs(upperBound - globalBestWeight) > 1.0e-3)
         {
            std::cerr << " [WARNING: last subproblem solver was not exact] " << std::flush;
         }

         if (_data.verbosity > 1)
            std::cerr << "-> optimal" << std::endl;

         return false;
      }

      for (std::size_t i = 0; i < rectangles.size(); ++i)
      {
#ifdef WITH_LP
         addRectangle(rectangles[i]);
#endif
      }

      if (_data.verbosity > 1)
         std::cerr << std::endl;

      return true;
   }


   LPBound::LPBound(Data& data, LPBound::Type type)
      : Bound(data, type == LPBound::NONNEGATIVE_HYPERPLANE_SEPARATION ? "<nonnegative hyperplane separation>" :
      (type == LPBound::HYPERPLANE_SEPARATION ? "<hyperplane separation>" :
      (type == LPBound::FRACTIONAL_RECTANGLE_COVERING ? "<fractional rectangle covering>" :
      "<fractional refined rectangle covering>")), true), _type(type), enumerateRectangles(false), symmetry(true),
      computeDualCertificate(false)
   {
#if !defined(WITH_LP)
      _isSupported = false;
#elif !defined(WITH_IP) && !defined(WITH_ENUMERATION)
      _isSupported = false;
#elif !defined(WITH_ENUMERATION)
      _isSupported = (_type != LPBound::MAXIMAL_HYPERPLANE_SEPARATION && _type !=
         LPBound::NONNEGATIVE_MAXIMAL_HYPERPLANE_SEPARATION);
#else
      _isSupported = true;
#endif

      _wasComputed = data.numNonzeros() == 0;
      if (type == LPBound::FRACTIONAL_REFINED_RECTANGLE_COVERING)
         _pairs = data.foolingPairs;
   }

   LPBound::~LPBound()
   {

   }

   bool LPBound::wasComputed() const
   {
      return _wasComputed && (!_matrices.empty() || !computeDualCertificate);
   }

   void LPBound::getCertificateWeights(std::vector<std::vector<double>>& weightMatrix) const
   {
      weightMatrix.resize(_data.numRows);
      for (std::size_t r = 0; r < _data.numRows; ++r)
      {
         weightMatrix[r].resize(_data.numColumns);
         for (std::size_t c = 0; c < _data.numColumns; ++c)
         {
            weightMatrix[r][c] = -std::numeric_limits<double>::infinity();
         }
      }
      for (std::size_t i = 0; i < _data.numNonzeros(); ++i)
      {
         weightMatrix[_data.nonzeros[i].row][_data.nonzeros[i].column] = _weights[i];
      }
   }

   void LPBound::getCertificateWeights(std::vector<std::size_t>& rows,
      std::vector< std::size_t>& columns, std::vector<double>& weights) const
   {
      rows.resize(_data.numNonzeros());
      columns.resize(_data.numNonzeros());
      weights.resize(_data.numNonzeros());
      for (std::size_t i = 0; i < _data.numNonzeros(); ++i)
      {
         rows[i] = _data.nonzeros[i].row;
         columns[i] = _data.nonzeros[i].column;
         weights[i] = _weights[i];
      }
   }

   void LPBound::getCertificateFoolingPairs(std::vector<FoolingPair>& pairs) const
   {
      pairs = _pairs;
   }

   void LPBound::printCertificate(std::ostream& stream)
   {
      if (_wasComputed)
      {
         stream << "# " << _name << " bound due to weights matrix ('-' for -infinity).\n";
         for (std::size_t r = 0; r < _data.numRows; ++r)
         {
            for (std::size_t c = 0; c < _data.numColumns; ++c)
            {
               if (c > 0)
                  stream << " ";
               std::size_t index = _data.denseIndices[r][c];
               if (index == std::numeric_limits<std::size_t>::max())
                  stream << "     -";
               else
                  stream << std::setw(6) << _weights[index];
            }
            stream << "\n";
         }
      }
      else
      {
         stream << "# " << _name << " bound was not computed.\n";
      }
   }

   bool LPBound::doCompute(bool abortNoImprovement, double& subtractTime)
   {
#if !defined(WITH_LP)
      throw std::runtime_error(_name + " bound is not supported. Please configure with an LP solver.");
#elif !defined(WITH_IP) && !defined(WITH_ENUMERATION)
      if (_type == LPBound::MAXIMAL_HYPERPLANE_SEPARATION || _type ==
         LPBound::NONNEGATIVE_MAXIMAL_HYPERPLANE_SEPARATION)
      {
         throw std::runtime_error(_name +
            " bound is not supported. Please configure with rectangle enumeration");
      }
      else
      {
         throw std::runtime_error(_name +
            " bound is not supported. Please configure with an IP solver or rectangle enumeration");
      }
#elif !defined(WITH_ENUMERATION)
      if (_type == LPBound::MAXIMAL_HYPERPLANE_SEPARATION || _type ==
         LPBound::NONNEGATIVE_MAXIMAL_HYPERPLANE_SEPARATION)
      {
         throw std::runtime_error(_name +
            " bound is not supported. Please configure with rectangle enumeration");
      }
#else

      // Check if we shall do anything at all.

      if (abortNoImprovement)
      {
         if (_data.bestLowerBound >= _data.bestUpperBound)
            return false;
      }

      // Check for zero matrix.

      if (_data.maximumEntry() == 0.0)
      {
         _value = 0.0;
         return true;
      }

      // Detect symmetry (if requested, supported and not yet done).

      if (symmetry)
      {
         std::chrono::time_point<std::chrono::system_clock> start, end;
         start = std::chrono::system_clock::now();
         _data.orbits->initialize();
         end = std::chrono::system_clock::now();
         double orbitsTime = std::chrono::duration_cast<std::chrono::milliseconds> (end - start).count() / 1000.0;
         _data.orbitsTime += orbitsTime;
         subtractTime += orbitsTime;
      }

      if (_data.verbosity > 0)
         std::cerr << "Computing " << _name << " bound. " << std::flush;

      // Initialize rectangles: if availabe, we use maximal rectangles up to symmetry, except if we need a dual 
      // certificate for the MHSB.

      std::vector<std::shared_ptr<Submatrix>>* initialRectangles = nullptr;
#if defined(WITH_SYMMETRY_nauty)
      if (enumerateRectangles && symmetry && _data.orbits->hasSymmetry())
      {
         subtractTime += enumerateMaximalRectanglesSymmetry(_data);
         initialRectangles = &_data.maximalRectanglesSymmetry;
      }
      else if (enumerateRectangles)
#else
      if (enumerateRectangles)
#endif
      {
         subtractTime += enumerateMaximalRectangles(_data);
         initialRectangles = &_data.maximalRectangles;
      }
      else
      {
         initialRectangles = &_data.usefulRectangles;
      }

      // Compute abortion objective values.

      double abortIfAtLeast = std::numeric_limits<double>::infinity();
      double abortIfAtMost = 0.0;
      if (abortNoImprovement)
      {
         abortIfAtLeast = _data.bestUpperBound - 1.0 + 0.01; // If we reach it, rounding up matches the upper bound.
         abortIfAtMost = _data.bestLowerBound - 0.01; // If we get to this value or below, rounding up cannot improve.
      }

      // Create subproblem solvers.

      // maximal -> no lazy generation
      // nonnegative -> if enumeration is on then lazy generation should not succeed.

      bool lazyRectangles = true;
      bool allRectangles = false;
      if (enumerateRectangles && (_type == LPBound::NONNEGATIVE_HYPERPLANE_SEPARATION || 
         _type == LPBound::FRACTIONAL_RECTANGLE_COVERING || _type == LPBound::FRACTIONAL_REFINED_RECTANGLE_COVERING))
      {
#ifdef NDEBUG
         lazyRectangles = false;
#endif
         allRectangles = true;
      }
      bool foolingPairs = _type == LPBound::FRACTIONAL_REFINED_RECTANGLE_COVERING;

      std::vector<SubproblemSolver*> subproblemSolvers;
      if (lazyRectangles)
      {
         subproblemSolvers.push_back(new SubproblemSolverEnumerateSmall(_data, foolingPairs, false));
#ifdef WITH_IP_scip
         subproblemSolvers.push_back(new SubproblemSolverSCIP(_data, foolingPairs, false));
#endif
#ifdef WITH_IP_gurobi
         subproblemSolvers.push_back(new SubproblemSolverGurobi(_data, foolingPairs, false));
#endif
      }
#ifndef NDEBUG
      for (std::size_t i = 0; i < subproblemSolvers.size(); ++i)
      {
         subproblemSolvers[i] = new SubproblemSolverDebug(subproblemSolvers[i], !allRectangles);
      }
#endif

      // Initialize LP.

      std::vector<double> objective(_data.numNonzeros());
      if (_type == LPBound::FRACTIONAL_RECTANGLE_COVERING || _type == LPBound::FRACTIONAL_REFINED_RECTANGLE_COVERING)
      {
         for (std::size_t v = 0; v < _data.numNonzeros(); ++v)
            objective[v] = 1.0;
      }
      else
      {
         for (std::size_t v = 0; v < _data.numNonzeros(); ++v)
            objective[v] = _data.nonzeros[v].entry * 1.0 / _data.maximumEntry();
      }
      bool lowerBound = _type != LPBound::HYPERPLANE_SEPARATION;
      bool upperBound = true;
      LP lp(_data, objective, lowerBound, _type == LPBound::FRACTIONAL_REFINED_RECTANGLE_COVERING,
         subproblemSolvers);

      // Add the rectangles.

      for (auto rect : *initialRectangles)
         lp.addRectangle(rect);
      if (_data.verbosity > 1)
         std::cerr << "\n  Initialized LP with " << initialRectangles->size() << " rectangles." << std::endl;

      // Exploit symmetry by merging variables.

      if (symmetry && _data.orbits->hasSymmetry())
      {
         if (_data.verbosity > 1)
            std::cerr << "  Merging variables of each orbit." << std::endl;

         std::vector<LP::Constraint> mergedVariables;
         for (std::size_t i = 0; i < _data.numNonzeros(); ++i)
         {
            std::size_t repr = _data.orbits->getRepresentative(i);
            if (repr != i)
               mergedVariables.push_back(lp.mergeEntryVariables(i, repr));
         }
         if (_type == FRACTIONAL_REFINED_RECTANGLE_COVERING)
         {
            for (std::size_t p = 0; p < _data.numFoolingPairs(); ++p)
            {
               std::size_t repr = _data.orbits->getFoolingPairRepresentative(p);
               if (repr != p)
               {
                  mergedVariables.push_back(lp.mergeFoolingPairVariables(p, repr));
               }
            }
         }

         // Solve with merged variables.

         lp.run(abortIfAtLeast, abortIfAtMost);

         if (computeDualCertificate)
         {
            if (_data.verbosity > 1)
               std::cerr << "  Splitting merged variables." << std::endl;

            lp.splitVariables(mergedVariables);
         }
      }

      if (symmetry && _data.orbits->hasSymmetry() && enumerateRectangles && computeDualCertificate)
      {
         for (auto solver : subproblemSolvers)
         {
            SubproblemSolverDebug* debugSolver = dynamic_cast<SubproblemSolverDebug*>(solver);
            if (debugSolver != nullptr)
               debugSolver->allowSuccess = true;
         }
      }

      if (!symmetry || !_data.orbits->hasSymmetry() || computeDualCertificate)
      {
         lp.run(abortIfAtLeast, abortIfAtMost);

         // Extract dual certificate, i.e., weighted rectangles.

         std::vector<double> duals;
         lp.getBasicRectangles(_data.usefulRectangles, duals);

         _matrices.reserve(duals.size());
         for (std::size_t i = 0; i < _data.usefulRectangles.size(); ++i)
         {
            if (duals[i] == 0.0)
               continue;

            const Submatrix& rectangle = *_data.usefulRectangles[i];
            _matrices.push_back(std::make_shared<Rank1Matrix>());
            for (std::size_t r = 0; r < rectangle.rows.size(); ++r)
               _matrices.back()->rows[rectangle.rows[r]] = duals[i];
            for (std::size_t c = 0; c < rectangle.columns.size(); ++c)
               _matrices.back()->columns[rectangle.columns[c]] = duals[i];
         }
      }

      // Extract primal certificate, i.e., the weight matrix.

      std::vector<double> pairWeights;
      _value = lp.getSolution(_weights, pairWeights);
      for (std::size_t p = 0; p < _pairs.size(); ++p)
         _pairs[p].weight = pairWeights[p];

      // Free the subproblem solver.

      for (std::size_t i = 0; i < subproblemSolvers.size(); ++i)
         delete subproblemSolvers[i];

      if (_data.verbosity > 0)
         std::cerr << "done." << std::endl;

      _data.updateLowerBound(_value);
      return true;

#endif
   }

} /* namespace nonnegrank */
