#include "data.hpp"
#include "enumeration.hpp"

#include <iostream>
#include <sstream>
#include <scip/scip.h>
#include <scip/scipdefplugins.h>

namespace nonnegrank
{

   bool RectangleCoveringBound::doComputeEnumeration(bool abortNoImprovement, double& subtractTime)
   {
      // Check if we shall do anything at all.

      if (abortNoImprovement)
      {
         if (_data.bestLowerBound >= _data.bestUpperBound)
            return false;
      }

      // Check for zero matrix.

      if (_data.maximumEntry() == 0)
      {
         _value = 0.0;
         return true;
      }

      if (_data.verbosity > 0)
      {
         std::cerr << "Computing <" << (_refined ? "refined " : "")
            << "rectangle covering> bound using maximal rectangles. " << std::flush;
      }

      // Initialize rectangles.

      if (enumerateRectangles)
      {
         enumerateMaximalRectangles(_data);
      }
      else
      {
         throw std::runtime_error(_name +
            " bound via enumeration is not supported. Please configure with an IP solver and rectangle enumeration.");
      }

   	SCIP * scip;
   	SCIP_CALL_ABORT(SCIPcreate(& scip));
      SCIP_CALL_ABORT(SCIPcreateProbBasic(scip, _refined ? "direct model for refined rectangle covering" 
         : "direct model for rectangle covering"));
      SCIP_CALL_ABORT(SCIPsetIntParam(scip, "display/verblevel", _data.verbosity > 1 ? 4 : 0));
      SCIP_CALL_ABORT(SCIPincludeDefaultPlugins(scip));
      SCIP_CALL_ABORT(SCIPsetBoolParam(scip, "misc/catchctrlc", false));
      SCIP_CALL_ABORT(SCIPsetObjsense(scip, SCIP_OBJSENSE_MINIMIZE));

      // Construct a variable lambda_R for every maximal rectangle in the given matrix.

      std::vector<SCIP_VAR*> lambda;
      lambda.resize(_data.maximalRectangles.size());
      for (std::size_t r = 0; r != _data.maximalRectangles.size(); ++r)
      {
         std::stringstream name;
         name << "lambda#" << r;
         SCIP_CALL_ABORT(SCIPcreateVarBasic(scip, &lambda[r], name.str().c_str(), 0.0, _refined ? 2.0 : 1.0, 1.0, 
            _refined ? SCIP_VARTYPE_INTEGER : SCIP_VARTYPE_BINARY));
         SCIP_CALL_ABORT(SCIPaddVar(scip, lambda[r]));
      }

      // Create a constraint sum_{R: nz in R} lambda_R >= 1 for every nonzero entry nz of the matrix.
      for (std::size_t s = 0; s < _data.numNonzeros(); ++s)
      {
      	SCIP_CONS* constraint = nullptr;
         std::stringstream name;
         name << "nonzero#" << s;
         SCIP_CALL_ABORT(SCIPcreateConsBasicLinear(scip, &constraint, name.str().c_str(), 0, nullptr, nullptr, 1.0, 
            SCIPinfinity(scip)));

         for (std::size_t r = 0; r != _data.maximalRectangles.size(); ++r)
         {
            auto rectangle = _data.maximalRectangles[r];
            if (rectangle->nonzeros[s])
         	  SCIP_CALL_ABORT(SCIPaddCoefLinear(scip, constraint, lambda[r], 1.0));
         }
         SCIP_CALL_ABORT(SCIPaddCons(scip, constraint));
         SCIP_CALL_ABORT(SCIPreleaseCons(scip, &constraint));
      }

      if (_refined)
      {
         // Create a constraing sum_{R: s1 in R or s2 in R} lambda_R >= 2 for every fooling pair {s1,s2}.

         for (std::size_t i = 0; i < _data.numFoolingPairs(); ++i)
         {
            SCIP_CONS* constraint = NULL;
            std::stringstream name;
            name << "foolingPair#" << i;
            const FoolingPair& fp = _data.foolingPairs[i];
            SCIP_CALL_ABORT(SCIPcreateConsBasicLinear(scip, &constraint, name.str().c_str(), 0, nullptr, nullptr, 2.0, 
               SCIPinfinity(scip)));

            for (std::size_t r = 0; r != _data.maximalRectangles.size(); ++r)
            {
               auto rectangle = _data.maximalRectangles[r];
               if (rectangle->nonzeros[fp.nonzero1] || rectangle->nonzeros[fp.nonzero2])
                  SCIP_CALL_ABORT(SCIPaddCoefLinear(scip, constraint, lambda[r], 1.0));
            }
            SCIP_CALL_ABORT(SCIPaddCons(scip, constraint));
            SCIP_CALL_ABORT(SCIPreleaseCons(scip, &constraint));
         }
      }

      if (_data.verbosity > 1)
      {
         std::cerr << "\n  Initialized IP with " << SCIPgetNOrigVars(scip) << " variables and "
            << SCIPgetNOrigConss(scip) << " constraints." << std::endl;
      }

      SCIP_CALL_ABORT(SCIPsolve(scip));

      _value = SCIPgetPrimalbound(scip);

      if (SCIPgetStatus(scip) != SCIP_STATUS_OPTIMAL)
      {
         std::cerr << "WARNING: <" << (_refined ? "refined " : "") << "rectangle covering> bound computation via scip"
            << " terminated with nonoptimal status " << SCIPgetStatus(scip) << "." << std::endl;
      }

      // Extract certificate.

      SCIP_SOL* sol = SCIPgetBestSol(scip);
      _rectangles.clear();
      for (std::size_t r = 0; r < _data.maximalRectangles.size(); ++r)
      {
         double x = SCIPgetSolVal(scip, sol, lambda[r]);
         if (x > 0.5)
            _rectangles.push_back(_data.maximalRectangles[r]);
         if (x > 1.5)
            _rectangles.push_back(_data.maximalRectangles[r]);
      }

      // Cleanup

      for (std::size_t r = 0; r != _data.maximalRectangles.size(); ++r)
      {
         SCIP_CALL_ABORT(SCIPreleaseVar(scip, &lambda[r]));
      }

      SCIP_CALL_ABORT(SCIPfree(&scip));

      if (_data.verbosity > 0)
      {
         std::cerr << "done.\n" << std::flush;
      }
      return true;
   }

}