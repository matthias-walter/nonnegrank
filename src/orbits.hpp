#ifndef _NONNEGRANK_ORBITS_HPP
#define _NONNEGRANK_ORBITS_HPP

#include <cassert>

#include "data.hpp"

namespace nonnegrank
{
   class Orbits
   {
   public:
      Orbits(Data& data);

      ~Orbits();

      void initialize();

      inline std::size_t numOrbits() const
      {
         return _numOrbits;
      }

      bool isInitialized() const;

      bool hasSymmetry() const;

      inline std::size_t getRepresentative(std::size_t index) const
      {
         assert(index < _representatives.size());
         assert(_representatives[index] < _representatives.size());
         assert(_representatives[_representatives[index]] == _representatives[index]);
         return _representatives[index];
      }

      inline std::size_t getFoolingPairRepresentative(std::size_t index) const
      {
         assert(index < _foolingPairRepresentatives.size());
         assert(_foolingPairRepresentatives[index] < _foolingPairRepresentatives.size());
         assert(_foolingPairRepresentatives[_foolingPairRepresentatives[index]] == _foolingPairRepresentatives[index]);
         return _foolingPairRepresentatives[index];
      }

   private:
      std::size_t findFoolingPairRepresentative(std::size_t index);

#if defined(WITH_SYMMETRY)
      void compute(Data& data);
#endif

   private:
      Data& _data;
      std::size_t _numOrbits;
      std::vector<std::size_t> _representatives;
      std::vector<std::size_t> _foolingPairRepresentatives;
      double _time;
   };

} /* namespace nonnegrank */

#endif /* _NONNEGRANK_ORBITS_HPP */
