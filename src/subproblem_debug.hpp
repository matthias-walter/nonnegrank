#ifndef _NONNEGRANK_SUBPROBLEM_DEBUG_HPP
#define _NONNEGRANK_SUBPROBLEM_DEBUG_HPP

#include "subproblem.hpp"

namespace nonnegrank
{

   class SubproblemSolverDebug: public SubproblemSolver
   {
   public:
      SubproblemSolverDebug(SubproblemSolver* otherSolver, bool allowSuccess);

      virtual ~SubproblemSolverDebug();

       virtual void findPositive(double constant, const std::vector<double>& nonzeroWeights,
         const std::vector<double>& foolingPairWeights,
         const std::vector<std::shared_ptr<BranchingData>>& branchingWeights,
         std::vector<std::shared_ptr<Submatrix>>& rectangles, double& bestWeight, double& upperBound);

   public:
      bool allowSuccess;

   protected:
      SubproblemSolver* _otherSolver;

   };

}

#endif /* _NONNEGRANK_SUBPROBLEM_DEBUG_HPP */
