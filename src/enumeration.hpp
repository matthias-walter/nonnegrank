#ifndef _NONNEGRANK_ENUMERATION_HPP
#define _NONNEGRANK_ENUMERATION_HPP

#include "data.hpp"

namespace nonnegrank
{
   double enumerateMaximalRectangles(Data& data);

   double enumerateMaximalRectanglesSymmetry(Data& data);

} /* namespace nonnegrank */

#endif /* _NONNEGRANK_ENUMERATION_HPP */
