#include "data.hpp"

#include "orbits.hpp"
#include "enumeration.hpp"

#include <algorithm>
#include <limits>
#include <cassert>

#include <chrono>
#include <iostream>

namespace nonnegrank
{

   Data::Data(const std::vector<std::vector<double>>& matrix, int& verboseOutput)
      : verbosity(verboseOutput), orbits(nullptr)
   {
      numRows = matrix.size();
      if (numRows > 0)
         numColumns = matrix[0].size();
      else
         numColumns = 0;

      denseIndices.resize(numRows);
      _maximumEntry = 0.0;
      for (std::size_t r = 0; r < numRows; ++r)
      {
         if (matrix[r].size() != numColumns)
            throw std::runtime_error("Input matrix' rows have different lengths!");
         assert(matrix[r].size() == numColumns);
         denseIndices[r].resize(numColumns);
         for (std::size_t c = 0; c < numColumns; ++c)
         {
            double entry = matrix[r][c];
            if (entry > 0.0)
            {
               denseIndices[r][c] = nonzeros.size();
               Nonzero nz;
               nz.row = r;
               nz.column = c;
               nz.entry = entry;
               if (entry > _maximumEntry)
                  _maximumEntry = entry;
               nonzeros.push_back(nz);
            }
            else if (entry == 0.0)
               denseIndices[r][c] = std::numeric_limits<std::size_t>::max();
            else
               throw std::runtime_error("Input matrix must be nonnegative!");
         }
      }

      initialize();
   }

   Data::Data(const std::vector<std::size_t>& rows, const std::vector<std::size_t>& columns,
      const std::vector<double>& entries, int& verboseOutput)
      : verbosity(verboseOutput), orbits(nullptr)
   {
      if (rows.size() != columns.size())
         throw std::runtime_error("Row- and column- indices must have the same lengths!");
      if (rows.size() != entries.size())
         throw std::runtime_error("Row indices and entries must have the same lengths!");

      nonzeros.reserve(rows.size());
      numRows = 0;
      numColumns = 0;
      _maximumEntry = 0.0;

      for (std::size_t i = 0; i < rows.size(); ++i)
      {
         Nonzero nz;
         nz.row = rows[i];
         nz.column = columns[i];

         if (entries[i] > 0.0)
         {
            nz.entry = entries[i];
            if (nz.entry > _maximumEntry)
               _maximumEntry = nz.entry;
            nonzeros.push_back(nz);
            if (nz.row >= numRows)
               numRows = nz.row + 1;
            if (nz.column >= numColumns)
               numColumns = nz.column + 1;
         }
         else if (entries[i] < 0.0)
            throw std::runtime_error("Input matrix must be nonnegative!");
      }

      // Allocate dense matrix.

      denseIndices.resize(numRows);
      for (std::size_t r = 0; r < numRows; ++r)
      {
         denseIndices[r].resize(numColumns, std::numeric_limits<std::size_t>::max());
      }

      // Fill it.

      for (std::size_t i = 0; i < nonzeros.size(); ++i)
      {
         denseIndices[nonzeros[i].row][nonzeros[i].column] = i;
      }

      initialize();
   }

   Data::~Data()
   {
      if (orbits != nullptr)
         delete orbits;
   }

   void Data::setSubmatrixNonzeros(std::shared_ptr<Submatrix>& submatrix)
   {
      submatrix->nonzeros.clear();
      submatrix->nonzeros.resize(numNonzeros(), false);
      for (std::size_t r = 0; r < submatrix->rows.size(); ++r)
      {
         for (std::size_t c = 0; c < submatrix->columns.size(); ++c)
         {
            std::size_t v = denseIndices[submatrix->rows[r]][submatrix->columns[c]];
            if (v != std::numeric_limits<std::size_t>::max())
               submatrix->nonzeros[v] = true;
         }
      }
   }

   bool Data::isRectangle(std::shared_ptr<Submatrix>& submatrix)
   {
      for (auto row : submatrix->rows)
      {
         for (auto column : submatrix->columns)
         {
            if (!isNonzero(row, column))
               return false;
         }
      }
      return true;
   }
   
   struct NonzeroIndexCompare
   {
      const Data& data;

      NonzeroIndexCompare(const Data& d)
         : data(d)
      {

      }

      inline bool operator()(const std::size_t i, const std::size_t j) const
      {
         if (data.nonzeros[i].entry > data.nonzeros[j].entry)
            return false;
         if (data.nonzeros[i].entry < data.nonzeros[j].entry)
            return true;
         if (data.nonzeros[i].row > data.nonzeros[j].row)
            return false;
         if (data.nonzeros[i].row < data.nonzeros[j].row)
            return true;
         return data.nonzeros[i].column < data.nonzeros[j].column;
      }
   };

   void Data::initialize()
   {
      verbosity = 1;
      bestLowerBound = 0;
      bestUpperBound = std::min(numRows, numColumns);
      orbits = new Orbits(*this);
      enumerateTime = 0.0;
      enumerateSymmetryTime = 0.0;
      orbitsTime = 0.0;

      // Create array of sorted nonzeros.

      NonzeroIndexCompare nonzeroIndexCompare(*this);
      sortedNonzeros.resize(numNonzeros());
      for (std::size_t i = 0; i < numNonzeros(); ++i)
         sortedNonzeros[i] = i;
      std::sort(sortedNonzeros.begin(), sortedNonzeros.end(), nonzeroIndexCompare);

      // Compute all fooling pairs.

      for (std::size_t i = 0; i < numNonzeros(); ++i)
      {
         for (std::size_t j = i + 1; j < numNonzeros(); ++j)
         {
            FoolingPair fp;
            fp.nonzero1 = i;
            fp.nonzero2 = j;
            if (nonzeros[i].row == nonzeros[j].row || nonzeros[i].column == nonzeros[j].column)
               continue;
            std::size_t k = denseIndices[nonzeros[i].row][nonzeros[j].column];
            std::size_t l = denseIndices[nonzeros[j].row][nonzeros[i].column];
            if (k == std::numeric_limits<std::size_t>::max() || l == std::numeric_limits<std::size_t>::max())
               continue;
            double det = nonzeros[i].entry * nonzeros[j].entry - nonzeros[k].entry * nonzeros[l].entry;
            if (det > 1.0e-12) // TODO: User-configured epsilon
            {
               fp.weight = 0.0;
               foolingPairs.push_back(fp);
            }
         }
      }
   }

   bool Data::updateLowerBound(double lowerBound)
   {
      {
         int rounded = int(lowerBound - 1.0e-3) + 1;
         if (rounded > bestLowerBound)
         {
            bestLowerBound = rounded;
            return true;
         }
         return false;
      }
   }

} /* namespace nonnegrank */
