#ifndef _NONNEGRANK_GANTER_TOOLS_HPP
#define _NONNEGRANK_GANTER_TOOLS_HPP

#include <boost/dynamic_bitset.hpp>

#include "data.hpp"

namespace nonnegrank
{

   std::vector<std::size_t> get_ones(const boost::dynamic_bitset<> chi_V);

   void pushRectangle(Data& data, std::vector<std::shared_ptr<Submatrix>>& Subm_maxRs, boost::dynamic_bitset<>& chi_rows, boost::dynamic_bitset<>& chi_columns);

   boost::dynamic_bitset<> inc(boost::dynamic_bitset<> A,const std::size_t i);

   bool sqsubseteq(const boost::dynamic_bitset<>& A, boost::dynamic_bitset<> B);

   bool preccurly(boost::dynamic_bitset<> & A, boost::dynamic_bitset<> & B);

   void phi(boost::dynamic_bitset<> & A,boost::dynamic_bitset<> & B, boost::dynamic_bitset<> & phi_A,boost::dynamic_bitset<> & phi_B,std::vector<std::size_t> & phiRows,std::vector<std::size_t> & phiCols);

   void construct_matrices(Data & data,std::vector<boost::dynamic_bitset<>> & M, std::vector<boost::dynamic_bitset<>> & M_T);

   void closure(const std::vector<boost::dynamic_bitset<>> & M, const std::vector<boost::dynamic_bitset<>> & M_T, boost::dynamic_bitset<> & A, boost::dynamic_bitset<> & B);

} /* namespace nonnegrank */

#endif /* _NONNEGRANK_GANTER_TOOLS_HPP */
