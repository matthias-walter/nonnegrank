#ifndef _NONNEGRANK_GANTER_NEXTCL_SYM_NAUTY_ALL_HPP
#define _NONNEGRANK_GANTER_NEXTCL_SYM_NAUTY_ALL_HPP

#include <boost/dynamic_bitset.hpp>

#include "data.hpp"
#include "group.hpp"

namespace nonnegrank
{
	// With symmetries
	boost::dynamic_bitset<> flatten(boost::dynamic_bitset<> & A, boost::dynamic_bitset<> & B);

	void closure_sym_all(std::vector<std::shared_ptr<Permutation>>& automorphisms, boost::dynamic_bitset<> & A, boost::dynamic_bitset<> & B);

   void nextClosure_sym_all(std::vector<std::shared_ptr<Permutation>>& automorphisms,const std::vector<boost::dynamic_bitset<>> & M, const std::vector<boost::dynamic_bitset<>> & M_T, boost::dynamic_bitset<> & A, boost::dynamic_bitset<> & B);

	void constructRectanglesGanterSymmetryNauty(Data & data);

} /* namespace nonnegrank */

#endif /* _NONNEGRANK_GANTER_NEXTCL_SYM_NAUTY_ALL_HPP */
