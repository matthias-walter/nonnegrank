#ifndef _NONNEGRANK_HPP
#define _NONNEGRANK_HPP

#include <memory>
#include <vector>
#include <unordered_map>
#include <ostream>

namespace nonnegrank
{
   class Data;

   /**
    * \brief Indices of a submatrix of M
    *
    * Indices of a submatrix of M.
    */

   // TODO: Nonzero index could be a typedef.

   // TODO: Distinguish between internal and interface one.

   struct Submatrix
   {
      /// Array with row indices
      std::vector<std::size_t> rows;

      /// Array with column indices
      std::vector<std::size_t> columns;

      /// Array for each nonzero s of M indicating whether s belongs to the submatrix.
      std::vector<bool> nonzeros;
   };

   /**
    * \brief Output operator for a \ref Submatrix
    *
    * Output operator for a \ref Submatrix \p submatrix, writing to \p stream.
    *
    * \param stream Stream reference to be written to
    * \param submatrix Submatrix to be written
    * \returns reference to \p stream
    */

   std::ostream& operator<<(std::ostream& stream, const Submatrix& submatrix);

   // TODO: Replace by submatrix and multipliers.

   struct Rank1Matrix
   {
      std::unordered_map<std::size_t, double> rows;
      std::unordered_map<std::size_t, double> columns;
   };

   struct FoolingPair
   {
      std::size_t nonzero1;
      std::size_t nonzero2;
      double weight;
   };

   /**
    * \brief Abstract base class for a bound on the nonnegative rank of M.
    *
    * Abstract base class for a bound on the nonnegative rank of M. Defines the interface for checking whether it is
    * supported, computed, and for retrieving statistics on the computation.
    */

   class Bound
   {

   protected:
      /**
       * \brief Constructor
       *
       * Constructor to initialize the bound structure.
       *
       * \param data The reference to \ref Data
       * \param name Name of the bound without the word `bound', surrounded by < and >.
       * \param isLowerBound Whether it is a lower bound on the nonnegative rank of M.
       */

      Bound(Data& data, const std::string& name, bool isLowerBound);

   public:

      /**
       * \brief Destructor
       *
       * Destructor.
       */

      virtual ~Bound();

      /**
       * \brief Returns the name of the bound
       *
       * Returns the name of the bound
       */

      inline const std::string& name() const
      {
         return _name;
      }

      /**
       * \brief Returns \c true iff the bound is supported
       *
       * Returns \c true if and only if the bound is supported. Note that one can determine supported bounds at compile
       * time already. TODO: How?
       */

      inline bool isSupported() const
      {
         return _isSupported;
      }

      /**
       * \brief Returns \c true iff the bound was already computed
       *
       * Returns \c true if and only if the bound was already computed, in which case its value can be obtained via the
       * \ref value method.
       */

      virtual bool wasComputed() const;

      /**
       * \brief Returns the value of the bound
       *
       * Returns the value of the bound.
       */

      inline double value() const
      {
         return _value;
      }

      /**
       * \brief Returns the time for computing the bound
       *
       * Returns the time for computing the bound, excluding auxiliary algorithms such as rectangle enumeration or
       * symmetry detection.
       */

      inline double time() const
      {
         return _time;
      }

      /**
       * \brief Returns \c true iff the bound is a lower bound
       *
       * Returns \c true if and only if the bound is a lower bound on the nonnegative rank of M.
       */

      inline bool isLowerBound() const
      {
         return _isLowerBound;
      }

      /**
       * \brief Prints the certificate of the bound
       *
       * Prints the certificate of the bound to the given \p stream.
       */

      virtual void printCertificate(std::ostream& stream) = 0;

      /**
       * \brief Prints statistics on the computation
       *
       * Prints statistics on the computation of the bonud to the given \p stream.
       */

      virtual void printStatistics(std::ostream& stream);

      /**
       * \brief Computes the bound
       *
       * Computes the bound.
       *
       * \param abortNoImprovement if \c true, the computation may be aborted if it is observed that it cannot improve
       * the current best bound. For lower bounds, this means that it is equal to or less than the current best
       * lower bound. If the computation is aborted, \ref wasComputed() will return \c false.
       */

      bool compute(bool abortNoImprovement = false);

   protected:

      /**
       * \brief Internal method for the actual computation
       *
       * Internal method for the actual computation.
       *
       * \param abortNoImprovement if \c true, the computation may be aborted if it is observed that it cannot improve
       * the current best bound. For lower bounds, this means that it is equal to or less than the current best
       * lower bound. If the computation is aborted, \ref wasComputed() will return \c false.
       * \param subtractTime amount of time that was spent for auxiliary algorithms.
       */

      virtual bool doCompute(bool abortNoImprovement, double& subtractTime) = 0;

   protected:

      /// Reference to \ref Data
      Data& _data;

      /// Whether this bound is a lower bound on the nonnegative rank of M
      bool _isLowerBound;

      /// Name of the bound
      std::string _name;

      /// Whether it can be computed
      bool _isSupported;

      /// Whether it was already computed
      bool _wasComputed;

      /// Its value
      double _value;

      /// Time spent to compute it
      double _time;
   };

   /**
    * \brief Lower bound \<rank\>
    *
    * Lower bound \<rank\> for the rank of M.
    */

   class RankBound: public Bound
   {
   public:

      /**
       * \brief Constructor
       *
       * Constructor to initialize with \ref Data.
       */

      RankBound(Data& data);

      /**
       * \brief Destructor
       *
       * Destructor.
       */

      virtual ~RankBound();

      /**
       * \brief Prints the certificate of the bound
       *
       * Prints the certificate of the bound to the given \p stream.
       */

      virtual void printCertificate(std::ostream& stream);

      /**
       * \brief Returns a regular submatrix of M having the same rank
       *
       * Returns a shared pointer to a regular submatrix of M having the same rank.
       */

      inline std::shared_ptr<const Submatrix> getCertificateMatrix() const
      {
         return _basisMatrix;
      }

   protected:

      /**
       * \brief Internal method for the actual computation
       *
       * Internal method for the actual computation.
       *
       * \param abortNoImprovement if \c true, the computation may be aborted if it is observed that it cannot improve
       * the current best bound. For lower bounds, this means that it is equal to or less than the current best
       * lower bound. If the computation is aborted, \ref wasComputed() will return \c false.
       * \param subtractTime amount of time that was spent for auxiliary algorithms.
       */

      virtual bool doCompute(bool abortNoImprovement, double& subtractTime);

   private:

      /// Pointer to a regular submatrix of M having the same rank
      std::shared_ptr<const Submatrix> _basisMatrix;
   };

   /**
    * \brief Lower bound \<fooling set\>
    *
    * Lower bound \<fooling set\> for the fooling-set number of M.
    */

   class FoolingSetBound: public Bound
   {
   public:

      /**
       * \brief Constructor
       *
       * Constructor to initialize with \ref Data.
       */

      FoolingSetBound(Data& data);

      /**
       * \brief Destructor
       *
       * Destructor.
       */

      virtual ~FoolingSetBound();

      /**
       * \brief Prints the certificate of the bound
       *
       * Prints the certificate of the bound to the given \p stream.
       */

      virtual void printCertificate(std::ostream& stream);

      /**
       * \brief Returns a maximum-size fooling set of M.
       *
       * Returns a maximum-size fooling set of M by means of a shared pointer to a \ref Submatrix. The rows and columns
       * of the submatrix are ordered according to the fooling set.
       */

      inline std::shared_ptr<const Submatrix> getCertificateMatrix() const
      {
         return _foolingSet;
      }

   protected:

      /**
       * \brief Internal method for the actual computation
       *
       * Internal method for the actual computation.
       *
       * \param abortNoImprovement if \c true, the computation may be aborted if it is observed that it cannot improve
       * the current best bound. For lower bounds, this means that it is equal to or less than the current best
       * lower bound. If the computation is aborted, \ref wasComputed() will return \c false.
       * \param subtractTime amount of time that was spent for auxiliary algorithms.
       */

      virtual bool doCompute(bool abortNoImprovement, double& subtractTime);

   private:

      /// Pointer to the fooling set submatrix.
      std::shared_ptr<const Submatrix> _foolingSet;
   };

   /**
    * \brief Lower bounds that are computed via linear programming
    *
    * Lower bounds that are computed via linear programming. More precisely, the
    *   - \c hyperplane \c separation \c bound
    *   - \c nonnegative \c hyperplane \c separation \c bound
    *   - \c maximal \c hyperplane \c separation \c bound
    *   - \c fractional \c rectangle \c covering \c bound
    *   - \c fractional \c refined \c rectangle \c covering \c bound
    */

   class LPBound: public Bound
   {
   public:

      /**
       * \brief Type of bound
       *
       * Type of bound.
       */

      enum Type
      {
         /// \c hyperplane \c separation \c bound
         HYPERPLANE_SEPARATION,
         /// \c nonnegative \c hyperplane \c separation \c bound
         NONNEGATIVE_HYPERPLANE_SEPARATION,
         /// \c fractional \c rectangle \c covering \c bound
         FRACTIONAL_RECTANGLE_COVERING,
         /// \c fractional \c refined \c rectangle \c covering \c bound
         FRACTIONAL_REFINED_RECTANGLE_COVERING
      };

      /**
       * \brief Constructor
       *
       * Constructor to initialize with \ref Data for computing the given \ref Type.
       */

      LPBound(Data& data, Type type);

      /**
       * \brief Destructor
       *
       * Destructor.
       */

      virtual ~LPBound();

      /**
       * \brief Returns \c true iff the bound was already computed
       *
       * Returns \c true if and only if the bound was already computed, in which case its value can be obtained via the
       * \ref value method.
       */

      virtual bool wasComputed() const;

      /**
       * \brief Prints the certificate of the bound
       *
       * Prints the certificate of the bound to the given \p stream.
       */

      virtual void printCertificate(std::ostream& stream);

      /**
       * \brief Extracts a weight matrix as a certificate
       *
       * Extracts a weight matrix as a certificate. The matrix is stored in a dense form in \p weightMatrix, which is a
       * 2-dimensional array of the same size as M.
       */

      void getCertificateWeights(std::vector<std::vector<double>>& weightMatrix) const;

      /**
       * \brief Extracts a weight matrix as a certificate
       *
       * Extracts a weight matrix as a certificate. The matrix is stored in a in \p weightMatrix, which is a
2-dimensional
       * array of the same size as M.
       */

      void getCertificateWeights(std::vector<std::size_t>& rows, std::vector<std::size_t>& columns,
         std::vector<double>& weights) const;

      void getCertificateFoolingPairs(std::vector<FoolingPair>& pairs) const;

      inline void getDualCertificate(std::vector<std::shared_ptr<Rank1Matrix>>& matrices) const
      {
         matrices = _matrices;
      }

   protected:

      /**
       * \brief Internal method for the actual computation
       *
       * Internal method for the actual computation.
       *
       * \param abortNoImprovement if \c true, the computation may be aborted if it is observed that it cannot improve
       * the current best bound. For lower bounds, this means that it is equal to or less than the current best
       * lower bound. If the computation is aborted, \ref wasComputed() will return \c false.
       * \param subtractTime amount of time that was spent for auxiliary algorithms.
       */

      virtual bool doCompute(bool abortNoImprovement, double& subtractTime);

   public:
      bool enumerateRectangles;
      bool symmetry;
      bool computeDualCertificate;

   private:
      Type _type;
      std::vector<double> _weights;
      std::vector<FoolingPair> _pairs;
      std::vector<std::shared_ptr<Rank1Matrix>> _matrices;
   };

   class RectangleCoveringBound: public Bound
   {
   public:
      RectangleCoveringBound(Data& data, bool refined);

      virtual ~RectangleCoveringBound();

      /**
       * \brief Prints the certificate of the bound
       *
       * Prints the certificate of the bound to the given \p stream.
       */

      virtual void printCertificate(std::ostream& stream);

      inline void getDualCertificate(std::vector<std::shared_ptr<Submatrix>>& rectangles) const
      {
         rectangles = _rectangles;
      }

   public:
      bool enumerateRectangles;

   protected:

      /**
       * \brief Internal method for the actual computation
       *
       * Internal method for the actual computation.
       *
       * \param abortNoImprovement if \c true, the computation may be aborted if it is observed that it cannot improve
       * the current best bound. For lower bounds, this means that it is equal to or less than the current best
       * lower bound. If the computation is aborted, \ref wasComputed() will return \c false.
       * \param subtractTime amount of time that was spent for auxiliary algorithms.
       */

      virtual bool doCompute(bool abortNoImprovement, double& subtractTime);

      bool doComputeBranchAndPrice(bool abortNoImprovement, double& subtractTime);

      bool doComputeEnumeration(bool abortNoImprovement, double& subtractTime);

   private:
      bool _refined;
      std::vector<std::shared_ptr<Submatrix>> _rectangles;
   };

   class Solver
   {
   private:
      Data* _data;

   public:
      Solver(const std::vector<std::vector<double>>& matrix);

      Solver(const std::vector<std::size_t>& rows, const std::vector<std::size_t>& columns,
         const std::vector<double>& nonzeros);

      ~Solver();

      std::size_t numRows() const;

      std::size_t numColumns() const;

      std::size_t numNonzeros() const;

      std::size_t numFoolingPairs() const;

      void printAuxiliaryStatistics(std::ostream& stream);

      RankBound rankBound;

      FoolingSetBound foolingSetBound;

      LPBound hyperplaneSeparationBound;

      LPBound nonnegativeHyperplaneSeparationBound;

      LPBound fractionalRectangleCoveringBound;

      LPBound fractionalRefinedRectangleCoveringBound;

      RectangleCoveringBound rectangleCoveringBound;

      RectangleCoveringBound refinedRectangleCoveringBound;

      double getCurrentLowerBound() const;

      int computeBestLowerBound();

      int computeAllLowerBounds();

      int getCurrentUpperBound() const;

      int computeUpperBounds();

      int verbosity;
   };

} /* namespace nonnegrank */

#endif /* _NONNEGRANK_HPP */

