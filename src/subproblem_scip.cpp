#include "subproblem_scip.hpp"

#include <limits>
#include <iostream>
#include <iterator>
#include <iomanip>
#include <sstream>

#include <scip/scip.h>
#include <scip/scipdefplugins.h>

namespace nonnegrank
{
   SubproblemSolverSCIP::SubproblemSolverSCIP(Data& data, bool foolingPairs, bool branchings)
      : SubproblemSolver("scip", data, foolingPairs, branchings)
   {
      SCIP_CALL_ABORT(SCIPcreate(&_scip));
      SCIP_CALL_ABORT(SCIPcreateProbBasic(_scip, "subproblem"));
      SCIP_CALL_ABORT(SCIPincludeDefaultPlugins(_scip));
      SCIP_CALL_ABORT(SCIPsetIntParam(_scip, "display/verblevel", 0));
      SCIP_CALL_ABORT(SCIPsetBoolParam(_scip, "misc/catchctrlc", false));
      SCIP_CALL_ABORT(SCIPsetObjsense(_scip, SCIP_OBJSENSE_MAXIMIZE));

      SCIP_CALL_ABORT(SCIPsetBoolParam(_scip, "conflict/enable", false));
      SCIP_CALL_ABORT(SCIPsetIntParam(_scip, "branching/pscost/priority", 100000));
      SCIP_CALL_ABORT(SCIPsetIntParam(_scip, "branching/pscost/priority", 100000));
      SCIP_CALL_ABORT(SCIPsetIntParam(_scip, "separating/impliedbounds/freq", -1));
      SCIP_CALL_ABORT(SCIPsetIntParam(_scip, "separating/disjunctive/freq", -1));
      SCIP_CALL_ABORT(SCIPsetIntParam(_scip, "separating/gomory/freq", -1));
      SCIP_CALL_ABORT(SCIPsetIntParam(_scip, "separating/strongcg/freq", -1));
      SCIP_CALL_ABORT(SCIPsetIntParam(_scip, "separating/aggregation/freq", -1));
      SCIP_CALL_ABORT(SCIPsetIntParam(_scip, "separating/clique/freq", -1));
      SCIP_CALL_ABORT(SCIPsetIntParam(_scip, "separating/zerohalf/freq", -1));
      SCIP_CALL_ABORT(SCIPsetIntParam(_scip, "separating/mcf/freq", -1));
      SCIP_CALL_ABORT(SCIPsetIntParam(_scip, "constraints/cardinality/sepafreq", -1));
      SCIP_CALL_ABORT(SCIPsetIntParam(_scip, "constraints/SOS1/sepafreq", -1));
      SCIP_CALL_ABORT(SCIPsetIntParam(_scip, "constraints/SOS2/sepafreq", -1));
      SCIP_CALL_ABORT(SCIPsetIntParam(_scip, "constraints/varbound/sepafreq", -1));
      SCIP_CALL_ABORT(SCIPsetIntParam(_scip, "constraints/knapsack/sepafreq", -1));
      SCIP_CALL_ABORT(SCIPsetIntParam(_scip, "constraints/setppc/sepafreq", -1));
      SCIP_CALL_ABORT(SCIPsetIntParam(_scip, "constraints/linking/sepafreq", -1));
      SCIP_CALL_ABORT(SCIPsetIntParam(_scip, "constraints/or/sepafreq", -1));
      SCIP_CALL_ABORT(SCIPsetIntParam(_scip, "constraints/and/sepafreq", -1));
      SCIP_CALL_ABORT(SCIPsetIntParam(_scip, "constraints/xor/sepafreq", -1));
      SCIP_CALL_ABORT(SCIPsetIntParam(_scip, "constraints/linear/sepafreq", -1));
      SCIP_CALL_ABORT(SCIPsetIntParam(_scip, "constraints/orbisack/sepafreq", -1));
      SCIP_CALL_ABORT(SCIPsetIntParam(_scip, "constraints/orbitope/sepafreq", -1));
      SCIP_CALL_ABORT(SCIPsetIntParam(_scip, "constraints/symresack/sepafreq", -1));
      SCIP_CALL_ABORT(SCIPsetIntParam(_scip, "constraints/logicor/sepafreq", -1));
      SCIP_CALL_ABORT(SCIPsetIntParam(_scip, "constraints/cumulative/sepafreq", -1));
      SCIP_CALL_ABORT(SCIPsetIntParam(_scip, "constraints/nonlinear/sepafreq", -1));
      SCIP_CALL_ABORT(SCIPsetIntParam(_scip, "constraints/indicator/sepafreq", -1));

      // x_r = 1 iff row r belongs to rectangle.

      _rowVariables.resize(_data.numRows);
      for (std::size_t row = 0; row < _data.numRows; ++row)
      {
         std::stringstream ss;
         ss << "row#" << row;
         SCIP_CALL_ABORT(SCIPcreateVarBasic(_scip, &_rowVariables[row], ss.str().c_str(), 0.0, 1.0, 0.0,
            SCIP_VARTYPE_BINARY));
         SCIP_CALL_ABORT(SCIPaddVar(_scip, _rowVariables[row]));
      }

      // y_c = 1 iff column c belongs to rectangle.

      _columnVariables.resize(_data.numColumns);
      for (std::size_t column = 0; column < _data.numColumns; ++column)
      {
         std::stringstream ss;
         ss << "column#" << column;
         SCIP_CALL_ABORT(SCIPcreateVarBasic(_scip, &_columnVariables[column], ss.str().c_str(), 0.0,
            1.0, 0.0, SCIP_VARTYPE_BINARY));
         SCIP_CALL_ABORT(SCIPaddVar(_scip, _columnVariables[column]));
      }

      // z_i = 1 iff nonzero i belongs to rectangle.

      _nonzeroVariables.resize(_data.numNonzeros());
      for (std::size_t i = 0; i < _data.numNonzeros(); ++i)
      {
         std::stringstream ss;
         ss << "nonzero#" << i << "#row#" << _data.nonzeros[i].row << "#column#" <<  _data.nonzeros[i].column;
         SCIP_CALL_ABORT(SCIPcreateVarBasic(_scip, &_nonzeroVariables[i], ss.str().c_str(), 0.0, 1.0, 0.0,
            SCIP_VARTYPE_BINARY));
         SCIP_CALL_ABORT(SCIPaddVar(_scip, _nonzeroVariables[i]));
      }

      if (_foolingPairs)
      {
         // f_{i,j} = 1 iff i or j belongs to the rectangle.

         _foolingPairVariables.resize(_data.foolingPairs.size());
         for (std::size_t p = 0; p < _data.foolingPairs.size(); ++p)
         {
            std::stringstream ss;
            ss << "pair#" << p << "#" << _data.foolingPairs[p].nonzero1 << "#" << _data.foolingPairs[p].nonzero2;
            SCIP_CALL_ABORT(SCIPcreateVarBasic(_scip, &_foolingPairVariables[p], ss.str().c_str(), 0.0, 1.0, 0.0,
               SCIP_VARTYPE_BINARY));
            SCIP_CALL_ABORT(SCIPaddVar(_scip, _foolingPairVariables[p]));
         }
      }

      // x_r + y_c <= 1 for zero entry (r,c).

      for (std::size_t row = 0; row < _data.numRows; ++row)
      {
         for (std::size_t column = 0; column < _data.numColumns; ++column)
         {
            if (_data.denseIndices[row][column] < std::numeric_limits<std::size_t>::max())
               continue;

            SCIP_CONS* cons = NULL;
            std::stringstream ss;
            ss << "zero#" << row << "#" << column;
            SCIP_CALL_ABORT(SCIPcreateConsBasicLinear(_scip, &cons, ss.str().c_str(), 0, NULL, NULL, 0.0, 1.0));
            SCIP_CALL_ABORT(SCIPaddCoefLinear(_scip, cons, _rowVariables[row], 1.0));
            SCIP_CALL_ABORT(SCIPaddCoefLinear(_scip, cons, _columnVariables[column], 1.0));
            SCIP_CALL_ABORT(SCIPaddCons(_scip, cons));
            SCIP_CALL_ABORT(SCIPreleaseCons(_scip, &cons));
         }
      }

      // x_r - z_i >= 0 for i'th nonzero (r,c).

      for (std::size_t i = 0; i < _data.nonzeros.size(); ++i)
      {
         SCIP_CONS* cons = NULL;
         std::stringstream ss;
         ss << "nonzero#" << i << "#row#" << _data.nonzeros[i].row;
         SCIP_CALL_ABORT(SCIPcreateConsBasicLinear(_scip, &cons, ss.str().c_str(), 0, NULL, NULL, 0.0, 1.0));
         SCIP_CALL_ABORT(SCIPaddCoefLinear(_scip, cons, _rowVariables[_data.nonzeros[i].row], 1.0));
         SCIP_CALL_ABORT(SCIPaddCoefLinear(_scip, cons, _nonzeroVariables[i], -1.0));
         SCIP_CALL_ABORT(SCIPaddCons(_scip, cons));
         SCIP_CALL_ABORT(SCIPreleaseCons(_scip, &cons));
      }

      // y_c - z_i >= 0 for i'th nonzero (r,c).

      for (std::size_t i = 0; i < _data.nonzeros.size(); ++i)
      {
         SCIP_CONS* cons = NULL;
         std::stringstream ss;
         ss << "nonzero#" << i << "#column#" << _data.nonzeros[i].column;
         SCIP_CALL_ABORT(SCIPcreateConsBasicLinear(_scip, &cons, ss.str().c_str(), 0, NULL, NULL, 0.0, 1.0));
         SCIP_CALL_ABORT(SCIPaddCoefLinear(_scip, cons, _columnVariables[_data.nonzeros[i].column], 1.0));
         SCIP_CALL_ABORT(SCIPaddCoefLinear(_scip, cons, _nonzeroVariables[i], -1.0));
         SCIP_CALL_ABORT(SCIPaddCons(_scip, cons));
         SCIP_CALL_ABORT(SCIPreleaseCons(_scip, &cons));
      }

      // x_r + y_c - z_i <= 1 for i'th nonzero (row,column).

      for (std::size_t i = 0; i < _data.nonzeros.size(); ++i)
      {
         SCIP_CONS* cons = NULL;
         std::stringstream ss;
         ss << "nonzero#" << i << "#row#" << _data.nonzeros[i].row << "#column#" << _data.nonzeros[i].column;
         SCIP_CALL_ABORT(SCIPcreateConsBasicLinear(_scip, &cons, ss.str().c_str(), 0, NULL, NULL, 0.0, 1.0));
         SCIP_CALL_ABORT(SCIPaddCoefLinear(_scip, cons, _rowVariables[_data.nonzeros[i].row], 1.0));
         SCIP_CALL_ABORT(SCIPaddCoefLinear(_scip, cons, _columnVariables[_data.nonzeros[i].column], 1.0));
         SCIP_CALL_ABORT(SCIPaddCoefLinear(_scip, cons, _nonzeroVariables[i], -1.0));
         SCIP_CALL_ABORT(SCIPaddCons(_scip, cons));
         SCIP_CALL_ABORT(SCIPreleaseCons(_scip, &cons));
      }

      if (_foolingPairs)
      {
         // p_{i,j} - z_i - z_j <= 0 for nonzeros i and j forming a fooling pair.

         for (std::size_t p = 0; p < _data.foolingPairs.size(); ++p)
         {
            const FoolingPair& fp = _data.foolingPairs[p];
            assert(fp.nonzero1 < std::numeric_limits<std::size_t>::max());
            assert(fp.nonzero2 < std::numeric_limits<std::size_t>::max());

            SCIP_CONS* cons = NULL;
            std::stringstream ss;
            ss << "pair#" << p << "#" << _data.foolingPairs[p].nonzero1 << "#" << _data.foolingPairs[p].nonzero2;
            SCIP_CALL_ABORT(SCIPcreateConsBasicLinear(_scip, &cons, ss.str().c_str(), 0, NULL, NULL, -1.0, 0.0));
            SCIP_CALL_ABORT(SCIPaddCoefLinear(_scip, cons, _nonzeroVariables[fp.nonzero1], -1.0));
            SCIP_CALL_ABORT(SCIPaddCoefLinear(_scip, cons, _nonzeroVariables[fp.nonzero2], -1.0));
            SCIP_CALL_ABORT(SCIPaddCoefLinear(_scip, cons, _foolingPairVariables[p], 1.0));
            SCIP_CALL_ABORT(SCIPaddCons(_scip, cons));
            SCIP_CALL_ABORT(SCIPreleaseCons(_scip, &cons));
         }
      }
   }

   SubproblemSolverSCIP::~SubproblemSolverSCIP()
   {
      for (std::size_t p = 0; p < _foolingPairVariables.size(); ++p)
         SCIP_CALL_ABORT(SCIPreleaseVar(_scip, &_foolingPairVariables[p]));
      for (std::size_t row = 0; row < _data.numRows; ++row)
         SCIP_CALL_ABORT(SCIPreleaseVar(_scip, &_rowVariables[row]));
      for (std::size_t column = 0; column < _data.numColumns; ++column)
         SCIP_CALL_ABORT(SCIPreleaseVar(_scip, &_columnVariables[column]));
      for (std::size_t i = 0; i < _data.numNonzeros(); ++i)
         SCIP_CALL_ABORT(SCIPreleaseVar(_scip, &_nonzeroVariables[i]));
      SCIP_CALL_ABORT(SCIPfree(&_scip));
   }

   void SubproblemSolverSCIP::findPositive(double constant, const std::vector<double>& nonzeroWeights,
      const std::vector<double>& foolingPairWeights,
      const std::vector<std::shared_ptr<BranchingData>>& branchingWeights,
      std::vector<std::shared_ptr<Submatrix>>& rectangles, double& bestWeight, double& upperBound)
   {
      bestWeight = 0.0;

      // b_B = 1 iff nonzeros in set B are in rectangle.

      std::vector<SCIP_VAR*> branchingVariables;
      std::vector<SCIP_CONS*> branchingConstraints;
      branchingVariables.resize(branchingWeights.size());
      for (std::size_t b = 0; b < branchingWeights.size(); ++b)
      {
         const BranchingData& bd = *branchingWeights[b];
         std::stringstream ss;
         ss << "branch#" << b;
         for (std::size_t i = 0; i < bd.nonzeros.size(); ++i)
            ss << "#" <<  bd.nonzeros[i];
         SCIP_CALL_ABORT(SCIPcreateVarBasic(_scip, &branchingVariables[b], ss.str().c_str(), 0.0, 1.0,
            bd.weight, SCIP_VARTYPE_BINARY));
         SCIP_CALL_ABORT(SCIPaddVar(_scip, branchingVariables[b]));

         // b_B - z_b <= 0 for all b in B.

         for (std::size_t i = 0; i < bd.nonzeros.size(); ++i)
         {
            SCIP_CONS* cons = NULL;
            std::stringstream ss;
            ss << "branchub#" << b << "#" << bd.nonzeros[i];
            SCIP_CALL_ABORT(SCIPcreateConsBasicLinear(_scip, &cons, ss.str().c_str(), 0, nullptr, nullptr, -1.0, 1.0));
            SCIP_CALL_ABORT(SCIPaddCoefLinear(_scip, cons, branchingVariables[b], 1.0));
            SCIP_CALL_ABORT(SCIPaddCoefLinear(_scip, cons, _nonzeroVariables[bd.nonzeros[i]], -1.0));
            SCIP_CALL_ABORT(SCIPaddCons(_scip, cons));
            branchingConstraints.push_back(cons);
         }

         // z_i + z_j + ... + z_k - b_B <= |B|-1 for B = {i,j,...,k}
         {
            SCIP_CONS* cons = NULL;
            std::stringstream ss;
            ss << "branchlb#" << b;
            SCIP_CALL_ABORT(SCIPcreateConsBasicLinear(_scip, &cons, ss.str().c_str(), 0, nullptr, nullptr, 0.0,
               bd.nonzeros.size() - 1.0));
            for (std::size_t i = 0; i < bd.nonzeros.size(); ++i)
               SCIP_CALL_ABORT(SCIPaddCoefLinear(_scip, cons, _nonzeroVariables[bd.nonzeros[i]], 1.0));
            SCIP_CALL_ABORT(SCIPaddCoefLinear(_scip, cons, branchingVariables[b], -1.0));
            SCIP_CALL_ABORT(SCIPaddCons(_scip, cons));
            branchingConstraints.push_back(cons);
         }
      }

      for (std::size_t i = 0; i < nonzeroWeights.size(); ++i)
         SCIP_CALL_ABORT(SCIPchgVarObj(_scip, _nonzeroVariables[i], nonzeroWeights[i]));
      assert(foolingPairWeights.size() == _foolingPairVariables.size());
      for (std::size_t p = 0; p < foolingPairWeights.size(); ++p)
         SCIP_CALL_ABORT(SCIPchgVarObj(_scip, _foolingPairVariables[p], foolingPairWeights[p]));

      // We only care about solutions with positive weight.
      SCIP_CALL_ABORT(SCIPsetObjlimit(_scip, -constant));

//       std::cerr << "[" << nonzeroWeights.size() << " entries, " << foolingPairWeights.size() << " fooling pairs, "
//          << branchingWeights.size() << " branchings]" << std::flush;

      SCIP_CALL_ABORT( SCIPsolve(_scip) );

      // Upper bound

      double dualBound = SCIPgetDualbound(_scip);
      if (dualBound < SCIPinfinity(_scip))
         upperBound = dualBound + constant;

      // Solutions

      int numSols = SCIPgetNSols(_scip);
      SCIP_SOL** sols = SCIPgetSols(_scip);
      for (int sol = 0; sol < numSols; ++sol)
      {
         double objectiveValue = SCIPgetSolOrigObj(_scip, sols[sol]) + constant;
         if (objectiveValue <= _epsilon)
            continue;

         auto rectangle = std::make_shared<Submatrix>();
         for (std::size_t r = 0; r < _data.numRows; ++r)
         {
            if (SCIPgetSolVal(_scip, sols[sol], _rowVariables[r]) > 0.5)
               rectangle->rows.push_back(r);
         }
         for (std::size_t c = 0; c < _data.numColumns; ++c)
         {
            if (SCIPgetSolVal(_scip, sols[sol], _columnVariables[c]) > 0.5)
               rectangle->columns.push_back(c);
         }
         // TODO: Check if weights are correct in branch-and-price too. If yes, we could skip this and extract
         // everything from the solution.
         double weight = computeWeight(constant, nonzeroWeights, foolingPairWeights, branchingWeights, rectangle);
//          std::cerr << "SCIP solution with objective value " << objectiveValue << " yields weight " << weight <<
//             std::endl;
         bestWeight = std::max(bestWeight, weight);
         if (weight > _epsilon)
         {
            rectangles.push_back(rectangle);
         }
      }

//       std::cerr << "SCIP -> bestWeight = " << bestWeight << ", upperBound = " << upperBound << std::endl;

      SCIP_CALL_ABORT(SCIPfreeSolve(_scip, true));
      SCIP_CALL_ABORT(SCIPfreeTransform(_scip));

      for (SCIP_CONS* cons : branchingConstraints)
      {
//          std::cerr << "Deleting constraint " << SCIPconsGetName(cons) << std::endl;
         SCIP_CALL_ABORT(SCIPdelCons(_scip, cons));
         SCIP_CALL_ABORT(SCIPreleaseCons(_scip, &cons));
      }
      for (SCIP_VAR* var : branchingVariables)
      {
//          std::cerr << "Deleting variable " << SCIPvarGetName(var) << std::endl;
         SCIP_Bool success;
         SCIP_CALL_ABORT(SCIPdelVar(_scip, var, &success));
         assert(success);
         SCIP_CALL_ABORT(SCIPreleaseVar(_scip, &var));
      }
   }

} /* namespace nonnegrank */
