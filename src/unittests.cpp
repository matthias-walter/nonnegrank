#include <criterion/criterion.h>

#include "nonnegrank.hpp"

std::shared_ptr<nonnegrank::Solver> init(const std::string& matrixString)
{
   std::stringstream ss(matrixString);
   std::size_t numRows, numColumns;
   ss >> numRows >> numColumns;
   std::vector<std::vector<double>> matrix;
   matrix.resize(numRows);
   for (std::size_t r = 0; r < numRows; ++r)
   {
      matrix[r].resize(numColumns);
      for (std::size_t c = 0; c < numColumns; ++c)
         ss >> matrix[r][c];
   }
   auto solver = std::make_shared<nonnegrank::Solver>(matrix);
   solver->verbosity = 0;
   return solver;
}

void check(nonnegrank::Bound& bound, double expectedValue, const std::string& matrixName)
{
   bound.compute();
   double value = bound.value();
   std::stringstream ss;
   ss << "Bound " << bound.name() << " for " << matrixName << " is " << value << ", but " << expectedValue << " was expected.";
   cr_assert((value > expectedValue - 1.0e-4) && (value < expectedValue + 1.0e-4), "%s", ss.str().c_str());
}

#if defined(WITH_RANK)

Test(Rank, Identity)
{
   auto solver = init("2 2 \
1 0 \
0 1 \
      ");
   check(solver->rankBound, 2.0, "2x2 identity");
}
   
Test(Rank, Sum)
{
   auto solver = init("3 3 \
1 2 3 \
4 5 6 \
5 7 9 \
      ");
   check(solver->rankBound, 2.0, "3x3 sum matrix");
}

Test(Rank, Cube)
{
   auto solver = init("8 16 \
0 0 0 0  0 0 0 0   1 1 1 1  1 1 1 1 \
0 0 0 0  1 1 1 1   0 0 0 0  1 1 1 1 \
0 0 1 1  0 0 1 1   0 0 1 1  0 0 1 1 \
0 1 0 1  0 1 0 1   0 1 0 1  0 1 0 1 \
1 1 1 1  1 1 1 1   0 0 0 0  0 0 0 0 \
1 1 1 1  0 0 0 0   1 1 1 1  0 0 0 0 \
1 1 0 0  1 1 0 0   1 1 0 0  1 1 0 0 \
1 0 1 0  1 0 1 0   1 0 1 0  1 0 1 0 \
      ");
   check(solver->rankBound, 5.0, "d-dimensional cube");
}

#endif

#if defined(WITH_FOOLINGSET)

Test(FoolingSet, Cube)
{
   auto solver = init("8 16 \
0 0 0 0  0 0 0 0   1 1 1 1  1 1 1 1 \
0 0 0 0  1 1 1 1   0 0 0 0  1 1 1 1 \
0 0 1 1  0 0 1 1   0 0 1 1  0 0 1 1 \
0 1 0 1  0 1 0 1   0 1 0 1  0 1 0 1 \
1 1 1 1  1 1 1 1   0 0 0 0  0 0 0 0 \
1 1 1 1  0 0 0 0   1 1 1 1  0 0 0 0 \
1 1 0 0  1 1 0 0   1 1 0 0  1 1 0 0 \
1 0 1 0  1 0 1 0   1 0 1 0  1 0 1 0 \
      ");
   check(solver->foolingSetBound, 8.0, "d-dimensional cube");
}

#endif


#if defined(WITH_LP) && defined(WITH_ENUMERATION)

Test(MaximalHyperplaneSeparation, A)
{
   auto solver = init("10 8 \
0 0 2 2 0 2 1 1 \
2 2 0 0 2 0 1 1 \
0 2 0 2 1 1 0 2 \
2 0 2 0 1 1 2 0 \
0 2 2 4 0 2 0 2 \
2 0 4 2 0 2 2 0 \
2 4 0 2 2 0 0 2 \
4 2 2 0 2 0 2 0 \
0 0 0 0 1 1 1 1 \
1 1 1 1 0 0 0 0 \
      ");
   solver->maximalHyperplaneSeparationBound.symmetry = false;
   solver->maximalHyperplaneSeparationBound.enumerateRectangles = true;
   check(solver->maximalHyperplaneSeparationBound, 2.75, "A");
}

#endif

#if defined(WITH_LP)

Test(MaximalHyperplaneSeparation, A_NO_ENUMERATE)
{
   auto solver = init("10 8 \
0 0 2 2 0 2 1 1 \
2 2 0 0 2 0 1 1 \
0 2 0 2 1 1 0 2 \
2 0 2 0 1 1 2 0 \
0 2 2 4 0 2 0 2 \
2 0 4 2 0 2 2 0 \
2 4 0 2 2 0 0 2 \
4 2 2 0 2 0 2 0 \
0 0 0 0 1 1 1 1 \
1 1 1 1 0 0 0 0 \
      ");
   solver->maximalHyperplaneSeparationBound.symmetry = false;
   solver->maximalHyperplaneSeparationBound.enumerateRectangles = false;
   bool caught = false;
   try
   {
      solver->maximalHyperplaneSeparationBound.compute();
   }
   catch(std::runtime_error& e)
   {
      caught = true;
   }
   cr_assert(caught, "Bound <maximal hyperplane separation> was computed without enumeration. This should not be possible!");
}

#endif

#if defined(WITH_LP) && defined(WITH_ENUMERATION) && defined(WITH_SYMMETRY)

Test(MaximalHyperplaneSeparation, A_Symmetry)
{
   auto solver = init("10 8 \
0 0 2 2 0 2 1 1 \
2 2 0 0 2 0 1 1 \
0 2 0 2 1 1 0 2 \
2 0 2 0 1 1 2 0 \
0 2 2 4 0 2 0 2 \
2 0 4 2 0 2 2 0 \
2 4 0 2 2 0 0 2 \
4 2 2 0 2 0 2 0 \
0 0 0 0 1 1 1 1 \
1 1 1 1 0 0 0 0 \
      ");
   solver->maximalHyperplaneSeparationBound.symmetry = true;
   solver->maximalHyperplaneSeparationBound.enumerateRectangles = true;
   check(solver->maximalHyperplaneSeparationBound, 2.75, "A");
}

#endif
