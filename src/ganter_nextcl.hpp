#ifndef _NONNEGRANK_GANTER_NEXTCL_HPP
#define _NONNEGRANK_GANTER_NEXTCL_HPP

#include <boost/dynamic_bitset.hpp>

#include "data.hpp"

namespace nonnegrank
{
	// Without symmetries
	void nextClosure(const std::vector<boost::dynamic_bitset<>> & M, const std::vector<boost::dynamic_bitset<>> & M_T, boost::dynamic_bitset<> & A, boost::dynamic_bitset<> & B);
	
	void constructMaximalRectanglesGanter(Data & data);

} /* namespace nonnegrank */

#endif /* _NONNEGRANK_GANTER_NEXTCL_HPP */