#include <boost/dynamic_bitset.hpp>

#include "data.hpp"
#include "ganter_common.hpp"
#include "ganter_nextcl.hpp"

#include <iostream>

namespace nonnegrank
{
   // Without symmetries
   void nextClosure(const std::vector<boost::dynamic_bitset<>> & M, const std::vector<boost::dynamic_bitset<>> & M_T, boost::dynamic_bitset<> & A, boost::dynamic_bitset<> & B)
   {
      boost::dynamic_bitset<> CI(A.size());
      boost::dynamic_bitset<> I(A.size());
      boost::dynamic_bitset<> notA(~A);

      auto i = notA.find_first();
      do {
         I = inc(A,i);
         i = notA.find_next(i);
         CI = I;
         closure(M,M_T,CI,B);
      } while (!sqsubseteq(I,CI));
      A = CI;
   }

   void constructMaximalRectanglesGanter(Data & data)
   {
      std::vector<boost::dynamic_bitset<>> M(data.numRows, boost::dynamic_bitset<>(data.numColumns));
      std::vector<boost::dynamic_bitset<>> M_T(data.numColumns, boost::dynamic_bitset<>(data.numRows));

      construct_matrices(data,M,M_T);

      boost::dynamic_bitset<> A(data.numRows);
      boost::dynamic_bitset<> B(data.numColumns);

      while(!A.all()) {
         nextClosure(M,M_T,A,B);
         if (B.any())
         {
            pushRectangle(data, data.maximalRectangles,A,B);
            data.setSubmatrixNonzeros(data.maximalRectangles.back());
            // std::cerr << "    " << A << std::endl;
            // std::cerr << "    " << B << std::endl << std::endl;
         }
      }
   }
}