#include "orbits.hpp"

#include <vector>

#include "data.hpp"
#include "nauty_common.hpp"

namespace nonnegrank
{

   void Orbits::compute(Data & data)
   {
      const std::size_t n = data.numRows + data.numColumns + data.numNonzeros();
      const std::size_t m = SETWORDSNEEDED(n);

      // Initializations for Nauty
      DYNALLSTAT(graph, g, g_sz);
      DYNALLSTAT(int, lab, lab_sz);
      DYNALLSTAT(int, ptn, ptn_sz);
      DYNALLSTAT(int, orbits, orbits_sz);
      static DEFAULTOPTIONS_GRAPH(options);
      statsblk stats;

      // Select option for custom partition.
      options.defaultptn = false;

      nauty_check(WORDSIZE, m, n, NAUTYVERSIONID);

      // Allocate memory for the graph.
      DYNALLOC1(int, lab, lab_sz, n, const_cast<char*>("malloc"));
      DYNALLOC1(int, ptn, ptn_sz, n, const_cast<char*>("malloc"));
      DYNALLOC1(int, orbits, orbits_sz, n, const_cast<char*>("malloc"));
      DYNALLOC2(graph, g, g_sz, n, m, const_cast<char*>("malloc"));

      // Empty the graph.

      EMPTYGRAPH(g, m, n);

      getMidNodeGraph(g, data, m, lab, ptn);

      // Determine the automorphism group, in particular the orbits, of the graph with nauty.

      densenauty(g, lab, ptn, orbits, &options, &stats, m, n, nullptr);

      const std::size_t firstNonzero = data.numRows + data.numColumns;
      for (std::size_t i = 0; i < data.numNonzeros(); ++i)
         _representatives[data.sortedNonzeros[i]] = data.sortedNonzeros[orbits[firstNonzero + i] - firstNonzero];

      // Clean up.

      DYNFREE(lab, lab_sz);
      DYNFREE(ptn, ptn_sz);
      DYNFREE(orbits, orbits_sz);
      DYNFREE(g, g_sz);
  }

}
