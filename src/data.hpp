#ifndef _NONNEGRANK_DATA_HPP
#define _NONNEGRANK_DATA_HPP

#include <limits>

#include "nonnegrank.hpp"
#include "orbits.hpp"

namespace nonnegrank
{
   struct Nonzero
   {
      std::size_t row;
      std::size_t column;
      double entry;
   };

   class Orbits;

   class Data
   {
   public:
      Data(const std::vector<std::vector<double>>& matrix, int& verbose);

      Data(const std::vector<std::size_t>& rows, const std::vector<std::size_t>& columns,
         const std::vector<double>& entries, int& verbose);

      ~Data();

      void setSubmatrixNonzeros(std::shared_ptr<Submatrix>& submatrix);

      bool isRectangle(std::shared_ptr<Submatrix>& submatrix);

      inline std::size_t numNonzeros() const
      {
         return nonzeros.size();
      }

      inline std::size_t numFoolingPairs() const
      {
         return foolingPairs.size();
      }

      bool updateLowerBound(double lowerBound);

      inline double maximumEntry() const
      {
         return _maximumEntry;
      }

      inline bool isNonzero(std::size_t row, std::size_t column) const
      {
         return denseIndices[row][column] < std::numeric_limits<std::size_t>::max();
      }

   private:
      void initialize();

   public:
      int& verbosity;

      int bestLowerBound;
      int bestUpperBound;

      std::size_t numRows;
      std::size_t numColumns;
      std::vector<Nonzero> nonzeros;
      std::vector<std::vector<std::size_t>> denseIndices;
      std::vector<FoolingPair> foolingPairs;
      std::vector<std::size_t> sortedNonzeros;

      Orbits* orbits;

      std::vector<std::shared_ptr<Submatrix>> smallRectangles;
      std::vector<std::shared_ptr<Submatrix>> maximalRectangles;
      std::vector<std::shared_ptr<Submatrix>> maximalRectanglesSymmetry;
      std::vector<std::shared_ptr<Submatrix>> usefulRectangles;

      double enumerateTime;
      double enumerateSymmetryTime;
      double orbitsTime;
   private:
      double _maximumEntry;
   };

} /* namespace nonnegrank */

#endif /* _NONNEGRANK_DATA_HPP */
