#include <boost/dynamic_bitset.hpp>

#include "data.hpp"
#include "group.hpp"
#include "ganter_common.hpp"

// #include <iostream>

namespace nonnegrank
{
   std::vector<std::size_t> get_ones(const boost::dynamic_bitset<> chi_V)
   {
      std::vector<std::size_t> V;
      for (auto pos=chi_V.find_first(); pos != boost::dynamic_bitset<>::npos; pos = chi_V.find_next(pos)) V.push_back(pos);
      return V;
   }

   void pushRectangle(Data& data, std::vector<std::shared_ptr<Submatrix>>& Subm_maxRs, boost::dynamic_bitset<>& chi_rows, boost::dynamic_bitset<>& chi_columns)
   {
      auto rectangle = std::make_shared<Submatrix>();
      rectangle->rows = get_ones(chi_rows);
      rectangle->columns = get_ones(chi_columns);
      data.setSubmatrixNonzeros(rectangle);
      Subm_maxRs.push_back(rectangle);
   }

   boost::dynamic_bitset<> inc(boost::dynamic_bitset<> A,const std::size_t i)
   {
      assert (i < A.size());

      for (auto pos=A.find_first(); pos != boost::dynamic_bitset<>::npos && pos < i; pos=A.find_next(pos)) A.flip(pos);
      A.set(i);

      return A;
   }

   bool sqsubseteq(const boost::dynamic_bitset<>& A, boost::dynamic_bitset<> B)
   {
      assert(A.size() == B.size());
      // Check if A is a subset of B
      if (A.is_subset_of(B))
      {
         auto min_A(A.find_first());
         // find first bit set to 1 in B
         // Skip all bits that are set to 0 in A and to 1 in B, and delete them from B
         // until a bit is found that is 1 in A and 1 in B. At that point, compare A to B
         for (auto pos = B.find_first(); pos != boost::dynamic_bitset<>::npos && pos < min_A; pos = B.find_next(pos))
            B.flip(pos);
         return (A == B);
      }
      else return false;
   }

   // Check if A \preccurly B
   bool preccurly(boost::dynamic_bitset<> & A, boost::dynamic_bitset<> & B)
   {
      assert(A.size() == B.size());

      if (A == B) return false; // in case A and B are equal

      std::size_t pos = A.size() - 1;
      while (true)
      {
         auto B_pos(B.test(pos));
         if (A.test(pos) != B_pos) return B_pos;
         --pos;
      }
   }

   void phi(boost::dynamic_bitset<> & A,boost::dynamic_bitset<> & B, boost::dynamic_bitset<> & phi_A,boost::dynamic_bitset<> & phi_B,std::vector<std::size_t> & phiRows,std::vector<std::size_t> & phiCols)
   {
      auto numRows(A.size());

      for (auto pos=A.find_first(); pos != boost::dynamic_bitset<>::npos; pos=A.find_next(pos))
         phi_A.flip(phiRows[pos]);

      for (auto pos=B.find_first(); pos != boost::dynamic_bitset<>::npos; pos=B.find_next(pos))
         phi_B.flip(phiCols[pos]); // fix
   }

   void construct_matrices(Data & data,std::vector<boost::dynamic_bitset<>> & M, std::vector<boost::dynamic_bitset<>> & M_T)
   {
      for (auto it=data.nonzeros.begin(); it != data.nonzeros.end();++it)
      {
         Nonzero coordinates(*it);
         M[coordinates.row].flip(coordinates.column);
         M_T[coordinates.column].flip(coordinates.row);
      }
   }

   // Compute the discrete convex hull of a point set A
   void closure(const std::vector<boost::dynamic_bitset<>> & M, const std::vector<boost::dynamic_bitset<>> & M_T, boost::dynamic_bitset<> & A, boost::dynamic_bitset<> & B)
   {
      B.set();
      // Intersect all sets of rows indexed by A
      for (auto pos = A.find_first(); pos != boost::dynamic_bitset<>::npos; pos = A.find_next(pos)) B &= M[pos];

      A.set();
      // Intersect all sets of columns indexed by B
      for (auto pos = B.find_first(); pos != boost::dynamic_bitset<>::npos; pos = B.find_next(pos)) A &= M_T[pos];
      // return A;
   }

}
