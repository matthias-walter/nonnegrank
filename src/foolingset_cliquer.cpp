#include "data.hpp"

extern "C"
{
#include "cliquer_interface.h"
}

#include <iostream>
#include <limits>
#include <cassert>

namespace nonnegrank
{

   bool FoolingSetBound::doCompute(bool abortNoImprovement, double& subtractTime)
   {
      if (abortNoImprovement)
      {
         if (_data.bestLowerBound >= _data.bestUpperBound)
            return false;
      }

      if (_data.verbosity > 0)
         std::cerr << "Computing <fooling set> bound." << std::flush;

      if (_data.numNonzeros() == 0)
      {
         if (_data.verbosity > 0)
            std::cerr << " done." << std::endl;
         return true;
      }

      // Create the cliquer graph.

      std::vector<int> edgeNodes;
      std::vector<int> cliqueNodes(_data.nonzeros.size(), 0);

      for (std::size_t i = 0; i < _data.nonzeros.size(); ++i)
      {
         std::size_t row1 = _data.nonzeros[i].row;
         std::size_t column1 = _data.nonzeros[i].column;
         for (std::size_t j = i + 1; j < _data.nonzeros.size(); ++j)
         {
            std::size_t row2 = _data.nonzeros[j].row;
            std::size_t column2 = _data.nonzeros[j].column;
            assert(_data.denseIndices[row1][column1] != std::numeric_limits<std::size_t>::max());
            assert(_data.denseIndices[row2][column2] != std::numeric_limits<std::size_t>::max());
            if (_data.denseIndices[row1][column2] == std::numeric_limits<std::size_t>::max()
               || _data.denseIndices[row2][column1] == std::numeric_limits<std::size_t>::max())
            {
               edgeNodes.push_back(i);
               edgeNodes.push_back(j);
            }
         }
      }
      if (_data.verbosity > 1)
      {
         std::cerr << "\n  Initialized Cliquer graph with " << _data.nonzeros.size() << " nodes and " 
            << (edgeNodes.size() / 2) << " edges." << std::endl;
      }

      int size = computeMaximumClique(_data.nonzeros.size(), edgeNodes.size() / 2, &edgeNodes[0], &cliqueNodes[0]);
      cliqueNodes.resize(size);

      auto foolingSet = std::make_shared<Submatrix>();
      foolingSet->rows.resize(size);
      foolingSet->columns.resize(size);
      for (int i = 0; i < size; ++i)
      {
         foolingSet->rows[i] = _data.nonzeros[cliqueNodes[i]].row;
         foolingSet->columns[i] = _data.nonzeros[cliqueNodes[i]].column;
      }
      _value = foolingSet->rows.size();
      _data.setSubmatrixNonzeros(foolingSet);
      if (_data.verbosity > 0) 
         std::cerr << " done." << std::endl;

      _foolingSet = foolingSet;
      _data.updateLowerBound(_value);
      return true;
   }

} /* namespace nonnegrank */
