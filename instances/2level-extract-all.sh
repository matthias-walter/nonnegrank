#!/usr/bin/env bash

for DIM in `seq 0 6`; do
   echo "Extracting 2-level polytopes of dimension $DIM."
   bash ./2level-extract-dimension.sh $DIM;
done
