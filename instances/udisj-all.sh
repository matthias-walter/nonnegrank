#!/bin/bash

mkdir -p udisj

for n in `seq 2 6`; do
   python udisj-gen.py $n > udisj/udisj-$n.mat
done
