#!/usr/bin/env python

import sys
import re

# Arg 1: comma-separated list of algorithms
# Args 2,3,...: list of instances logs

configs = sys.argv[1].split(',')
instances = sys.argv[2:]

times = {}
reTotalTime = re.compile('.*: [0-9.e]* \\(sym: [0-9.einf]*s, enum: [0-9.einf]*, bound: ([0-9.einf]*)\\)')
for instance in instances:
   times[(instance, 'VBS')] = float('inf')
   for line in open(instance, 'r').read().split('\n'):
      line = line.strip()
      for config in configs:
         if line.startswith(config + ':'):
            result = reTotalTime.match(line)
            assert result is not None
            t = float(result.group(1))
            times[(instance, config)] = t
            if t < times[(instance, 'VBS')]:
               times[(instance, 'VBS')] = t

sys.stderr.write('Evaluating configurations\n')
for config in configs:
   sys.stderr.write('  {}\n'.format(config))

sys.stdout.write('''\\documentclass[a4paper]{article}

\\usepackage[utf8]{inputenc}
\\usepackage[left=10mm,right=10mm,top=10mm,bottom=20mm]{geometry}
\\usepackage{pgfplots}
\\usepackage{amsmath}

\\begin{document}

\\pgfplotscreateplotcyclelist{perfplotslinear}{%
  {black,mark=none},%
  {red,mark=none},%
  {blue,mark=none},%
  {green!80!black,mark=none},%
  {orange,mark=none},%
  {purple,mark=none},%
  {cyan,mark=none},%
  {green!40!black,mark=none},%
}

\\pgfplotsset{every axis plot/.append style={%
  const plot,thick,
}}

\\begin{center}''')
sys.stdout.write('\n  \\textbf{%s}' % (sys.argv[1]))
sys.stdout.write('''\n
  \\begin{tikzpicture}
    \\begin{semilogxaxis}[
      xmode=log,
      enlargelimits=0.05,
      width=180mm,
      height=140mm,
      xmin=1,
      xmax=8,
      ymin=0,
      ymax=1,
      xtick={1,2,4,8},
      xticklabels={1,2,4,8},
      legend columns=1,
      legend cell align=left,
      legend pos=south east,
      cycle list name=perfplotslinear,
      ]\n''')
i = 0
for config in configs:
   # Compute ratios
   ratios = []
   for instance in instances:
      if times[(instance, 'VBS')] == float('inf'):
         ratios.append(float('inf'))
      else:
         ratios.append( (times[(instance, config)] + 0.1) / (times[(instance, 'VBS')] + 0.1) )

   # Compute dict with ratios as keys and occurences as values
   sortedRatios = {}
   for ratio in ratios:
      sortedRatios[ratio] = sortedRatios.get(ratio, 0.0) + 1

   # Turn it into a sorted list and scale down to have sum = 1.0
   scaledRatios = [ (key, value * 1.0 / len(ratios)) for key,value in sortedRatios.items() ]
   scaledRatios.sort()

   aggregatedRatios = [0.0] * (len(scaledRatios) + 1)
   aggregatedRatios[0] = (0.99999, 0.0)
   aggregatedRatios[1] = scaledRatios[0]
   for i in xrange(1, len(scaledRatios)):
      aggregatedRatios[i + 1] = (scaledRatios[i][0], scaledRatios[i][1] + aggregatedRatios[i][1])
   aggregatedRatios.append((100, aggregatedRatios[-1][1]))

   coords = []
   for result in aggregatedRatios:
      coords.append( '(%f,%f)' % (result[0], result[1]))
   sys.stdout.write('          \\addplot coordinates { %s };\n' % (' '.join(coords)))
   i += 1
sys.stdout.write('      \\legend{%s}\n' % (','.join( configs )))

sys.stdout.write('''    \\end{semilogxaxis}\n  \\end{tikzpicture}\n
\\end{center}

\\end{document}\n''')

