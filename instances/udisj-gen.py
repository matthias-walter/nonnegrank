import sys

n = int(sys.argv[1])

sys.stdout.write('%d %d\n\n' % (2**n, 2**n))

for r in range(2**n):
   for c in range(2**n):
      count = 0
      for bit in range(n):
         if (r / 2**bit) % 2 == 1 and (c / 2**bit) % 2 == 1:
            count += 1
      if c > 0:
         sys.stdout.write(' ')
      sys.stdout.write('%d' % ((count-1)*(count-1)))
   sys.stdout.write('\n')
