#!/usr/bin/env bash

DATABASE=../../experiments/

mkdir -p 2level/$1/

let "i = 0"
zcat $DATABASE/$1.gz | while read line; do
	echo "$(cut -d' ' -f3 <<< "${line}")" "$(cut -d' ' -f4 <<< "${line}")$(cut -d' ' -f5 <<< "${line}" | sed -e 's/0/ 0/g' -e 's/1/ 1/g')" > 2level/$1/$i.mat
	let "i++"
done
