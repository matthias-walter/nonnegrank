#!/bin/bash

mkdir perfprofile-output

python perfprofile-gen.py hsbXEG,hsbNEG,hsbXLG,hsbNLG,hsbXES,hsbNES,hsbXLS,hsbNLS udisj/udisj-*.log > perfprofile-output/udisj-hsb.tex 
python perfprofile-gen.py nhsbXEG,nhsbNEG,nhsbXLG,nhsbNLG,nhsbXES,nhsbNES,nhsbXLS,nhsbNLS udisj/udisj-*.log > perfprofile-output/udisj-nhsb.tex 
python perfprofile-gen.py frcXEG,frcNEG,frcXLG,frcNLG,frcXES,frcNES,frcXLS,frcNLS udisj/udisj-*.log > perfprofile-output/udisj-frc.tex 
python perfprofile-gen.py frrcXEG,frrcNEG,frrcXLG,frrcNLG,frrcXES,frrcNES,frrcXLS,frrcNLS udisj/udisj-*.log > perfprofile-output/udisj-frrc.tex 
python perfprofile-gen.py rcXLSS,rcXES,rcXEG,rcXLSG udisj/udisj-*.log > perfprofile-output/udisj-rc.tex
python perfprofile-gen.py rrcXLSS,rrcXES,rrcXEG,rrcXLSG udisj/udisj-*.log > perfprofile-output/udisj-rrc.tex
python perfprofile-gen.py fsXLS,fsXLG,fsXLC udisj/udisj-*.log > perfprofile-output/udisj-fs.tex

python perfprofile-gen.py hsbXEG,hsbNEG,hsbXLG,hsbNLG,hsbXES,hsbNES,hsbXLS,hsbNLS 01-random/*.log > perfprofile-output/01-random-hsb.tex 
python perfprofile-gen.py nhsbXEG,nhsbNEG,nhsbXLG,nhsbNLG,nhsbXES,nhsbNES,nhsbXLS,nhsbNLS 01-random/*.log > perfprofile-output/01-random-nhsb.tex 
python perfprofile-gen.py frcXEG,frcNEG,frcXLG,frcNLG,frcXES,frcNES,frcXLS,frcNLS 01-random/*.log > perfprofile-output/01-random-frc.tex 
python perfprofile-gen.py frrcXEG,frrcNEG,frrcXLG,frrcNLG,frrcXES,frrcNES,frrcXLS,frrcNLS 01-random/*.log > perfprofile-output/01-random-frrc.tex 
python perfprofile-gen.py rcXLSS,rcXES,rcXEG,rcXLSG 01-random/*.log > perfprofile-output/01-random-rc.tex
python perfprofile-gen.py rrcXLSS,rrcXES,rrcXEG,rrcXLSG 01-random/*.log > perfprofile-output/01-random-rrc.tex
python perfprofile-gen.py fsXLS,fsXLG,fsXLC 01-random/*.log > perfprofile-output/01-random-fs.tex

python perfprofile-gen.py hsbXEG,hsbNEG,hsbXLG,hsbNLG,hsbXES,hsbNES,hsbXLS,hsbNLS 2level/*/*.log > perfprofile-output/2level-hsb.tex 
python perfprofile-gen.py nhsbXEG,nhsbNEG,nhsbXLG,nhsbNLG,nhsbXES,nhsbNES,nhsbXLS,nhsbNLS 2level/*/*.log > perfprofile-output/2level-nhsb.tex 
python perfprofile-gen.py frcXEG,frcNEG,frcXLG,frcNLG,frcXES,frcNES,frcXLS,frcNLS 2level/*/*.log > perfprofile-output/2level-frc.tex 
python perfprofile-gen.py frrcXEG,frrcNEG,frrcXLG,frrcNLG,frrcXES,frrcNES,frrcXLS,frrcNLS 2level/*/*.log > perfprofile-output/2level-frrc.tex 
python perfprofile-gen.py rcXLSS,rcXES,rcXEG,rcXLSG 2level/*/*.log > perfprofile-output/2level-rc.tex
python perfprofile-gen.py rrcXLSS,rrcXES,rrcXEG,rrcXLSG 2level/*/*.log > perfprofile-output/2level-rrc.tex
python perfprofile-gen.py fsXLS,fsXLG,fsXLC 2level/*/*.log > perfprofile-output/2level-fs.tex

cd perfprofile-output

for TEX in *.tex; do
   latexmk -pdf -e '$pdflatex=q/pdflatex -shell-escape -halt-on-error -interaction=nonstopmode %S/' ${TEX}
done
