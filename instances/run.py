import sys
import subprocess
import re

BUILD_CLIQUER='../build-release-cliquer/src/nonnegrank'
BUILD_NAUTY_GUROBI='../build-release-nauty-gurobi/src/nonnegrank'
BUILD_NAUTY_SCIP='../build-release-nauty-scip/src/nonnegrank'
BUILD_BLISS_GUROBI='../build-release-bliss-gurobi/src/nonnegrank'
BUILD_BLISS_SCIP='../build-release-bliss-scip/src/nonnegrank'

# Config names:
# lowercase: bound
# E/L: enumeration or lazy generation
# N/B/X: symmetry detection with nauty or bliss or off
# C: cliquer
# G/S: gurobi/scip (twice: first for master problem, second for subproblem)

configs = {
   'rkXL': {'executable': BUILD_NAUTY_GUROBI, 'bound': 'rk', 'sym': False, 'enum': False},
   'fsXLC': {'executable': BUILD_CLIQUER, 'bound': 'fs', 'sym': False, 'enum': False},
   'fsXLG': {'executable': BUILD_NAUTY_GUROBI, 'bound': 'fs', 'sym': False, 'enum': False},
   'fsXLS': {'executable': BUILD_NAUTY_SCIP, 'bound': 'fs', 'sym': False, 'enum': False},
   'rcXEG': {'executable': BUILD_NAUTY_GUROBI, 'bound': 'rc', 'sym': False, 'enum': True},
   'rcXES': {'executable': BUILD_NAUTY_SCIP, 'bound': 'rc', 'sym': False, 'enum': True},
   'rcXLSG': {'executable': BUILD_NAUTY_GUROBI, 'bound': 'rc', 'sym': False, 'enum': False},
   'rcXLSS': {'executable': BUILD_NAUTY_SCIP, 'bound': 'rc', 'sym': False, 'enum': False},
   'rrcXEG': {'executable': BUILD_NAUTY_GUROBI, 'bound': 'rrc', 'sym': False, 'enum': True},
   'rrcXES': {'executable': BUILD_NAUTY_SCIP, 'bound': 'rrc', 'sym': False, 'enum': True},
   'rrcXLSG': {'executable': BUILD_NAUTY_GUROBI, 'bound': 'rrc', 'sym': False, 'enum': False},
   'rrcXLSS': {'executable': BUILD_NAUTY_SCIP, 'bound': 'rrc', 'sym': False, 'enum': False},
   'frcXEG': {'executable': BUILD_NAUTY_GUROBI, 'bound': 'frc', 'sym': False, 'enum': True},
   'frcNEG': {'executable': BUILD_NAUTY_GUROBI, 'bound': 'frc', 'sym': True, 'enum': True},
   'frcBEG': {'executable': BUILD_BLISS_GUROBI, 'bound': 'frc', 'sym': True, 'enum': True},
   'frcXLG': {'executable': BUILD_NAUTY_GUROBI, 'bound': 'frc', 'sym': False, 'enum': False},
   'frcNLG': {'executable': BUILD_NAUTY_GUROBI, 'bound': 'frc', 'sym': True, 'enum': False},
   'frcBLG': {'executable': BUILD_BLISS_GUROBI, 'bound': 'frc', 'sym': True, 'enum': False},
   'frcXES': {'executable': BUILD_NAUTY_SCIP, 'bound': 'frc', 'sym': False, 'enum': True},
   'frcNES': {'executable': BUILD_NAUTY_SCIP, 'bound': 'frc', 'sym': True, 'enum': True},
   'frcBES': {'executable': BUILD_BLISS_SCIP, 'bound': 'frc', 'sym': True, 'enum': True},
   'frcXLS': {'executable': BUILD_NAUTY_SCIP, 'bound': 'frc', 'sym': False, 'enum': False},
   'frcNLS': {'executable': BUILD_NAUTY_SCIP, 'bound': 'frc', 'sym': True, 'enum': False},
   'frcBLS': {'executable': BUILD_BLISS_SCIP, 'bound': 'frc', 'sym': True, 'enum': False},
   'frrcXEG': {'executable': BUILD_NAUTY_GUROBI, 'bound': 'frrc', 'sym': False, 'enum': True},
   'frrcNEG': {'executable': BUILD_NAUTY_GUROBI, 'bound': 'frrc', 'sym': True, 'enum': True},
   'frrcBEG': {'executable': BUILD_BLISS_GUROBI, 'bound': 'frrc', 'sym': True, 'enum': True},
   'frrcXLG': {'executable': BUILD_NAUTY_GUROBI, 'bound': 'frrc', 'sym': False, 'enum': False},
   'frrcNLG': {'executable': BUILD_NAUTY_GUROBI, 'bound': 'frrc', 'sym': True, 'enum': False},
   'frrcBLG': {'executable': BUILD_BLISS_GUROBI, 'bound': 'frrc', 'sym': True, 'enum': False},
   'frrcXES': {'executable': BUILD_NAUTY_SCIP, 'bound': 'frrc', 'sym': False, 'enum': True},
   'frrcNES': {'executable': BUILD_NAUTY_SCIP, 'bound': 'frrc', 'sym': True, 'enum': True},
   'frrcBES': {'executable': BUILD_BLISS_SCIP, 'bound': 'frrc', 'sym': True, 'enum': True},
   'frrcXLS': {'executable': BUILD_NAUTY_SCIP, 'bound': 'frrc', 'sym': False, 'enum': False},
   'frrcNLS': {'executable': BUILD_NAUTY_SCIP, 'bound': 'frrc', 'sym': True, 'enum': False},
   'frrcBLS': {'executable': BUILD_BLISS_SCIP, 'bound': 'frrc', 'sym': True, 'enum': False},
   'nhsbXEG': {'executable': BUILD_NAUTY_GUROBI, 'bound': 'nhsb', 'sym': False, 'enum': True},
   'nhsbNEG': {'executable': BUILD_NAUTY_GUROBI, 'bound': 'nhsb', 'sym': True, 'enum': True},
   'nhsbBEG': {'executable': BUILD_BLISS_GUROBI, 'bound': 'nhsb', 'sym': True, 'enum': True},
   'nhsbXLG': {'executable': BUILD_NAUTY_GUROBI, 'bound': 'nhsb', 'sym': False, 'enum': False},
   'nhsbNLG': {'executable': BUILD_NAUTY_GUROBI, 'bound': 'nhsb', 'sym': True, 'enum': False},
   'nhsbBLG': {'executable': BUILD_BLISS_GUROBI, 'bound': 'nhsb', 'sym': True, 'enum': False},
   'nhsbXES': {'executable': BUILD_NAUTY_SCIP, 'bound': 'nhsb', 'sym': False, 'enum': True},
   'nhsbNES': {'executable': BUILD_NAUTY_SCIP, 'bound': 'nhsb', 'sym': True, 'enum': True},
   'nhsbBES': {'executable': BUILD_BLISS_SCIP, 'bound': 'nhsb', 'sym': True, 'enum': True},
   'nhsbXLS': {'executable': BUILD_NAUTY_SCIP, 'bound': 'nhsb', 'sym': False, 'enum': False},
   'nhsbNLS': {'executable': BUILD_NAUTY_SCIP, 'bound': 'nhsb', 'sym': True, 'enum': False},
   'nhsbBLS': {'executable': BUILD_BLISS_SCIP, 'bound': 'nhsb', 'sym': True, 'enum': False},
   'hsbXEG': {'executable': BUILD_NAUTY_GUROBI, 'bound': 'hsb', 'sym': False, 'enum': True},
   'hsbNEG': {'executable': BUILD_NAUTY_GUROBI, 'bound': 'hsb', 'sym': True, 'enum': True},
   'hsbBEG': {'executable': BUILD_BLISS_GUROBI, 'bound': 'hsb', 'sym': True, 'enum': True},
   'hsbXLG': {'executable': BUILD_NAUTY_GUROBI, 'bound': 'hsb', 'sym': False, 'enum': False},
   'hsbNLG': {'executable': BUILD_NAUTY_GUROBI, 'bound': 'hsb', 'sym': True, 'enum': False},
   'hsbBLG': {'executable': BUILD_BLISS_GUROBI, 'bound': 'hsb', 'sym': True, 'enum': False},
   'hsbXES': {'executable': BUILD_NAUTY_SCIP, 'bound': 'hsb', 'sym': False, 'enum': True},
   'hsbNES': {'executable': BUILD_NAUTY_SCIP, 'bound': 'hsb', 'sym': True, 'enum': True},
   'hsbBES': {'executable': BUILD_BLISS_SCIP, 'bound': 'hsb', 'sym': True, 'enum': True},
   'hsbXLS': {'executable': BUILD_NAUTY_SCIP, 'bound': 'hsb', 'sym': False, 'enum': False},
   'hsbNLS': {'executable': BUILD_NAUTY_SCIP, 'bound': 'hsb', 'sym': True, 'enum': False},
   'hsbBLS': {'executable': BUILD_BLISS_SCIP, 'bound': 'hsb', 'sym': True, 'enum': False},
}

instance = sys.argv[1]
selectedConfigs = sys.argv[2:]
if len(selectedConfigs) == 0:
   selectedConfigs = configs.keys()

for config in selectedConfigs:
   params = configs[config]
   prefix = 'ulimit -t 600; '
   call = '%s %s %s' % (params['executable'], instance, params['bound'])
   if params['sym']:
      call += ' +sym'
   else:
      call += ' -sym'
   if params['enum']:
      call += ' +enum'
   else:
      call += ' -enum'

   #sys.stderr.write('# ' + call + '\n')
   #sys.stderr.flush()
   sys.stdout.write('%s: ' % (config))
   sys.stdout.flush()
   process = subprocess.Popen(prefix + call, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
   output = process.communicate()[0].decode('ascii')
   reSymmetry = re.compile('.*Orbits: ([0-9e.+]*) \\(time: ([0-9e.+]*)s\\).*')
   reEnumeration = re.compile('.*Enumerated.*: ([0-9e.+]*) \\(time: ([0-9e.+]*)s\\).*')
   reBound = re.compile('.*Lower bound <[a-z ]*>: ([0-9e.+]*) \\(time: ([0-9e.+]*)s\\).*')
   symTime = 0.0
   enumTime = 0.0
   value = None
   for line in output.split('\n'):
      result = reSymmetry.match(line)
      if result:
         assert symTime == 0.0
         symTime = float(result.group(2))
      result = reEnumeration.match(line)
      if result:
         assert enumTime == 0.0
         enumTime = float(result.group(2))
      result = reBound.match(line)
      if result:
         assert value is None
         value = float(result.group(1))
         time = float(result.group(2))
   if value is None:
      value = 0.0
      time = float('inf')
   sys.stdout.write('%f (sym: %fs, enum: %f, bound: %f)\n' % (value, symTime, enumTime, time))
   sys.stdout.flush()
