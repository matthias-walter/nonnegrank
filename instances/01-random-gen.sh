#!/usr/bin/env bash

n=$1 # number of variables
k=$2 # number of random points

code=$(cat << EOF
my \$vertices = rand_vert(cube($n, 1, 0)->VERTICES, $k);
my \$P = new Polytope<Rational>(VERTICES=>\$vertices);
my \$facets = \$P->FACETS;
print(\$P->N_FACETS, " ", \$P->N_VERTICES, "\n\n");
for (my \$r = 0; \$r < \$P->N_FACETS; \$r++)
{
   for (my \$c = 0; \$c < \$P->N_VERTICES; \$c++)
   {
      my \$x = (\$facets->minor(range(\$r,\$r), ~[0]) * transpose(\$vertices->minor(range(\$c,\$c), ~[0])))->elem(0,0) / \$vertices->elem(\$c,0) + \$facets->elem(\$r,0);
      print("\$x ");
   }
   print("\n");
}
exit;
EOF
)

polymake "${code}"
