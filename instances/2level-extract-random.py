#!/usr/bin/env python

import os
import sys
import gzip
import random

DATABASE='../../experiments/'

if not os.path.exists('2level'):
   os.mkdir('2level')
if not os.path.exists('2level/' + sys.argv[1]):
   os.mkdir('2level/' + sys.argv[1])

count = 0
with gzip.open('%s/%s.gz' % (DATABASE, sys.argv[1]), 'rt') as f:
   for line in f:
      count += 1

num = int(sys.argv[2])

indices = [ i for i in range(count) ]
random.shuffle(indices)
indices = indices[:num]
indices.sort()

print('Extracting {} of {} matrices.'.format(num, count))

i = 0
nextIndex = 0
with gzip.open('%s/%s.gz' % (DATABASE, sys.argv[1]), 'rt') as f:
   for line in f:
      if i == indices[nextIndex]:
         print('Extracting index {}.'.format(i))
         line = line.split(' ')
         matFile = open('2level/{}/{}.mat'.format(sys.argv[1], i), 'w')
         matFile.write('{} {}\n\n{}\n'.format(line[2], line[3], ' '.join(line[4])))
         matFile.close()
         nextIndex += 1
         if nextIndex == len(indices):
            break
      i += 1

#let "i = 0"
#zcat $DATABASE/$1.gz | while read line; do
#   if [ $i -eq ${INDEX} ]; then
#      echo "$(cut -d' ' -f3 <<< "${line}")" "$(cut -d' ' -f4 <<< "${line}")$(cut -d' ' -f5 <<< "${line}" | sed -e 's/0/ 0/g' -e 's/1/ 1/g')" > 2level/$1/$i.mat
#   fi
#   let "i++"
#done
