#!/usr/bin/env bash

mkdir -p 01-random

for NUM in `seq 1 10`; do
   NUM=`printf "%02d\n" ${NUM}`
   ./01-random-gen.sh 6 12  > 01-random/rnd-06-12-${NUM}.mat
   ./01-random-gen.sh 6 18  > 01-random/rnd-06-18-${NUM}.mat
   ./01-random-gen.sh 6 24  > 01-random/rnd-06-24-${NUM}.mat
   ./01-random-gen.sh 6 30  > 01-random/rnd-06-30-${NUM}.mat

   ./01-random-gen.sh 7 14  > 01-random/rnd-07-14-${NUM}.mat
   ./01-random-gen.sh 7 18  > 01-random/rnd-07-18-${NUM}.mat
   ./01-random-gen.sh 7 22  > 01-random/rnd-07-22-${NUM}.mat
   
   ./01-random-gen.sh 8 16  > 01-random/rnd-08-16-${NUM}.mat
   ./01-random-gen.sh 8 19  > 01-random/rnd-08-19-${NUM}.mat
   ./01-random-gen.sh 8 22  > 01-random/rnd-08-22-${NUM}.mat
done

