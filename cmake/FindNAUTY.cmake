include(FindPackageHandleStandardArgs)

# Check whether environment variable NAUTY_DIR was set.
if(NOT NAUTY_DIR)
   set(NAUTY_DIR_TEST $ENV{NAUTY_DIR})
   if(NAUTY_DIR_TEST)
      set(NAUTY_DIR $ENV{NAUTY_DIR} CACHE PATH "Path to nauty build / install directory")
   endif()
endif()

# If the nauty directory is specified, first try to use exactly that.
if(NAUTY_DIR)
   # Look for the includes with subdirectory bliss
   find_path(NAUTY_MAIN_INCLUDE_DIR
      NAMES nauty/nauty.h
      PATHS ${NAUTY_DIR}
      NO_DEFAULT_PATH
      )
endif()

if(NAUTY_MAIN_INCLUDE_DIR)
   # Look for the other headers in the nauty directory.
   find_path(NAUTY_EXTRA_INCLUDE_DIR
      NAMES nauty/naugroup.h
      PATHS ${NAUTY_DIR}
      NO_DEFAULT_PATH
      )

   # Look for the library in the nauty directory.
   find_library(NAUTY_LIBRARY
      NAMES nauty.a nauty
      PATHS ${NAUTY_DIR}
      NO_DEFAULT_PATH
      )
else()
   find_path(NAUTY_MAIN_INCLUDE_DIR
      NAMES nauty/nauty.h
      PATH_SUFFIXES include nauty include/nauty
      )

   find_path(NAUTY_EXTRA_INCLUDE_DIR
      NAMES nauty/naugroup.h
      PATH_SUFFIXES include nauty include/nauty
      )

   find_library(NAUTY_LIBRARY
      NAMES nauty.a nauty
      PATH_SUFFIXES lib
      )
endif()

if(NAUTY_MAIN_INCLUDE_DIR AND NAUTY_EXTRA_INCLUDE_DIR)
   # Extract version from header.
   file(STRINGS "${NAUTY_MAIN_INCLUDE_DIR}/nauty/nauty.h" _NAUTY_VERSION_LINE REGEX "^#define[\t ]+NAUTYVERSION.*")
   string(REGEX REPLACE "^.*NAUTYVERSION[\t ]+\"([^\" ]*).*$" "\\1" NAUTY_VERSION "${_NAUTY_VERSION_LINE}")

   # Set variables and handle standard args.
   if(NAUTY_MAIN_INCLUDE_DIR STREQUAL NAUTY_EXTRA_INCLUDE_DIR)
      set(NAUTY_INCLUDE_DIRS ${NAUTY_MAIN_INCLUDE_DIR})
   else()
      set(NAUTY_INCLUDE_DIRS ${NAUTY_MAIN_INCLUDE_DIR} ${NAUTY_EXTRA_INCLUDE_DIR})
   endif()
   set(NAUTY_LIBRARIES ${NAUTY_LIBRARY})
endif()

find_package_handle_standard_args(NAUTY
   REQUIRED_VARS NAUTY_INCLUDE_DIRS NAUTY_LIBRARIES
   VERSION_VAR NAUTY_VERSION
   )

