from libcpp.vector cimport vector
from libcpp cimport bool

include "config.pxi" # Compile-time definitions

cdef extern from "nonnegrank.hpp" namespace "nonnegrank":
   cdef cppclass NonnegRankSolver "nonnegrank::Solver":
      NonnegRankSolver()
      NonnegRankSolver(vector[vector[double]])

      double getCurrentLowerBound()


cdef class Solver:
   cdef NonnegRankSolver* _solver

   def __cinit__(self, matrix):
      cdef vector[vector[double]] denseMatrix
      if isinstance(matrix, list):
         denseMatrix.resize(len(matrix))
         for r in xrange(len(matrix)):
            if isinstance(matrix[r], list):
               denseMatrix[r].resize(len(matrix[r]))
               for c in xrange(len(matrix[r])):
                  denseMatrix[r][c] = matrix[r][c]
            else:
               raise Exception('Unknown type for matrix row!')
      else:
         raise Exception('Unknown type for matrix!')
      self._solver = new NonnegRankSolver(denseMatrix)

   def __dealloc__(self):
      del self._solver

