## Purpose

This tool can compute bounds on the nonnegative rank of a given matrix.

## Build instructions

1. Clone the repository.
2. Create build directory, e.g., using `mkdir build`
3. Go to the build directory, e.g., using `cd build`
4. Run cmake via `cmake ..`.

